import 'package:flutter/material.dart';
import 'package:gcmedicalapp/main.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'classes/caixa.dart';
import 'globals.dart';

class BuscaMateriaisOldPage extends StatefulWidget {
  BuscaMateriaisOldPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _BuscaMateriaisOldState createState() => _BuscaMateriaisOldState();
}

List<int> selectedItems = [];
List<DropdownMenuItem> items = [];
bool habilitado = false;

class _BuscaMateriaisOldState extends State<BuscaMateriaisOldPage> {
  List<Caixa> caixas;
  List<Caixa> list;

  @override
  void initState() {
    super.initState();
    habilitado = false;
    loadCaixas();
    caixaSelecionada = Caixa.empty();
  }

  @override
  Widget build(BuildContext context) {
    Caixa selectedValue = Caixa.empty();
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG2.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 30,
                              height: 30,
                              decoration: new BoxDecoration(
                                color: Colors.lightBlue[50],
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(18.0),
                                    topRight: const Radius.circular(18.0),
                                    bottomLeft: const Radius.circular(18.0),
                                    bottomRight: const Radius.circular(18.0)),
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    navigateToAgendamentoMensagem(context);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black,
                                    size: 18.0,
                                    semanticLabel: 'Clear',
                                  ))),
                        ])),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                        child: Container(
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 30),
                          child: Stack(
                            children: [
                              new Positioned(
                                top: 2.0,
                                left: 2.0,
                                child: new Text(
                                  'Seleção de Materiais',
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.5),
                                      fontFamily: 'Montserrat',
                                      fontSize: 26,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              new Text('Seleção de Materiais',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      color: Colors.white,
                                      fontSize: 26,
                                      fontWeight: FontWeight.bold)),
                            ],
                          )),
                    ))),
                Flexible(
                    flex: 6,
                    fit: FlexFit.tight,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 20),
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.teal[300],
                            blurRadius: 0.0, // soften the shadow
                            spreadRadius: -8.0, //extend the shadow
                            offset: Offset(
                              0.0, // Move to right 10  horizontally
                              20.0, // Move to bottom 10 Vertically
                            ),
                          )
                        ],
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0),
                            bottomLeft: const Radius.circular(10.0),
                            bottomRight: const Radius.circular(10.0)),
                      ),
                      child: Column(children: <Widget>[
                        Expanded(flex: 1, child: Center()),
                        Expanded(
                            flex: 2,
                            child: Container(
                              alignment: Alignment.center,
                              child: Text(
                                  'Escolha a caixa pelo seu nome ou pelo item',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      fontStyle: FontStyle.italic,
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold)),
                            )),
                        Expanded(flex: 1, child: Center()),
                        Flexible(
                            flex: 4,
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    decoration: new BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: new BorderRadius.only(
                                          topLeft: const Radius.circular(10.0),
                                          topRight: const Radius.circular(10.0),
                                          bottomLeft:
                                              const Radius.circular(10.0),
                                          bottomRight:
                                              const Radius.circular(10.0)),
                                    ),
                                    child: SearchableDropdown(
                                      closeButton: Container(),
                                      hint: Text(
                                        'SELECIONE A CAIXA',
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 18),
                                      ),
                                      items: items,
                                      isExpanded: true,
                                      displayClearIcon: false,
                                      value: selectedValue.nome,
                                      isCaseSensitiveSearch: false,
                                      searchFn: (String keyword, items) {
                                        List<int> ret = List<int>();
                                        if (keyword != null &&
                                            items != null &&
                                            keyword.isNotEmpty) {
                                          if (keyword.length >= 3) {
                                            buscaPalavra(keyword);
                                            keyword.split(" ").forEach((k) {
                                              int i = 0;
                                              items.forEach((item) {
                                                if (k.isNotEmpty &&
                                                    (item.value
                                                        .toString()
                                                        .toLowerCase()
                                                        .contains(
                                                            k.toLowerCase()))) {
                                                  ret.add(i);
                                                }
                                                i++;
                                              });
                                            });
                                          }
                                        }
                                        if (keyword.isEmpty) {
                                          ret = Iterable<int>.generate(
                                                  items.length)
                                              .toList();
                                        }
                                        return (ret);
                                      },
                                      searchHint: new Text(
                                        'Selecione a caixa',
                                        style: new TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 16),
                                      ),
                                      onChanged: (newValue) {
                                        setState(() {
                                          if (newValue.nome != null) {
                                            habilitado = true;
                                            caixaSelecionada.nome =
                                                newValue.nome;
                                            caixaSelecionada.id = newValue.id;
                                            caixaSelecionada.idProprietario =
                                                newValue.idProprietario;
                                          }
                                        });
                                      },
                                    ),
                                  )
                                ])),
                        Expanded(flex: 1, child: Center()),
                      ]),
                    )),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 25),
                          child: Container(
                              padding: EdgeInsets.all(15),
                              decoration: new BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[700],
                                    blurRadius: 2.0, // soften the shadow
                                    spreadRadius: 1.0, //extend the shadow
                                    offset: Offset(
                                      0.0, // Move to right 10  horizontally
                                      1.0, // Move to bottom 10 Vertically
                                    ),
                                  )
                                ],
                                color: Colors.white,
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(50.0),
                                    topRight: const Radius.circular(50.0),
                                    bottomLeft: const Radius.circular(50.0),
                                    bottomRight: const Radius.circular(50.0)),
                              ),
                              child: Container(
                                  margin: EdgeInsets.only(left: 5),
                                  child: habilitado
                                      ? GestureDetector(
                                          onTap: () {
                                            indexCaixa = indexCaixa + 1;
                                            caixaSelecionada.indexNaLista =
                                                indexCaixa;
                                            navigateToVisualizacaoCaixa(
                                                context);
                                          },
                                          child: Icon(
                                            Icons.arrow_forward_ios,
                                            color: Colors.grey[600],
                                            size: 34.0,
                                            semanticLabel: 'Advance',
                                          ))
                                      : GestureDetector(
                                          onTap: () {},
                                          child: Icon(
                                            Icons.arrow_forward_ios,
                                            color: Colors.grey[300],
                                            size: 34.0,
                                            semanticLabel: 'Advance',
                                          ))))),
                    )),
                Flexible(flex: 3, fit: FlexFit.tight, child: Center()),
              ]))
        ])));
  }

  Future loadCaixas() async {
    List<Caixa> sublist = new List();

    http.get(url + '/api/caixas/a', headers: {
      "Content0-Type": "application/json",
      "Authorization": token
    }).then((http.Response response) {
      if (response.statusCode == 200) {
        var obj = json.decode(response.body)["data"];
        List<Map<String, dynamic>> cxs = obj != null ? List.from(obj) : null;
        for (int i = 0; i < cxs.length; i++) {
          var item = cxs[i];

          var idCaixa = item["id"];
          var nomeCaixa = item["nome"];
          var proprietario = item["proprietario_id"];
          Caixa c = new Caixa(idCaixa, nomeCaixa, proprietario);
          sublist.add(c);
        }
      }

      this.setState(() {
        items = sublist
            .map((item) => new DropdownMenuItem<Caixa>(
                child: Row(children: <Widget>[
                  Flexible(
                      flex: 4,
                      fit: FlexFit.tight,
                      child: Text(
                        item.nome,
                        style:
                            TextStyle(fontFamily: 'Montserrat', fontSize: 18),
                      )),
                  if (item.idProprietario == 1)
                    Flexible(
                        flex: 1,
                        child: Container(
                            margin: EdgeInsets.only(left: 10),
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("img/Logo GC - Pag 12.png"),
                                fit: BoxFit.fill,
                              ),
                            )))
                  else
                    Flexible(
                        flex: 1,
                        child: Padding(padding: EdgeInsets.only(left: 10)))
                ]),
                value: item))
            .toList();
      });
    });
  }

  void buscaPalavra(String keyword) {
    List<Caixa> sublist = new List();
    http.get(url + '/api/caixas/' + keyword, headers: {
      "Content0-Type": "application/json",
      "Authorization": token
    }).then((http.Response response) {
      if (response.statusCode == 200) {
        var obj = json.decode(response.body)["data"];
        List<Map<String, dynamic>> cxs = obj != null ? List.from(obj) : null;

        for (int i = 0; i < cxs.length; i++) {
          var item = cxs[i];

          var idCaixa = item["id"];
          var nomeCaixa = item["nome"];
          var proprietario = item["proprietario_id"];
          Caixa c = new Caixa(idCaixa, nomeCaixa, proprietario);
          sublist.add(c);
        }
      }
      this.setState(() {
        items = sublist
            .map((item) => new DropdownMenuItem<Caixa>(
                child: Row(children: <Widget>[
                  Flexible(
                      flex: 4,
                      fit: FlexFit.tight,
                      child: Text(
                        item.nome,
                        style:
                            TextStyle(fontFamily: 'Montserrat', fontSize: 18),
                      )),
                  if (item.idProprietario == 1)
                    Flexible(
                        flex: 1,
                        child: Container(
                            margin: EdgeInsets.only(left: 10),
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("img/Logo GC - Pag 12.png"),
                                fit: BoxFit.fill,
                              ),
                            )))
                  else
                    Flexible(
                        flex: 1,
                        child: Padding(padding: EdgeInsets.only(left: 10)))
                ]),
                value: item))
            .toList();
      });
    });
  }
}
