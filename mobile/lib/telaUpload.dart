import 'package:flutter/material.dart';
import 'package:gcmedicalapp/main.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'globals.dart';

class TelaUploadPage extends StatefulWidget {
  TelaUploadPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _TelaUploadState createState() => _TelaUploadState();
}

final ImagePicker _picker = ImagePicker();
bool habilitado = false;

class _TelaUploadState extends State<TelaUploadPage> {
  List<Widget> list = new List();

  @override
  void initState() {
    list.clear();
    habilitado = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG2.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Stack(children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 30,
                              height: 30,
                              decoration: new BoxDecoration(
                                color: Colors.lightBlue[50],
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(18.0),
                                    topRight: const Radius.circular(18.0),
                                    bottomLeft: const Radius.circular(18.0),
                                    bottomRight: const Radius.circular(18.0)),
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    navigateToVisualizacaoFinalRelato(context);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black,
                                    size: 18.0,
                                    semanticLabel: 'Clear',
                                  ))),
                        ])
                      ],
                    )),
                Flexible(flex: 2, fit: FlexFit.tight, child: Center()),
                Flexible(
                    flex: 6,
                    fit: FlexFit.tight,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(15, 50, 10, 40),
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0),
                            bottomLeft: const Radius.circular(10.0),
                            bottomRight: const Radius.circular(10.0)),
                      ),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Flexible(
                                flex: 3,
                                fit: FlexFit.tight,
                                child: Container(
                                  margin: EdgeInsets.symmetric(horizontal: 20),
                                  child: SingleChildScrollView(
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: <Widget>[
                                        Flexible(
                                            flex: 2,
                                            fit: FlexFit.tight,
                                            child: Align(
                                                alignment: Alignment.centerLeft,
                                                child: GestureDetector(
                                                    onTap: () {
                                                      _choose();
                                                    },
                                                    child: Icon(
                                                      Icons.add_circle,
                                                      color:
                                                          hexToColor('#3fecc8'),
                                                      size: 80.0,
                                                      semanticLabel: 'Add file',
                                                    )))),
                                        Flexible(
                                            flex: 5,
                                            fit: FlexFit.tight,
                                            child: Container(
                                              height: 70,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              decoration: new BoxDecoration(
                                                borderRadius: new BorderRadius
                                                        .only(
                                                    topLeft:
                                                        const Radius.circular(
                                                            10.0),
                                                    topRight:
                                                        const Radius.circular(
                                                            10.0),
                                                    bottomLeft:
                                                        const Radius.circular(
                                                            10.0),
                                                    bottomRight:
                                                        const Radius.circular(
                                                            10.0)),
                                              ),
                                              // child: ListView(
                                              //   children: list,
                                              //   scrollDirection:
                                              //       Axis.horizontal,
                                              // ),
                                              child: list.length > 0
                                                  ? ListView(
                                                      children: list,
                                                      scrollDirection:
                                                          Axis.horizontal)
                                                  : Align(
                                                      alignment:
                                                          Alignment.center,
                                                      child: Text(
                                                        'Adicionar fotos dos Documentos e Caixas',
                                                        style: TextStyle(
                                                            fontSize: 20,
                                                            decoration:
                                                                TextDecoration
                                                                    .underline,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      )),
                                            ))
                                      ])),
                                )),
                            Flexible(
                              flex: 2,
                              fit: FlexFit.tight,
                              child: Container(
                                  margin: EdgeInsets.symmetric(vertical: 10),
                                  width: MediaQuery.of(context).size.width,
                                  height: 60,
                                  child: habilitado
                                      ? RaisedButton(
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      20.0),
                                              side: BorderSide(
                                                  color: hexToColor('#3fecc8'),
                                                  width: 1)),
                                          onPressed: () {
                                            confirmImages();
                                          },
                                          color: Colors.white,
                                          textColor: Colors.black,
                                          child: Stack(
                                            children: <Widget>[
                                              Center(
                                                child: Text("CONFIRMAR",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontFamily:
                                                            'Montserrat',
                                                        fontSize: 24,
                                                        color:hexToColor('#3fecc8'))),
                                              ),
                                              Align(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Icon(
                                                    Icons.arrow_forward_ios,
                                                    color:hexToColor('#3fecc8'),
                                                    size: 24.0,
                                                    semanticLabel: 'Advance',
                                                  )),
                                            ],
                                          ))
                                      : 
                                      RaisedButton(
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      20.0),
                                              side: BorderSide(
                                                  color: hexToColor('#3fecc8'),
                                                  width: 1)),
                                          onPressed: () {
                                            navigateToMensagemFinalRelato(context);
                                          },
                                          color: Colors.white,
                                          textColor: Colors.black,
                                          child: Stack(
                                            children: <Widget>[
                                              Center(
                                                child: Text("CONFIRMAR",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontFamily:
                                                            'Montserrat',
                                                        fontSize: 24,
                                                        color:hexToColor('#3fecc8'))),
                                              ),
                                              Align(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Icon(
                                                    Icons.arrow_forward_ios,
                                                    color:hexToColor('#3fecc8'),
                                                    size: 24.0,
                                                    semanticLabel: 'Advance',
                                                  )),
                                            ],
                                          ))
                                      // RaisedButton(
                                      //     shape: new RoundedRectangleBorder(
                                      //         borderRadius:
                                      //             new BorderRadius.circular(
                                      //                 20.0),
                                      //         side: BorderSide(
                                      //             color: Colors.grey[600],
                                      //             width: 1)),
                                      //     onPressed: () {
                                            
                                      //     },
                                      //     color: Colors.white,
                                      //     textColor: Colors.black,
                                      //     child: Stack(
                                      //       children: <Widget>[
                                      //         Center(
                                      //           child: Text("CONFIRMAR",
                                      //               textAlign: TextAlign.center,
                                      //               style: TextStyle(
                                      //                   fontFamily:
                                      //                       'Montserrat',
                                      //                   fontSize: 24,
                                      //                   color: Colors.grey[600])),
                                      //         ),
                                      //         Align(
                                      //             alignment:
                                      //                 Alignment.centerRight,
                                      //             child: Icon(
                                      //               Icons.arrow_forward_ios,
                                      //               color:Colors.grey[600],
                                      //               size: 24.0,
                                      //               semanticLabel: 'Advance',
                                      //             )),
                                      //       ],
                                      //     ))
                                      ),
                            )
                          ]),
                    )),
                Flexible(flex: 4, fit: FlexFit.tight, child: Center())
              ]))
        ])));
  }

  void _choose() async {
    File _image;
    final pickedFile = await _picker.getImage(source: ImageSource.camera);
    _image = File(pickedFile.path);
    setState(() {
      list = List.from(list)
        ..add(Miniatura(_image, list.length, onClick,
            key: new Key(_image.toString())));
      if (list.length > 0) {
        habilitado = true;
      } else {
        habilitado = false;
      }
    });
  }

  void onClick(Key key) {
    setState(() {
      list = List.from(list)..removeWhere((item) => item.key == key);
      if (list.length > 0) {
        habilitado = true;
      } else {
        habilitado = false;
      }
    });
  }

  void confirmImages() {
    imagens.clear();
    int len = list.length;
    for (int i = 0; i < len; i++) {
      Miniatura m = list[i];
      imagens.add(m.img);
    }
    navigateToMensagemFinalRelato(context);
  }
}

class Miniatura extends StatelessWidget {
  Miniatura(this.img, this.index, this.notifyClick, {Key key})
      : super(key: key);
  File img;
  int index;
  final Function notifyClick;
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(right: 8),
        height: 70,
        width: 70,
        decoration: BoxDecoration(
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(10.0),
              topRight: const Radius.circular(10.0),
              bottomLeft: const Radius.circular(10.0),
              bottomRight: const Radius.circular(10.0)),
        ),
        child: Stack(children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15.0))),
              width: 70,
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.file(
                    img,
                    fit: BoxFit.fill,
                  ))),
          Container(
              margin: EdgeInsets.fromLTRB(0, 5, 5, 0),
              child: GestureDetector(
                  onTap: () {
                    notifyClick(key);
                  },
                  child: Align(
                      alignment: Alignment.topRight,
                      child: Stack(children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15.0))),
                          width: 20,
                          height: 20,
                        ),
                        Icon(
                          Icons.close,
                          color: hexToColor('#77c6bf'),
                          size: 20.0,
                          semanticLabel: 'Advance',
                        )
                      ]))))
        ]));
  }
}
