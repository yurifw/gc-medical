import 'package:flutter/material.dart';
import 'package:gcmedicalapp/main.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:rflutter_alert/rflutter_alert.dart';
import 'globals.dart' as gl;

class ListaSolicitacoesPage extends StatefulWidget {
  ListaSolicitacoesPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListaSolicitacoesState createState() => _ListaSolicitacoesState();
}

List<Widget> list = new List();
bool exibirMensagem = false;
bool inicial = true;
String msg;

class _ListaSolicitacoesState extends State<ListaSolicitacoesPage> {
  @override
  void initState() {
    super.initState();
    list.clear();
    msg = "";
    loadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG1.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 2,
                    fit: FlexFit.loose,
                    child: Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Stack(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(top: 20),
                                  width: 30,
                                  height: 30,
                                  decoration: new BoxDecoration(
                                    color: Colors.lightBlue[50],
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(18.0),
                                        topRight: const Radius.circular(18.0),
                                        bottomLeft: const Radius.circular(18.0),
                                        bottomRight:
                                            const Radius.circular(18.0)),
                                  ),
                                  child: GestureDetector(
                                      onTap: () {
                                        navigateToLogin(context);
                                      },
                                      child: Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.black,
                                        size: 18.0,
                                        semanticLabel: 'Clear',
                                      ))),
                              Container(
                                  width: MediaQuery.of(context).size.width - 30,
                                  child: Center(
                                      child: Stack(
                                    children: [
                                      new Positioned(
                                        top: 2.0,
                                        left: 2.0,
                                        child: new Text(
                                          'Lista de Solicitações',
                                          style: TextStyle(
                                              color:
                                                  Colors.black.withOpacity(0.5),
                                              fontFamily: 'Montserrat',
                                              fontSize: 26,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      new Text('Lista de Solicitações',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              color: Colors.white,
                                              fontSize: 26,
                                              fontWeight: FontWeight.bold)),
                                    ],
                                  )))
                            ])
                          ],
                        ))),
                Flexible(
                    flex: 3,
                    child: Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: 10),
                            padding: EdgeInsets.all(0),
                            width: MediaQuery.of(context).size.width,
                            height: 60,
                            child: GestureDetector(
                                onTap: () {
                                  gl.limpar = true;
                                  navigateToTipoCirurgia(context);
                                },
                                child: Container(
                                    padding: EdgeInsets.only(right: 20),
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20.0),
                                      gradient: LinearGradient(
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                        colors: [
                                          Color.fromRGBO(203, 232, 228, 1),
                                          Colors.white
                                        ],
                                      ),
                                    ),
                                    child: Stack(
                                      children: <Widget>[
                                        Center(
                                          child: Text("Cadastrar Nova",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontFamily: 'Montserrat',
                                                  fontSize: 24)),
                                        ),
                                        Align(
                                            alignment: Alignment.centerRight,
                                            child: Text("+",
                                                style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 36)))
                                      ],
                                      //)
                                    )))),
                        ValueListenableBuilder(
                          builder:
                              (BuildContext context, int value, Widget child) {
                            return Container(
                                margin: EdgeInsets.symmetric(vertical: 15),
                                child: value > 0
                                    ? Text(
                                        'Solicitações na fila: ' +
                                            value.toString(),
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black))
                                    : Center());
                          },
                          valueListenable: gl.requisicoesEmAberto,
                        )
                      ],
                    ))),
                Flexible(
                    flex: 12,
                    fit: FlexFit.tight,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[700],
                              blurRadius: 2.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                1.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                          child: Column(children: <Widget>[
                            Container(
                              child: Column(children: list),
                            )
                          ]),
                        )))
              ]))
        ])));
  }

  Future loadItems() async {
    List<Widget> sublist = new List();
    sublist.clear();
    http.get(gl.url + '/api/encaminhamentos/medicos', headers: {
      "Content-Type": "application/json",
      "Authorization": gl.token
    }).then((http.Response response) {
      if (response.statusCode == 200) {
        var obj = json.decode(response.body)["data"];
        List<Map<String, dynamic>> tags = obj != null ? List.from(obj) : null;
        if (tags != null) {
          for (int i = 0; i < tags.length; i++) {
            var item = tags[i];
            var nomePaciente = item["cirurgia"]["paciente"]["nome"];
            var procedimento = item["cirurgia"]["procedimento"];
            var idCirurgia = item["cirurgia"]["id"];
            var observacao = item["observacao"];
            sublist.add(Item(
                idCirurgia, nomePaciente, "eletiva", procedimento, observacao));
          }
        } else {
          sublist.add(Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  child: Text("Você não possui solicitações pendentes.",
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center)),
              Icon(
                Icons.check_circle,
                color: Colors.green,
                size: 68.0,
                semanticLabel: 'Clear',
              )
            ],
          ));
        }
        if (gl.deuErro == true) {
          gl.deuErro = false;
          _onAlertError(context);
        }
      }

      this.setState(() {
        if (gl.deuErro == true) {
          gl.deuErro = false;
          _onAlertError(context);
        }
        list.clear();
        list += sublist;
        inicial = false;
      });
    });
  }
}

class Item extends StatelessWidget {
  Item(this.idCirurgia, this.nomePaciente, this.tipoCirurgia, this.procedimento,
      this.obs);
  final int idCirurgia;
  final String nomePaciente;
  String tipoCirurgia;
  final String procedimento;
  final String obs;

  @override
  Widget build(BuildContext context) {
    if (tipoCirurgia == "eletiva")
      this.tipoCirurgia = "Eletiva";
    else {
      this.tipoCirurgia = "Urgência";
    }
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(padding: EdgeInsets.only(top: 10)),
          Text(
            nomePaciente,
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 20,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Row(children: <Widget>[
            Flexible(
                flex: 10,
                fit: FlexFit.tight,
                child: Text(
                  tipoCirurgia,
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                )),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: GestureDetector(
                  onTap: () {
                    msg = this.obs;
                    _onAlert(context);
                  },
                  child: Icon(
                    Icons.assignment_late,
                    color: Colors.red,
                    size: 30.0,
                  )),
            ),
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: GestureDetector(
                  onTap: () {
                    gl.idCirurgiaEdicao = idCirurgia;
                    navigateToAgendamentoEdicao(context);
                  },
                  child: Icon(
                    Icons.edit,
                    color: Colors.grey,
                    size: 30.0,
                  )),
            ),
          ]),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Text(
            procedimento,
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          Divider(
            color: Colors.black,
            height: 20,
            thickness: 1,
            endIndent: 0,
          ),
        ],
      ),
    );
  }
}

_onAlert(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Solicitação recusada",
    desc: msg,
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onAlertError(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Erro encontrado!",
    desc:
        "Ocorreu algum no envio da cirurgia, verifique sua conexão e tente novamente.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

class ItemUrgencia extends StatelessWidget {
  ItemUrgencia(
      this.idCirurgia, this.nomePaciente, this.tipoCirurgia, this.procedimento);
  final int idCirurgia;
  final String nomePaciente;
  final String tipoCirurgia;
  final String procedimento;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            nomePaciente,
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 20,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Row(children: <Widget>[
            Flexible(
                flex: 12,
                fit: FlexFit.tight,
                child: Text(
                  tipoCirurgia,
                  style: TextStyle(
                    fontFamily: 'Montserrat',
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                )),
          ]),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Text(
            procedimento,
            style: TextStyle(
              fontFamily: 'Montserrat',
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          Divider(
            color: Colors.black,
            height: 20,
            thickness: 1,
            endIndent: 0,
          ),
        ],
      ),
    );
  }
}
