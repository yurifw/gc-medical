import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gcmedicalapp/classes/cirurgiaJson.dart';
import 'package:gcmedicalapp/globals.dart';
import 'package:gcmedicalapp/main.dart';
import 'dart:convert';

import 'classes/serviceLocator.dart';
import 'classes/storageService.dart';
import 'classes/storageServiceWeb.dart';

class MensagemFinalRelatoPage extends StatefulWidget {
  MensagemFinalRelatoPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MensagemFinalRelatoState createState() => _MensagemFinalRelatoState();
}

final _observacaoController = TextEditingController();
CirurgiaJson novaCirurgia;

class _MensagemFinalRelatoState extends State<MensagemFinalRelatoPage> {
  @override
  void initState() {
    super.initState();
    _observacaoController.text = "";
    cirurgia.observacao = "";
    novaCirurgia = CirurgiaJson.empty();
    var tipoString = cirurgia.tipo;
    novaCirurgia.tipo = tipoString;
    novaCirurgia.data = cirurgia.data;
    novaCirurgia.procedimento = cirurgia.procedimento;
    novaCirurgia.cid = cirurgia.cid;
    var tipoPaciente = cirurgia.paciente.toJson();
    novaCirurgia.paciente = tipoPaciente;
    novaCirurgia.medico = cirurgia.medico;
    var jsonNovo = jsonEncode(cirurgia.materiais.map((e) => e).toList());
    var jsonFinal = jsonDecode(jsonNovo);
    novaCirurgia.materiais = jsonFinal;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG2.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 30,
                              height: 30,
                              decoration: new BoxDecoration(
                                color: Colors.lightBlue[50],
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(18.0),
                                    topRight: const Radius.circular(18.0),
                                    bottomLeft: const Radius.circular(18.0),
                                    bottomRight: const Radius.circular(18.0)),
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    navigateToVisualizacaoFinalRelato(context);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black,
                                    size: 18.0,
                                    semanticLabel: 'Clear',
                                  ))),
                        ])),
                Flexible(
                    flex: 8,
                    child: Center(
                        child: Column(children: <Widget>[
                      Container(
                          margin: EdgeInsets.fromLTRB(30, 30, 30, 10),
                          alignment: Alignment.center,
                          child: Text(
                              'Seu relatório foi enviado para aprovação',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  color: Colors.white,
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold))),
                      Container(
                          margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
                          child: Text(
                              "Você tem alguma observação? Caso tenha escreva abaixo",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.italic))),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
                          child: TextFormField(
                            style: new TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 18,
                              color: Colors.black,
                            ),
                            controller: _observacaoController,
                            maxLines: 8,
                            decoration: new InputDecoration(
                              hintText: "Insira o texto aqui",
                              contentPadding:
                                  EdgeInsets.fromLTRB(20, 15, 20, 15),
                              filled: true,
                              fillColor: Colors.white,
                              border: new OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                  const Radius.circular(18.0),
                                ),
                              ),
                            ),
                          ))
                    ]))),
                Flexible(
                  flex: 3,
                  child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(20.0),
                            side: BorderSide(color: Colors.lightBlue[100])),
                        onPressed: () {
                          finishItem();
                        },
                        color: Colors.lightBlue[50],
                        textColor: Colors.black,
                        child: Center(
                          child: Text("Finalizar",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'Montserrat', fontSize: 24)),
                        ),
                      )),
                ),
              ]))
        ])));
  }

  void finishItem() async {
    cirurgia.observacao = _observacaoController.text;
    novaCirurgia.observacao = cirurgia.observacao;
    var finalCirurgia = jsonEncode(novaCirurgia);
    int vezes = 0;
    int i = requisicoesEmAberto.value + 1;
    requisicoesEmAberto.value = i;
    limpar = true;
    StorageServiceWeb _myService = locator<StorageService>();

    Timer.periodic(const Duration(seconds: 10), (timer) {
      vezes++;
      if (sucessoRelato == true) {
        if (requisicoesEmAberto.value > 0) {
          int i = requisicoesEmAberto.value - 1;
          requisicoesEmAberto.value = i;
        }
        timer.cancel();
      } else {
        if (vezes <= 5) {
          _myService.sendRequestWithPhoto(finalCirurgia, imagens);
        } else {
          deuErro = true;
          timer.cancel();
        }
      }
    });
    navigateToListaSolicitacoes(context);
  }
}
