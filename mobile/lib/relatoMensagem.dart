import 'package:flutter/material.dart';
import 'package:gcmedicalapp/globals.dart';
import 'package:gcmedicalapp/main.dart';

import 'classes/caixa.dart';
import 'classes/materialCaixa.dart';

class RelatoMensagemPage extends StatefulWidget {
  RelatoMensagemPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _RelatoMensagemState createState() => _RelatoMensagemState();
}

class _RelatoMensagemState extends State<RelatoMensagemPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG1.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 30,
                              height: 30,
                              decoration: new BoxDecoration(
                                color: Colors.lightBlue[50],
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(18.0),
                                    topRight: const Radius.circular(18.0),
                                    bottomLeft: const Radius.circular(18.0),
                                    bottomRight: const Radius.circular(18.0)),
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    navigateToRelatoCirurgia(context);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black,
                                    size: 18.0,
                                    semanticLabel: 'Clear',
                                  ))),
                        ])),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 30),
                          child: Stack(
                            children: [
                              new Positioned(
                                top: 2.0,
                                left: 2.0,
                                child: new Text(
                                  'Relato de Cirurgia',
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.5),
                                      fontFamily: 'Montserrat',
                                      fontSize: 26,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              new Text('Relato de Cirurgia',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      color: Colors.white,
                                      fontSize: 26,
                                      fontWeight: FontWeight.bold)),
                            ],
                          )),
                    )),
                Flexible(
                    flex: 6,
                    fit: FlexFit.tight,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[700],
                              blurRadius: 2.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                1.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: Column(children: <Widget>[
                          Expanded(flex: 1, child: Center()),
                          Expanded(
                              flex: 3,
                              child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                      'Quase lá! Agora vamos adicionar os materiais requisitados.',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black,
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold)))),
                          Expanded(flex: 1, child: Center()),
                        ]))),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 25),
                          child: Container(
                              padding: EdgeInsets.all(15),
                              decoration: new BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[700],
                                    blurRadius: 2.0, // soften the shadow
                                    spreadRadius: 1.0, //extend the shadow
                                    offset: Offset(
                                      0.0, // Move to right 10  horizontally
                                      1.0, // Move to bottom 10 Vertically
                                    ),
                                  )
                                ],
                                color: Colors.white,
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(50.0),
                                    topRight: const Radius.circular(50.0),
                                    bottomLeft: const Radius.circular(50.0),
                                    bottomRight: const Radius.circular(50.0)),
                              ),
                              child: Container(
                                  margin: EdgeInsets.only(left: 5),
                                  child: GestureDetector(
                                      onTap: () {
                                        caixasUsadas = new List<Caixa>();
                                        cirurgia.materiais =
                                            new List<MaterialCaixa>();
                                        navigateToBuscaMateriaisRelato(context);
                                      },
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.grey[600],
                                        size: 34.0,
                                        semanticLabel: 'Advance',
                                      ))))),
                    )),
                Flexible(flex: 3, fit: FlexFit.tight, child: Center()),
              ]))
        ])));
  }
}
