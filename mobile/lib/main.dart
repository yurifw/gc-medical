import 'package:flutter/material.dart';
import 'package:gcmedicalapp/esqueceuSenha.dart';
import 'package:gcmedicalapp/login.dart';
import 'package:gcmedicalapp/mensagemFinalRelato.dart';
import 'package:gcmedicalapp/relatoCirurgia.dart';
import 'package:gcmedicalapp/tipoCirurgia.dart';
import 'package:gcmedicalapp/listaSolicitacoes.dart';
import 'package:gcmedicalapp/agendamentoCirurgia.dart';
import 'package:gcmedicalapp/agendamentoMensagem.dart';
import 'package:gcmedicalapp/visualizacaoCaixa.dart';
import 'package:gcmedicalapp/visualizacaoCaixaRelato.dart';
import 'package:gcmedicalapp/visualizacaoFinal.dart';
import 'package:gcmedicalapp/mensagemFinal.dart';
import 'package:gcmedicalapp/relatoMensagem.dart';
import 'agendamentoEdicao.dart';
import 'buscaMateriais.dart';
import 'buscaMateriaisEdicao.dart';
import 'buscaMateriaisRelato.dart';
import 'package:gcmedicalapp/visualizacaoFinalRelato.dart';
import 'package:gcmedicalapp/telaUpload.dart';
import 'classes/serviceLocator.dart';
import 'mensagemFinalEdicao.dart';
import 'resetarSenha.dart';
import 'visualizacaoCaixaEdicao.dart';
import 'visualizacaoFinalEdicao.dart';

void main() {
  setupServiceLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GC Medical',
      theme: ThemeData(
        inputDecorationTheme: const InputDecorationTheme(
            hintStyle: TextStyle(
        fontFamily: 'Montserrat-Semibold'),
        labelStyle: TextStyle(
        fontFamily: 'Montserrat'),),
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginPage(title: 'Social Protect')
    );
  }
}

Color hexToColor(String hexCode) {
    return new Color(int.parse(hexCode.substring(1, 7), radix: 16) + 0xFF000000);
}

Future navigateToLogin(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
}

Future navigateToBuscaMateriais(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => BuscaMateriaisPage()));
}

Future navigateToAgendamentoCirurgia(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => AgendamentoCirurgiaPage()));
}

Future navigateToAgendamentoEdicao(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => AgendamentoEdicaoPage()));
}

Future navigateToAgendamentoMensagem(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => AgendamentoMensagemPage()));
}

Future navigateToBuscaMateriaisOld(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => BuscaMateriaisPage()));
}

Future navigateToBuscaMateriaisEdicao(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => BuscaMateriaisEdicaoPage()));
}

Future navigateToListaSolicitacoes(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => ListaSolicitacoesPage()));
}

Future navigateToTipoCirurgia(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => TipoCirurgiaPage()));
}

Future navigateToVisualizacaoCaixa(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => VisualizacaoCaixaPage()));
}

Future navigateToVisualizacaoCaixaEdicao(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => VisualizacaoCaixaEdicaoPage()));
}

Future navigateToVisualizacaoFinal(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => VisualizacaoFinalPage()));
}

Future navigateToVisualizacaoFinalEdicao(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => VisualizacaoFinalEdicaoPage()));
}

Future navigateToMensagemFinal(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MensagemFinalPage()));
}

Future navigateToMensagemFinalEdicao(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MensagemFinalEdicaoPage()));
}

Future navigateToRelatoCirurgia(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => RelatoCirurgiaPage()));
}

Future navigateToRelatoMensagem(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => RelatoMensagemPage()));
}

Future navigateToBuscaMateriaisRelato(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => BuscaMateriaisRelatoPage()));
}

Future navigateToVisualizacaoCaixaRelato(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => VisualizacaoCaixaRelatoPage()));
}

Future navigateToVisualizacaoFinalRelato(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => VisualizacaoFinalRelatoPage()));
}

Future navigateToMensagemFinalRelato(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MensagemFinalRelatoPage()));
}

Future navigateToEsqueceuSenha(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => EsqueceuSenhaPage()));
}

Future navigateToResetarSenha(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => ResetarSenhaPage()));
}

Future navigateToTelaUpload(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => TelaUploadPage()));
}