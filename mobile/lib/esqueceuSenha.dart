import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:gcmedicalapp/main.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'globals.dart';

class EsqueceuSenhaPage extends StatefulWidget {
  EsqueceuSenhaPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _EsqueceuSenhaState createState() => _EsqueceuSenhaState();
}

final _emailController = TextEditingController();

class _EsqueceuSenhaState extends State<EsqueceuSenhaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG1.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                      child: Container(
                          alignment: Alignment.bottomCenter,
                          margin: EdgeInsets.only(bottom: 20),
                          child: Text('Esqueci minha senha',
                              style: TextStyle(
                                  fontFamily: 'Montserrat',
                                  color: Colors.white,
                                  fontSize: 28,
                                  fontWeight: FontWeight.bold))),
                    )),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                        child: Container(
                      height: 100,
                      padding: EdgeInsets.fromLTRB(10, 10, 25, 10),
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 20),
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[700],
                            blurRadius: 2.0, // soften the shadow
                            spreadRadius: 1.0, //extend the shadow
                            offset: Offset(
                              0.0, // Move to right 10  horizontally
                              1.0, // Move to bottom 10 Vertically
                            ),
                          )
                        ],
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0),
                            bottomLeft: const Radius.circular(10.0),
                            bottomRight: const Radius.circular(10.0)),
                      ),
                      child: Row(children: <Widget>[
                        Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Align(
                              alignment: Alignment.center,
                              child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: new BoxDecoration(
                                    color: Colors.grey[500],
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(30.0),
                                        topRight: const Radius.circular(30.0),
                                        bottomLeft: const Radius.circular(30.0),
                                        bottomRight:
                                            const Radius.circular(30.0)),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "!",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          color: Colors.white,
                                          fontSize: 34,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )),
                            )),
                        Padding(
                          padding: EdgeInsets.only(right: 10),
                        ),
                        Flexible(
                            flex: 3,
                            fit: FlexFit.tight,
                            child: Text(
                                'Identifique-se para receber um e-mail com as informações para trocar a sua senha.',
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic)))
                      ]),
                    ))),
                Flexible(
                    flex: 6,
                    fit: FlexFit.tight,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 30),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[700],
                              blurRadius: 2.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                1.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "E-mail",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.italic),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 5)),
                              Stack(children: <Widget>[
                                Container(
                                    padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
                                    child: TextFormField(
                                      controller: _emailController,
                                      decoration: new InputDecoration(
                                          contentPadding: EdgeInsets.fromLTRB(
                                              15, 15, 50, 15),
                                          filled: true,
                                          fillColor: Colors.grey[300],
                                          border: new OutlineInputBorder(
                                            borderRadius:
                                                const BorderRadius.all(
                                              const Radius.circular(19.0),
                                            ),
                                          ),
                                          hintText: "Insira o seu e-mail aqui"),
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontSize: 18.0),
                                      validator: (value) {
                                        return null;
                                      },
                                    )),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Container(
                                      margin: EdgeInsets.fromLTRB(0, 20, 20, 0),
                                      child: GestureDetector(
                                          onTap: () {
                                            _emailController.text = "";
                                          },
                                          child: Icon(
                                            Icons.close,
                                            size: 24.0,
                                            semanticLabel: 'Clear',
                                          ))),
                                )
                              ]),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 50,
                                  child: RaisedButton(
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(18.0),
                                          side: BorderSide(
                                              color: Colors.teal[300],
                                              width: 2)),
                                      onPressed: () {
                                        performEmailSend();
                                      },
                                      color: Colors.white,
                                      child: Stack(
                                        children: <Widget>[
                                          Center(
                                            child: Text("Enviar",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 22,
                                                    color: Colors.teal[300])),
                                          ),
                                          Align(
                                              alignment: Alignment.centerRight,
                                              child: Icon(
                                                Icons.arrow_forward_ios,
                                                color: Colors.teal[300],
                                                size: 20.0,
                                                semanticLabel: 'Advance',
                                              )),
                                        ],
                                      ))),
                            ]))),
                Flexible(flex: 3, fit: FlexFit.tight, child: Center()),
              ]))
        ])));
  }

  Future performEmailSend() async {
    if (_emailController.text == "") {
      _onBlankEmail(context);
    } else {
      String s = _emailController.text;
      var body = jsonEncode({"email": s.trim()});
      http
          .post(url + '/api/esqueci-senha',
              headers: {"Content-Type": "application/json"}, body: body)
          .then((http.Response response) {
        if (response.statusCode >= 200 && response.statusCode < 208) {
          _onEmailSent(context);
        }
      });
    }
  }

  _onBlankEmail(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "E-mail não preenchido!",
      desc: "O e-mail precisa ser informado.",
      buttons: [
        DialogButton(
          child: Text(
            "Entendi",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        ),
      ],
    ).show();
  }

  _onEmailSent(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "E-mail enviado!",
      desc:
          "Instruções para troca de senha foram enviadas para o e-mail informado. Caso não esteja na caixa de entrada, procure no Spam.",
      buttons: [
        DialogButton(
          child: Text(
            "Entendi",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            Navigator.pop(context);
            navigateToLogin(context);
          },
          width: 120,
        ),
      ],
    ).show();
  }
}
