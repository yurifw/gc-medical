import 'package:flutter/material.dart';
import 'package:gcmedicalapp/classes/materialCaixa.dart';
import 'package:gcmedicalapp/main.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'classes/caixa.dart';
import 'classes/itemDaCaixa.dart';
import 'globals.dart';

class VisualizacaoCaixaPage extends StatefulWidget {
  VisualizacaoCaixaPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _VisualizacaoCaixaState createState() => _VisualizacaoCaixaState();
}

List<Widget> list = new List();
int idCaixa;
String nomeCaixa;
int idProprietario;
String nomeText;

class _VisualizacaoCaixaState extends State<VisualizacaoCaixaPage> {
  @override
  void initState() {
    super.initState();
    list.clear();
    loadCaixaSelecionada();
  }

  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG2.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Stack(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(top: 20),
                                  width: 30,
                                  height: 30,
                                  decoration: new BoxDecoration(
                                    color: Colors.lightBlue[50],
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(18.0),
                                        topRight: const Radius.circular(18.0),
                                        bottomLeft: const Radius.circular(18.0),
                                        bottomRight:
                                            const Radius.circular(18.0)),
                                  ),
                                  child: GestureDetector(
                                      onTap: () {
                                        navigateToBuscaMateriais(context);
                                      },
                                      child: Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.black,
                                        size: 18.0,
                                        semanticLabel: 'Clear',
                                      ))),
                              Center(
                                child: Container(
                                    width:
                                        MediaQuery.of(context).size.width - 30,
                                    alignment: Alignment.center,
                                    child: Stack(
                                      children: [
                                        new Positioned(
                                          top: 2.0,
                                          left: 2.0,
                                          child: new Text(
                                            'Seleção de Materiais',
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.5),
                                                fontFamily: 'Montserrat',
                                                fontSize: 26,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        new Text('Seleção de Materiais',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                color: Colors.white,
                                                fontSize: 26,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    )),
                              )
                            ])
                          ],
                        ))),
                Flexible(
                    flex: 2,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          width: MediaQuery.of(context).size.width,
                          height: 60,
                          child: GestureDetector(
                              onTap: () {
                                finishBox();
                                navigateToBuscaMateriais(context);
                              },
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0),
                                    gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        Color.fromRGBO(203, 232, 228, 1),
                                        Colors.white
                                      ],
                                    ),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      Center(
                                        child: Text("Adicionar Caixa",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 24)),
                                      ),
                                    ],
                                  )))),
                    )),
                Flexible(
                    flex: 8,
                    fit: FlexFit.tight,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(15, 30, 10, 30),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.teal[300],
                              blurRadius: 0.0, // soften the shadow
                              spreadRadius: -8.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                20.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                          child: Column(children: <Widget>[
                            Container(
                              child: Column(children: <Widget>[
                                Center(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Flexible(
                                        child: Container(child: textWidget())),
                                    Padding(
                                      padding: EdgeInsets.only(right: 10),
                                    ),
                                    getWidget(),
                                    Padding(
                                      padding: EdgeInsets.only(right: 10),
                                    ),
                                    GestureDetector(
                                        onTap: () {
                                          navigateToBuscaMateriais(context);
                                        },
                                        child: Icon(
                                          Icons.delete,
                                          color: Colors.black,
                                          size: 30.0,
                                          semanticLabel: 'Delete',
                                        )),
                                  ],
                                )),
                                Column(children: list)
                              ]),
                            )
                          ]),
                        ))),
                Flexible(
                    flex: 2,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          width: MediaQuery.of(context).size.width,
                          height: 60,
                          child: GestureDetector(
                              onTap: () {
                                finishBox();
                                navigateToVisualizacaoFinal(context);
                              },
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0),
                                    gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        Color.fromRGBO(203, 232, 228, 1),
                                        Colors.white
                                      ],
                                    ),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      Center(
                                        child: Text("Finalizar",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 24)),
                                      ),
                                    ],
                                  )))),
                    )),
              ]))
        ])));
  }

  void loadCaixaSelecionada() {
    if (caixaSelecionada.nome == null) {
      nomeText = "";
    } else {
      nomeText = caixaSelecionada.nome;
    }
    List<Widget> sublist = new List();
    http.get(url + 'api/caixa/' + caixaSelecionada.id.toString(), headers: {
      "Content0-Type": "application/json",
      "Authorization": token
    }).then((http.Response response) {
      if (response.statusCode == 200) {
        var obj = json.decode(response.body)["data"];
        idCaixa = obj["id"];
        nomeCaixa = obj["nome"];
        idProprietario = obj["proprietario_id"];
        List<Map<String, dynamic>> cxs =
            obj["materiais"] != null ? List.from(obj["materiais"]) : null;
        for (int i = 0; i < cxs.length; i++) {
          var item = cxs[i];
          var idCaixa = item["caixa_id"];
          var idMaterial = item["material_id"];
          var idConteudo = item["conteudo_id"];
          var nomeMaterial = item["nome"];
          var qtd = item["quantidade"];
          sublist.add(new ItemDaCaixa(
            idCaixa: idCaixa,
            nomeMaterial: nomeMaterial,
            idConteudo: idConteudo,
            idMaterial: idMaterial,
            qtd: qtd,
            novaQuantidade: 0,
          ));
        }
      }

      this.setState(() {
        list = sublist;
      });
    });
  }

  void finishBox() {
    Caixa estaCaixa =
        new Caixa.fim(idCaixa, nomeCaixa, idProprietario, indexCaixa);

    caixasUsadas.add(estaCaixa);
    for (int i = 0; i < list.length; i++) {
      ItemDaCaixa t = list[i];
      if (t.novaQuantidade != 0) {
        MaterialCaixa m = new MaterialCaixa(
            t.idConteudo, t.novaQuantidade, t.nomeMaterial, indexCaixa);
        cirurgia.materiais.add(m);
      }
    }
  }

  textWidget() {
    if (nomeText.length > 22) {
      return Text(
        nomeText,
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontSize: 16,
            fontWeight: FontWeight.bold),
      );
    } else {
      return Text(
        nomeText,
        textAlign: TextAlign.center,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            fontFamily: 'Montserrat',
            fontSize: 18,
            fontWeight: FontWeight.bold),
      );
    }
  }

  getWidget() {
    if (caixaSelecionada.idProprietario == 1) {
      return Container(
          margin: EdgeInsets.only(left: 10),
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("img/Logo GC - Pag 12.png"),
              fit: BoxFit.fill,
            ),
          ));
    } else {
      return Container(margin: EdgeInsets.only(left: 10));
    }
  }
}
