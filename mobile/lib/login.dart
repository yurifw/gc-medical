import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:gcmedicalapp/main.dart';
import 'package:http/http.dart' as http;
import 'globals.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginState createState() => _LoginState();
}

var url = 'http://107.152.38.248/';
final _usernameController = TextEditingController();
final _passwordController = TextEditingController();

class _LoginState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG1.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(
                children: <Widget>[
                  Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Center(
                        child: Container(
                            margin: EdgeInsets.only(top: 35),
                            height: 60,
                            width: MediaQuery.of(context).size.width - 100,
                            child: Image.asset(
                              'img/Logo GC - Login.png',
                            ))),
                  ),
                  Flexible(
                      flex: 9,
                      fit: FlexFit.loose,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(15, 30, 15, 30),
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[700],
                              blurRadius: 2.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                1.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(left: 5),
                                  child: Text(
                                    "Nome de Usuário",
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        fontStyle: FontStyle.italic),
                                  )),
                              Padding(padding: EdgeInsets.only(bottom: 10)),
                              Stack(children: <Widget>[
                                Container(
                                    height: 60,
                                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                    child: TextFormField(
                                      controller: _usernameController,
                                      decoration: new InputDecoration(
                                          contentPadding: EdgeInsets.fromLTRB(
                                              15, 15, 50, 15),
                                          filled: true,
                                          fillColor: Colors.grey[300],
                                          border: new OutlineInputBorder(
                                            borderRadius:
                                                const BorderRadius.all(
                                              const Radius.circular(19.0),
                                            ),
                                          ),
                                          hintText:
                                              "Insira o seu nome de usuário aqui"),
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontSize: 18.0),
                                      validator: (value) {
                                        return null;
                                      },
                                    )),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: GestureDetector(
                                        onTap: () {
                                          _usernameController.text = "";
                                        },
                                        child: Container(
                                            margin: EdgeInsets.fromLTRB(
                                                0, 20, 20, 15),
                                            child: Icon(
                                              Icons.close,
                                              size: 24.0,
                                              semanticLabel: 'Clear',
                                            )))),
                              ]),
                              Container(
                                  margin: EdgeInsets.fromLTRB(5, 10, 0, 0),
                                  child: Text(
                                    "Senha",
                                    style: TextStyle(
                                        fontFamily: 'Montserrat',
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        fontStyle: FontStyle.italic),
                                  )),
                              Padding(padding: EdgeInsets.only(bottom: 10)),
                              Stack(children: <Widget>[
                                Container(
                                    height: 60,
                                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                    child: TextFormField(
                                      controller: _passwordController,
                                      obscureText: true,
                                      decoration: new InputDecoration(
                                          contentPadding: EdgeInsets.fromLTRB(
                                              15, 15, 50, 15),
                                          filled: true,
                                          fillColor: Colors.grey[300],
                                          border: new OutlineInputBorder(
                                            borderRadius:
                                                const BorderRadius.all(
                                              const Radius.circular(19.0),
                                            ),
                                          ),
                                          hintText: "Insira a sua senha aqui"),
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontSize: 18.0),
                                      validator: (value) {
                                        return null;
                                      },
                                    )),
                                Align(
                                    alignment: Alignment.centerRight,
                                    child: GestureDetector(
                                        onTap: () {
                                          _passwordController.text = "";
                                        },
                                        child: Container(
                                            margin: EdgeInsets.fromLTRB(
                                                0, 20, 20, 15),
                                            child: Icon(
                                              Icons.close,
                                              size: 24.0,
                                              semanticLabel: 'Clear',
                                            )))),
                              ]),
                              Container(
                                  margin: EdgeInsets.fromLTRB(0, 40, 0, 20),
                                  width: MediaQuery.of(context).size.width,
                                  height: 50,
                                  child: RaisedButton(
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(18.0),
                                          side: BorderSide(
                                            width: 1,
                                            color: hexToColor('#3fecc8'),
                                          )),
                                      onPressed: () {
                                        performLogin();
                                      },
                                      color: Colors.white,
                                      textColor: hexToColor('#3fecc8'),
                                      child: Stack(
                                        children: <Widget>[
                                          Center(
                                            child: Text("Entrar",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 24)),
                                          ),
                                          Align(
                                              alignment: Alignment.centerRight,
                                              child: Icon(
                                                Icons.arrow_forward_ios,
                                                color: Colors.teal[300],
                                                size: 20.0,
                                                semanticLabel: 'Advance',
                                              ))
                                        ],
                                      ))),
                              Container(
                                height:35,
                                margin: EdgeInsets.only(left: 5),
                                child: InkWell(
                                  onTap: () {
                                    navigateToEsqueceuSenha(context);
                                  },
                                  child: Text("Esqueci minha senha",
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontSize: 18,
                                          color: Colors.blue[800])),
                                ),
                              )
                            ])),
                      )),
                  Flexible(flex: 1, child: Center()),
                ],
              ))
        ])));
  }

  Future performLogin() async {
    if (_usernameController.text == "") {
      _onBlankUsername(context);
    } else if (_passwordController.text == "") {
      _onBlankPassword(context);
    } else {
      String s = _usernameController.text;
      var body = jsonEncode({
        "email": s.trim(),
        "senha": _passwordController.text
      });
      http
          .post(url + '/api/login',
              headers: {"Content-Type": "application/json"}, body: body)
          .then((http.Response response) {
        if (response.statusCode == 200) {
          var obj = json.decode(response.body);
          token = obj["data"];
          if (obj["data"] != null) {
            limpar = true;
            navigateToListaSolicitacoes(context);
          } else {
            _onLoginError(context);
          }
        }
      });
    }
  }

  _onBlankUsername(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Nome de usuário não preenchido!",
      desc: "O nome de usuário precisa ser informado.",
      buttons: [
        DialogButton(
          child: Text(
            "Entendi",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        ),
      ],
    ).show();
  }

  _onBlankPassword(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Senha não preenchida!",
      desc: "A senha precisa ser informada.",
      buttons: [
        DialogButton(
          child: Text(
            "Entendi",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        ),
      ],
    ).show();
  }

  _onLoginError(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Nome de usuário e/ou senha inválidos!",
      desc: "Seu cadastro não foi encontrado.",
      buttons: [
        DialogButton(
          child: Text(
            "Entendi",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        ),
      ],
    ).show();
  }
}
