class Medico {
  int id;
  String nome;

  Medico(this.id, this.nome);

  Medico.empty();

  Medico.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        nome = json['nome'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'nome': nome,
      };
}
