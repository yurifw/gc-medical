class ItemCaixa {
  int materialId;
  int conteudoId;
  String nome;
  int qtd;

  ItemCaixa(this.materialId, this.conteudoId, this.nome, this.qtd);

  ItemCaixa.empty();

  ItemCaixa.fromJson(Map<String, dynamic> json)
      : conteudoId = json["conteudo_id"],
        materialId = json["material_id"],
        nome = json['nome'],
        qtd = json["quantidade"];

  Map<String, dynamic> toJson() => {
        'conteudo_id': conteudoId,
        'material_id': materialId,
        'nome': nome,
        'quantidad': qtd
      };
}
