class MaterialCaixaEdicao {
  int conteudoCaixaId;
  int qtdSolicitada;
  String nome;
  int consumido;
  int indexCaixa;

  MaterialCaixaEdicao(
      this.conteudoCaixaId, this.qtdSolicitada, this.nome, this.consumido,this.indexCaixa);
  MaterialCaixaEdicao.empty();

  MaterialCaixaEdicao.fromJson(Map<String, dynamic> json) {
    indexCaixa = json["caixa_id"];
    conteudoCaixaId = json['conteudo_caixa_id'];
    qtdSolicitada = json['solicitado'];
    consumido = json['consumido'];
    nome = json["nome"];
  }

  Map<String, dynamic> toJson() => {
        'caixa_id': indexCaixa,
        'conteudo_caixa_id': conteudoCaixaId,
        'solicitado': qtdSolicitada,
        'consumido' : consumido,
        'nome': nome
      };

  @override
  String toString() {
    return 'conteudo_caixa_id: $conteudoCaixaId, solicitado: $qtdSolicitada, nome: $nome';
  }
}
