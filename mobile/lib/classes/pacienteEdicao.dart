class PacienteEdicao {
  String nome;
  String dataNascimento;
  int numeroPaciente;

  PacienteEdicao(this.nome, this.dataNascimento, this.numeroPaciente);

  PacienteEdicao.empty();

  PacienteEdicao.fromJson(Map<String, dynamic> json)
      : dataNascimento = json['nascimento'],
        nome = json['nome'],
        numeroPaciente = json["numero"];

  Map<String, dynamic> toJson() =>
      {'nascimento': dataNascimento, 'nome': nome, 'numero': numeroPaciente};
}
