import 'dart:io';

abstract class StorageService {
  Future<void> sendRequest(String cirurgia);
  Future<void> sendUpdate(String cirurgia);
  Future<void> sendRequestWithPhoto(String cirurgia, List<File> imagens);
}