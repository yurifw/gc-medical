class Caixa {
  int id;
  String nome;
  int idProprietario;
  int indexNaLista;

  Caixa(this.id, this.nome, this.idProprietario);

  Caixa.fim(this.id, this.nome, this.idProprietario, this.indexNaLista);

  Caixa.empty();

  @override
  String toString() {
    return '$nome';
  }
}
