import 'package:get_it/get_it.dart';
import 'storageServiceWeb.dart';
import 'storageService.dart';

GetIt locator = GetIt.instance;
setupServiceLocator() {
  locator.registerLazySingleton<StorageService>(() => StorageServiceWeb());
}