
import 'materialCaixaEdicao.dart';
import 'paciente.dart';
import 'tipo.dart';

class CirurgiaEdicao {
  Tipo tipo;
  String data; //data+hora
  String procedimento;
  String cid;
  Paciente paciente;
  int pagamentoRealizado;
  int idMedico;
  List<MaterialCaixaEdicao> materiais;
  String observacao;

  CirurgiaEdicao(this.tipo, this.data, this.procedimento, this.cid, this.pagamentoRealizado ,this.paciente, this.idMedico,
      this.materiais, this.observacao);
  CirurgiaEdicao.empty();

  CirurgiaEdicao.fromJson(Map<String, dynamic> json) {
    tipo = json['tipo'];
    data = json['data'];
    procedimento = json["procedimento"];
    idMedico = json["medico"];
    cid = json["cid"];
    pagamentoRealizado = json["pagamento_realizado"];
    paciente = json['paciente'];
    materiais = json["materiais"];
    observacao = json["observacao"];
  }

  Map<String, dynamic> toJson() => {
        'tipo': tipo,
        'data': data,
        'procedimento': procedimento,
        'cid': cid,
        'medico': idMedico,
        'pagamento_realizado': pagamentoRealizado,
        'paciente': paciente,
        'materiais':materiais,
        'observacao': observacao
      };
}
