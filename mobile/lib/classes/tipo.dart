class Tipo {
  int id; //1- urgência 2- eletiva
  String nome;
 
  Tipo(this.id, this.nome);
  
  Tipo.empty();

  Tipo.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        nome = json['nome'];

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'nome': nome,
    };
}