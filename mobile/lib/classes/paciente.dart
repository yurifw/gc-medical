class Paciente {
  String nome;
  String dataNascimento;
  int numeroPaciente;
  String sexo;
  String cidade;
 
  Paciente(this.nome, this.dataNascimento, this.numeroPaciente,this.sexo,this.cidade);
  
  Paciente.empty();

  Paciente.fromJson(Map<String, dynamic> json)
      : nome = json['nome'],
        dataNascimento = json['nascimento'],
        numeroPaciente = json["numero"],
        sexo = json["sexo"],
        cidade = json["cidade_residencia"]
      ;

  Map<String, dynamic> toJson() =>
    {
      'nome': nome,
      'nascimento': dataNascimento,
      'numero': numeroPaciente,
      'sexo' : sexo,
      'cidade_residencia' : cidade
    };
}