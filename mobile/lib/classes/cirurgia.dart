import 'package:gcmedicalapp/classes/tipo.dart';

import 'materialCaixa.dart';
import 'paciente.dart';

class Cirurgia {
  Tipo tipo;
  String data; //data+hora
  String procedimento;
  String cid;
  Paciente paciente;
  List<MaterialCaixa> materiais;
  int medico;
  String observacao;

  Cirurgia(this.tipo, this.data, this.procedimento, this.cid, this.paciente,
      this.materiais, this.medico, this.observacao);
  Cirurgia.empty();

  Cirurgia.fromJson(Map<String, dynamic> json) {
    tipo = json['tipo'];
    data = json['data'];
    procedimento = json["procedimento"];
    cid = json["cid"];
    paciente = json['paciente'];
    materiais = json["materiais"];
    medico = json["medico"];
    observacao = json["observacao"];
  }

  Map<String, dynamic> toJson() => {
        'tipo': tipo,
        'data': data,
        'procedimento': procedimento,
        'cid': cid,
        'paciente': paciente,
        'materiais': materiais,
        'medico': medico,
        'observacao': observacao
      };
}
