import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:gcmedicalapp/classes/storageService.dart';
import 'package:gcmedicalapp/globals.dart' as gl;
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' as http;

class StorageServiceWeb extends StorageService {
  @override
  Future<void> sendRequest(String cirurgia) async {
    var url = gl.url;
    var token = gl.token;
    print(cirurgia);
    Response response = await post(url + '/api/cirurgia',
        headers: {"Content-Type": "application/json", "Authorization": token},
        body: cirurgia);
    print(response.statusCode);
    if (response.statusCode >= 200 && response.statusCode < 208) {
      gl.sucessoAgendamento = true;
    }
  }

  @override
  Future<void> sendUpdate(String cirurgia) async {
    var url = gl.url;
    var token = gl.token;
    print(gl.idCirurgiaEdicao.toString());
    Response response = await patch(
        url + '/api/cirurgia/' + gl.idCirurgiaEdicao.toString(),
        headers: {"Content-Type": "application/json", "Authorization": token},
        body: cirurgia);
    print(response.statusCode);
    if (response.statusCode >= 200 && response.statusCode < 208) {
      gl.sucessoEdicao = true;
    }
  }

  @override
  Future<void> sendRequestWithPhoto(String cirurgia, List<File> imagens) async {
    var url = gl.url;
    var token = gl.token;
    int indexCirurgia = 0;

    Response response = await post(url + '/api/cirurgia',
        headers: {"Content-Type": "application/json", "Authorization": token},
        body: cirurgia);
    print(response.statusCode);
    if (response.statusCode >= 200 && response.statusCode < 205) {
      indexCirurgia = json.decode(response.body)["data"];
    }
    print('passou cadastro');
    if (response.statusCode >= 200 && response.statusCode < 205) {
      Map<String, String> headers = {"Authorization": token};
      var postUri = Uri.parse(url + '/api/documento');
      var request;
      for (int j = 0; j < imagens.length; j++) {
        print("Foto: " + j.toString());
        var stream =
            new ByteStream(new Stream.value(await imagens[j].readAsBytes()));
        var length = await imagens[j].length();
        var multipartFile =
            _copyMulti3(stream, length, "foto" + j.toString() + ".jpg");

        var nomeFoto = "foto" + j.toString() + ".jpg";
        request = new MultipartRequest("POST", postUri);
        request.headers.addAll(headers);
        request.fields['cirurgia_id'] = indexCirurgia.toString();
        request.fields['descricao'] = "fotos do relatório";
        request.fields['nome_exibicao'] = nomeFoto;
        request.files.add(multipartFile);
        var req = _copyRequest(request);
        var response2 = await req.send();
        print(response2.statusCode);
      }
      gl.sucessoRelato = true;
    }
  }

  BaseRequest _copyRequest(MultipartRequest request) {
    MultipartRequest requestCopy;

    requestCopy = MultipartRequest(request.method, request.url)
      ..fields.addAll(request.fields)
      ..files.addAll(request.files);

    requestCopy
      ..persistentConnection = request.persistentConnection
      ..followRedirects = request.followRedirects
      ..maxRedirects = request.maxRedirects
      ..headers.addAll(request.headers);

    return requestCopy;
  }

  MultipartFile _copyMulti3(Stream stream, int length, String nome) {
    MultipartFile m1;
    m1 = new http.MultipartFile(
      'documento',
      stream,
      length,
      filename: nome,
      contentType: new MediaType('image', 'jpg'),
    );
    return m1;
  }
}
