class CirurgiaJsonEdicao {
  var tipo;
  String data; //data+hora
  String procedimento;
  String cid;
  int pagamentoRealizado;
  var paciente;
  var materiais;
  String observacao;
  var medico;

  CirurgiaJsonEdicao(this.tipo, this.data, this.procedimento, this.cid, this.pagamentoRealizado, this.paciente,
      this.materiais, this.observacao, this.medico);
  CirurgiaJsonEdicao.empty();

  CirurgiaJsonEdicao.fromJson(Map<String, dynamic> json) {
    tipo = json['tipo'];
    data = json['data'];
    procedimento = json["procedimento"];
    cid = json["cid"];
    pagamentoRealizado = json["pagamento_realizado"];
    paciente = json['paciente'];
    materiais = json["materiais"];
    observacao = json["observacao"];
    medico = json["medico"];
  }

  Map<String, dynamic> toJson() => {
        'tipo': tipo,
        'data': data,
        'procedimento': procedimento,
        'cid': cid,
        'pagamento_realizado': pagamentoRealizado,
        'paciente': paciente,
        'materiais':materiais,
        'observacao': observacao,
        'medico':medico
      };
}
