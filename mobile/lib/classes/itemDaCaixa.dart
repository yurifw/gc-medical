import 'package:flutter/material.dart';

class ItemDaCaixa extends StatefulWidget {
  final int idCaixa;
  final int idMaterial;
  final int idConteudo;
  final String nomeMaterial;
  final int qtd;
  int novaQuantidade;

  ItemDaCaixa(
      {Key key, this.idCaixa,this.idConteudo, this.idMaterial, this.nomeMaterial, this.qtd, this.novaQuantidade})
      : super(key: key);

  @override
  _ItemDaCaixaState createState() => _ItemDaCaixaState();
}

class _ItemDaCaixaState extends State<ItemDaCaixa> {
  int newQtd;
  int maxQtd;

  @override
  void initState() {
    super.initState();
    this.newQtd = 0;
    this.maxQtd = this.widget.qtd;
    this.widget.novaQuantidade = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Padding(padding: EdgeInsets.only(top: 15)),
      Row(children: <Widget>[
        Flexible(
            flex: 16,
            fit: FlexFit.tight,
            child: Container(
              decoration: new BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(15.0),
                      topRight: const Radius.circular(15.0),
                      bottomLeft: const Radius.circular(15.0),
                      bottomRight: const Radius.circular(15.0))),
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              child: Text(widget.nomeMaterial,
                  overflow: TextOverflow.clip,
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 14,
                      fontWeight: FontWeight.bold)),
            )),
        Flexible(flex: 1, fit: FlexFit.tight, child: Text("")),
        Flexible(
            flex: 2,
            fit: FlexFit.loose,
            child: Container(
                width: 30,
                child: GestureDetector(
                    onTap: () {
                      this._subtractQuantity();
                    },
                    child: Icon(
                      Icons.remove,
                      color: Colors.black,
                      size: 30.0,
                      semanticLabel: 'Subtract',
                    )))),
        Padding(
          padding: EdgeInsets.only(right: 3),
        ),
        Flexible(
            flex: 3,
            fit: FlexFit.loose,
            child: Container(
              width: 50,
              decoration: new BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(15.0),
                      topRight: const Radius.circular(15.0),
                      bottomLeft: const Radius.circular(15.0),
                      bottomRight: const Radius.circular(15.0))),
              padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
              child: Text(newQtd.toString(),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontSize: 18,
                      fontWeight: FontWeight.bold)),
            )),
        Padding(
          padding: EdgeInsets.only(right: 3),
        ),
        Flexible(
          flex: 2,
          fit: FlexFit.loose,
          child: Container(
              width: 30,
              child: GestureDetector(
                  onTap: () {
                    this._addQuantity();
                  },
                  child: Icon(
                    Icons.add,
                    color: Colors.black,
                    size: 30.0,
                    semanticLabel: 'Add',
                  ))),
        ),
      ])
    ]);
  }

  void _addQuantity() {
    this.setState(() {
      if (this.newQtd < this.maxQtd) {
        this.newQtd++;
        this.widget.novaQuantidade = newQtd;
      }
    });
  }

  void _subtractQuantity() {
    this.setState(() {
      if (this.newQtd > 0) {
        this.newQtd--;
        this.widget.novaQuantidade = newQtd;
      }
    });
  }
}
