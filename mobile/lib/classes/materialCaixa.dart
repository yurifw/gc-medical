class MaterialCaixa {
  int conteudoCaixaId;
  int qtdSolicitada;
  String nome;
  int indexCaixa;

  MaterialCaixa(
      this.conteudoCaixaId, this.qtdSolicitada, this.nome, this.indexCaixa);
  MaterialCaixa.empty();

  MaterialCaixa.fromJson(Map<String, dynamic> json) {
    indexCaixa = json["caixa_id"];
    conteudoCaixaId = json['conteudo_caixa_id'];
    qtdSolicitada = json['solicitado'];
    nome = json["nome"];
  }

  Map<String, dynamic> toJson() => {
        'caixa_id': indexCaixa,
        'conteudo_caixa_id': conteudoCaixaId,
        'solicitado': qtdSolicitada,
        'nome': nome
      };

  @override
  String toString() {
    return 'conteudo_caixa_id: $conteudoCaixaId, solicitado: $qtdSolicitada, nome: $nome';
  }
}
