class CirurgiaJson {
  var tipo;
  String data; //data+hora
  String procedimento;
  String cid;
  var paciente;
  var materiais;
  int medico;
  String observacao;

  CirurgiaJson(this.tipo, this.data, this.procedimento, this.cid, this.paciente,
      this.materiais, this.medico,this.observacao);
  CirurgiaJson.empty();

  CirurgiaJson.fromJson(Map<String, dynamic> json) {
    tipo = json['tipo'];
    data = json['data'];
    procedimento = json["procedimento"];
    cid = json["cid"];
    paciente = json['paciente'];
    materiais = json["materiais"];
    medico = json["medico"];
    observacao = json["observacao"];
  }

  Map<String, dynamic> toJson() => {
        'tipo': tipo,
        'data': data,
        'procedimento': procedimento,
        'cid': cid,
        'paciente': paciente,
        'materiais':materiais,
        'medico':medico,
        'observacao': observacao
      };
}
