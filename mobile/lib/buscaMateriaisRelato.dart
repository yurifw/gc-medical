import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gcmedicalapp/main.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'classes/caixa.dart';
import 'globals.dart';

class BuscaMateriaisRelatoPage extends StatefulWidget {
  BuscaMateriaisRelatoPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _BuscaMateriaisRelatoState createState() => _BuscaMateriaisRelatoState();
}

class _BuscaMateriaisRelatoState extends State<BuscaMateriaisRelatoPage> {
  @override
  void initState() {
    super.initState();
    habilitado.value = 0;
    loadCaixas();
    caixaSelecionada = Caixa.empty();
  }

  List<Caixa> _searchResult = [];
  ValueNotifier<int> habilitado = ValueNotifier<int>(0);
  List<Widget> items = [];
  TextEditingController controller = new TextEditingController();
  Timer _debounce;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG2.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 30,
                              height: 30,
                              decoration: new BoxDecoration(
                                color: Colors.lightBlue[50],
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(18.0),
                                    topRight: const Radius.circular(18.0),
                                    bottomLeft: const Radius.circular(18.0),
                                    bottomRight: const Radius.circular(18.0)),
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    navigateToRelatoMensagem(context);
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black,
                                    size: 18.0,
                                    semanticLabel: 'Clear',
                                  ))),
                        ])),
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Center(
                        child: Container(
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 30),
                          child: Stack(
                            children: [
                              new Positioned(
                                top: 2.0,
                                left: 2.0,
                                child: new Text(
                                  'Relato de Materiais',
                                  style: TextStyle(
                                      color: Colors.black.withOpacity(0.5),
                                      fontFamily: 'Montserrat',
                                      fontSize: 26,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              new Text('Relato de Materiais',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Montserrat',
                                      color: Colors.white,
                                      fontSize: 26,
                                      fontWeight: FontWeight.bold)),
                            ],
                          )),
                    ))),
                Flexible(
                    flex: 16,
                    fit: FlexFit.tight,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.teal[300],
                              blurRadius: 0.0, // soften the shadow
                              spreadRadius: -8.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                20.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: Column(children: <Widget>[
                          Flexible(
                              flex: 3,
                              child: Column(children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  alignment: Alignment.center,
                                  child: Text(
                                      'Escolha a caixa pelo seu nome ou pelo item',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: 'Montserrat',
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold)),
                                ),
                                Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                        color: Colors.grey,
                                        width: 2,
                                      ),
                                      borderRadius: new BorderRadius.only(
                                          topLeft: const Radius.circular(8.0),
                                          topRight: const Radius.circular(8.0),
                                          bottomLeft:
                                              const Radius.circular(8.0),
                                          bottomRight:
                                              const Radius.circular(8.0)),
                                    ),
                                    height: 48,
                                    margin: EdgeInsets.only(top: 8),
                                    padding: EdgeInsets.all(8),
                                    width: MediaQuery.of(context).size.width,
                                    child: Row(
                                      children: <Widget>[
                                        Flexible(
                                            flex: 1,
                                            fit: FlexFit.tight,
                                            child: Icon(
                                              Icons.search,
                                              color: Colors.grey,
                                            )),
                                        Flexible(
                                            flex: 8,
                                            fit: FlexFit.tight,
                                            child: Container(
                                                alignment: Alignment.center,
                                                child: TextFormField(
                                                  controller: controller,
                                                  style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 18),
                                                  decoration:
                                                      InputDecoration.collapsed(
                                                    hintText: 'Digite aqui',
                                                  ).copyWith(isDense: true),
                                                  onChanged:
                                                      onSearchTextChanged,
                                                ))),
                                        Flexible(
                                            flex: 1,
                                            child: Container(
                                                child: GestureDetector(
                                              onTap: () {
                                                controller.clear();
                                                onSearchTextChanged('');
                                              },
                                              child: Icon(
                                                Icons.cancel,
                                                color: Colors.grey,
                                              ),
                                            ))),
                                      ],
                                    )),
                                Flexible(
                                    flex: 3,
                                    fit: FlexFit.tight,
                                    child: SingleChildScrollView(
                                      child: items.length > 0
                                          ? Column(children: <Widget>[
                                              Container(
                                                  child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          bottom: 10),
                                                      decoration:
                                                          new BoxDecoration(
                                                        border: Border.all(
                                                          color: Colors.grey,
                                                          width: 2,
                                                        ),
                                                        borderRadius: new BorderRadius
                                                                .only(
                                                            topLeft: const Radius
                                                                .circular(8.0),
                                                            topRight: const Radius
                                                                .circular(8.0),
                                                            bottomLeft:
                                                                const Radius
                                                                        .circular(
                                                                    8.0),
                                                            bottomRight:
                                                                const Radius
                                                                        .circular(
                                                                    8.0)),
                                                      ),
                                                      child: Column(
                                                        children: <Widget>[
                                                          Container(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .fromLTRB(
                                                                      12,
                                                                      8,
                                                                      0,
                                                                      0),
                                                              child:
                                                                  SingleChildScrollView(
                                                                child: Column(
                                                                    children: <
                                                                        Widget>[
                                                                      Container(
                                                                        child: Column(
                                                                            children:
                                                                                items),
                                                                      )
                                                                    ]),
                                                              ))
                                                        ],
                                                      ),
                                                    )
                                                  ]))
                                            ])
                                          : Center(),
                                    ))
                              ]))
                        ]))),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 25),
                          child: Container(
                              padding: EdgeInsets.all(15),
                              decoration: new BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey[700],
                                    blurRadius: 2.0, // soften the shadow
                                    spreadRadius: 1.0, //extend the shadow
                                    offset: Offset(
                                      0.0, // Move to right 10  horizontally
                                      1.0, // Move to bottom 10 Vertically
                                    ),
                                  )
                                ],
                                color: Colors.white,
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(50.0),
                                    topRight: const Radius.circular(50.0),
                                    bottomLeft: const Radius.circular(50.0),
                                    bottomRight: const Radius.circular(50.0)),
                              ),
                              child: ValueListenableBuilder(
                                builder: (BuildContext context, int value,
                                    Widget child) {
                                  return Container(
                                      margin: EdgeInsets.only(left: 5),
                                      child: value > 0
                                          ? GestureDetector(
                                              onTap: () {
                                                indexCaixa = indexCaixa + 1;
                                                caixaSelecionada.indexNaLista =
                                                    indexCaixa;
                                                navigateToVisualizacaoCaixaRelato(
                                                    context);
                                              },
                                              child: Icon(
                                                Icons.arrow_forward_ios,
                                                color: Colors.grey[600],
                                                size: 34.0,
                                                semanticLabel: 'Advance',
                                              ))
                                          : GestureDetector(
                                              onTap: () {},
                                              child: Icon(
                                                Icons.arrow_forward_ios,
                                                color: Colors.grey[300],
                                                size: 34.0,
                                                semanticLabel: 'Advance',
                                              )));
                                },
                                valueListenable: habilitado,
                              ))),
                    )),
              ]))
        ])));
  }

  Future loadCaixas() async {
    List<Caixa> sublist = new List();
    http.get(url + '/api/caixas/a', headers: {
      "Content0-Type": "application/json",
      "Authorization": token
    }).then((http.Response response) {
      if (response.statusCode == 200) {
        var obj = json.decode(response.body)["data"];
        List<Map<String, dynamic>> cxs = obj != null ? List.from(obj) : null;
        for (int i = 0; i < cxs.length; i++) {
          var item = cxs[i];

          var idCaixa = item["id"];
          var nomeCaixa = item["nome"];
          var proprietario = item["proprietario_id"];
          Caixa c = new Caixa(idCaixa, nomeCaixa, proprietario);
          sublist.add(c);
        }
      }

      this.setState(() {
        items = sublist
            .map((item) => new Container(
                margin: EdgeInsets.fromLTRB(0, 0, 5, 10),
                child: GestureDetector(
                    onTap: () {
                      int ant = habilitado.value;
                      ant = ant + 1;
                      habilitado.value = ant;
                      controller.text = item.nome;
                      caixaSelecionada.nome = item.nome;
                      caixaSelecionada.id = item.id;
                      caixaSelecionada.idProprietario = item.idProprietario;
                    },
                    child: Row(children: <Widget>[
                      Flexible(
                          flex: 6,
                          fit: FlexFit.tight,
                          child: Text(
                            item.nome,
                            style: TextStyle(
                                fontFamily: 'Montserrat', fontSize: 18),
                          )),
                      if (item.idProprietario == 1)
                        Flexible(
                            flex: 1,
                            child: Container(
                                margin: EdgeInsets.only(left: 10),
                                height: 24,
                                width: 24,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image:
                                        AssetImage("img/Logo GC - Pag 12.png"),
                                    fit: BoxFit.fill,
                                  ),
                                )))
                      else
                        Flexible(
                            flex: 1,
                            child: Padding(padding: EdgeInsets.only(left: 10)))
                    ]))))
            .toList();
      });
    });
  }

  Future onSearchTextChanged(String text) async {
    List<Widget> newList = new List<Widget>();
    _searchResult.clear();
    if (habilitado.value > 0) {
      habilitado.value = 0;
    }

    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 1500), () {
      if (text.isEmpty) {
        items.clear();
        _searchResult.clear();
        http.get(url + '/api/caixas/a', headers: {
          "Content0-Type": "application/json",
          "Authorization": token
        }).then((http.Response response) {
          if (response.statusCode == 200) {
            var obj = json.decode(response.body)["data"];
            List<Map<String, dynamic>> cxs =
                obj != null ? List.from(obj) : null;
            for (int i = 0; i < cxs.length; i++) {
              var item = cxs[i];

              var idCaixa = item["id"];
              var nomeCaixa = item["nome"];
              var proprietario = item["proprietario_id"];
              Caixa c = new Caixa(idCaixa, nomeCaixa, proprietario);
              _searchResult.add(c);
            }
            newList = _searchResult
                .map((item) => new Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: GestureDetector(
                        onTap: () {
                          int ant = habilitado.value;
                          ant = ant + 1;
                          habilitado.value = ant;
                          controller.text = item.nome;
                          caixaSelecionada.nome = item.nome;
                          caixaSelecionada.id = item.id;
                          caixaSelecionada.idProprietario = item.idProprietario;
                        },
                        child: Row(children: <Widget>[
                          Flexible(
                              flex: 4,
                              fit: FlexFit.tight,
                              child: Text(
                                item.nome,
                                style: TextStyle(
                                    fontFamily: 'Montserrat', fontSize: 18),
                              )),
                          if (item.idProprietario == 1)
                            Flexible(
                                flex: 1,
                                child: Container(
                                    margin: EdgeInsets.only(left: 10),
                                    height: 24,
                                    width: 24,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "img/Logo GC - Pag 12.png"),
                                        fit: BoxFit.fill,
                                      ),
                                    )))
                          else
                            Flexible(
                                flex: 1,
                                child:
                                    Padding(padding: EdgeInsets.only(left: 10)))
                        ]))))
                .toList();
            setState(() {
              items = newList;
            });
          }
        });

        return;
      }

      if (text.length >= 3) {
        items.clear();
        _searchResult.clear();
        http.get(url + '/api/caixas/' + text, headers: {
          "Content0-Type": "application/json",
          "Authorization": token
        }).then((http.Response response) {
          if (response.statusCode == 200) {
            var obj = json.decode(response.body)["data"];
            List<Map<String, dynamic>> cxs =
                obj != null ? List.from(obj) : null;
            for (int i = 0; i < cxs.length; i++) {
              var item = cxs[i];

              var idCaixa = item["id"];
              var nomeCaixa = item["nome"];
              var proprietario = item["proprietario_id"];
              Caixa c = new Caixa(idCaixa, nomeCaixa, proprietario);
              _searchResult.add(c);
            }
            newList = _searchResult
                .map((item) => new Container(
                    margin: EdgeInsets.only(bottom: 5),
                    child: GestureDetector(
                        onTap: () {
                          int ant = habilitado.value;
                          ant = ant + 1;
                          habilitado.value = ant;
                          controller.text = item.nome;
                          caixaSelecionada.nome = item.nome;
                          caixaSelecionada.id = item.id;
                          caixaSelecionada.idProprietario = item.idProprietario;
                        },
                        child: Row(children: <Widget>[
                          Flexible(
                              flex: 4,
                              fit: FlexFit.tight,
                              child: Text(
                                item.nome,
                                style: TextStyle(
                                    fontFamily: 'Montserrat', fontSize: 18),
                              )),
                          if (item.idProprietario == 1)
                            Flexible(
                                flex: 1,
                                child: Container(
                                    margin: EdgeInsets.only(left: 10),
                                    height: 24,
                                    width: 24,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "img/Logo GC - Pag 12.png"),
                                        fit: BoxFit.fill,
                                      ),
                                    )))
                          else
                            Flexible(
                                flex: 1,
                                child:
                                    Padding(padding: EdgeInsets.only(left: 10)))
                        ]))))
                .toList();
            setState(() {
              items = newList;
            });
          }
        });
      }
    });
  }
}
