import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:gcmedicalapp/main.dart';
import 'classes/materialCaixaEdicao.dart';
import 'globals.dart';

class VisualizacaoFinalEdicaoPage extends StatefulWidget {
  VisualizacaoFinalEdicaoPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _VisualizacaoFinalEdicaoState createState() =>
      _VisualizacaoFinalEdicaoState();
}

List<Widget> list = new List();
bool habilitado = false;

class _VisualizacaoFinalEdicaoState extends State<VisualizacaoFinalEdicaoPage> {
  @override
  void initState() {
    super.initState();
    list.clear();

    if (cirurgiaEdicao.materiais.length > 0) {
      habilitado = true;
    } else {
      habilitado = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG2.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Center(
                        child: Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Stack(children: <Widget>[
                                  Container(
                                      margin: EdgeInsets.only(top: 20),
                                      width: 30,
                                      height: 30,
                                      decoration: new BoxDecoration(
                                        color: Colors.lightBlue[50],
                                        borderRadius: new BorderRadius.only(
                                            topLeft:
                                                const Radius.circular(18.0),
                                            topRight:
                                                const Radius.circular(18.0),
                                            bottomLeft:
                                                const Radius.circular(18.0),
                                            bottomRight:
                                                const Radius.circular(18.0)),
                                      ),
                                      child: GestureDetector(
                                          onTap: () {
                                            navigateToAgendamentoEdicao(
                                                context);
                                          },
                                          child: Icon(
                                            Icons.arrow_back_ios,
                                            color: Colors.black,
                                            size: 18.0,
                                            semanticLabel: 'Clear',
                                          ))),
                                  Center(
                                    child: Container(
                                        width:
                                            MediaQuery.of(context).size.width -
                                                30,
                                        alignment: Alignment.center,
                                        child: Stack(
                                          children: [
                                            new Positioned(
                                              top: 2.0,
                                              left: 2.0,
                                              child: new Text(
                                                'Seleção de Materiais',
                                                style: TextStyle(
                                                    color: Colors.black
                                                        .withOpacity(0.5),
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 26,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                            new Text('Seleção de Materiais',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    color: Colors.white,
                                                    fontSize: 26,
                                                    fontWeight:
                                                        FontWeight.bold)),
                                          ],
                                        )),
                                  )
                                ])
                              ],
                            )))),
                Flexible(
                    flex: 2,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          width: MediaQuery.of(context).size.width,
                          height: 60,
                          child: GestureDetector(
                              onTap: () {
                                limpar = true;
                                navigateToBuscaMateriaisEdicao(context);
                              },
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0),
                                    gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        Color.fromRGBO(203, 232, 228, 1),
                                        Colors.white
                                      ],
                                    ),
                                  ),
                                  child: Stack(
                                    children: <Widget>[
                                      Center(
                                        child: Text("Adicionar Caixa",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                fontSize: 24)),
                                      ),
                                    ],
                                  )))),
                    )),
                Flexible(
                    flex: 9,
                    fit: FlexFit.loose,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(20, 10, 10, 20),
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.teal[300],
                              blurRadius: 0.0, // soften the shadow
                              spreadRadius: -8.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                20.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                              Container(
                                  height: MediaQuery.of(context).size.height,
                                  child: new ListView(
                                      children: new List.generate(
                                          caixasEdicao.length, (int index) {
                                    return Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: Row(
                                        children: <Widget>[
                                          Flexible(
                                              flex: 10,
                                              fit: FlexFit.tight,
                                              child: Container(
                                                  child: Text(
                                                caixasEdicao[index].nome,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ))),
                                          Flexible(
                                              flex: 2,
                                              child: containerWidget(
                                                  caixasEdicao[index])),
                                          Flexible(
                                              flex: 1,
                                              child: Container(
                                                margin:
                                                    EdgeInsets.only(left: 5),
                                                child: GestureDetector(
                                                    onTap: () {
                                                      setState(() {
                                                        caixasEdicao
                                                            .removeAt(index);
                                                      });
                                                    },
                                                    child: Icon(
                                                      Icons.delete,
                                                      color: Colors.black,
                                                      size: 30.0,
                                                      semanticLabel: 'Delete',
                                                    )),
                                              ))
                                        ],
                                      ),
                                    );
                                  })))
                            ])))),
                Flexible(
                    flex: 2,
                    child: Center(
                      child: Container(
                          margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
                          width: MediaQuery.of(context).size.width,
                          height: 60,
                          child: habilitado
                              ? GestureDetector(
                                  onTap: () {
                                    navigateToMensagemFinalEdicao(context);
                                  },
                                  child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                        gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            Color.fromRGBO(203, 232, 228, 1),
                                            Colors.white
                                          ],
                                        ),
                                      ),
                                      child: Stack(
                                        children: <Widget>[
                                          Center(
                                            child: Text("Confirmar Material",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontFamily: 'Montserrat',
                                                    fontSize: 24)),
                                          ),
                                        ],
                                      )))
                              : GestureDetector(
                                  onTap: () {},
                                  child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                        color: Colors.white,
                                      ),
                                      child: Center(
                                        child: Text("Confirmar Material",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.grey[600],
                                                fontFamily: 'Montserrat',
                                                fontSize: 24)),
                                      )))),
                    )),
              ]))
        ])));
  }

  void removeItems(index) {
    List<MaterialCaixaEdicao> listMateriais = new List<MaterialCaixaEdicao>();
    listMateriais.clear();
    int t = cirurgiaEdicao.materiais.length;
    for (int i = 0; i < t; i++) {
      if (cirurgiaEdicao.materiais[i].indexCaixa != index) {
        listMateriais.add((cirurgiaEdicao.materiais[i]));
      }
    }
    cirurgiaEdicao.materiais.clear();
    cirurgiaEdicao.materiais = listMateriais;
    if (cirurgiaEdicao.materiais.length > 0) {
      habilitado = true;
    } else {
      habilitado = false;
    }
  }

  containerWidget(item) {
    if (item.idProprietario == 1) {
      return Container(
          margin: EdgeInsets.only(left: 10),
          height: 30,
          width: 30,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("img/Logo GC - Pag 12.png"),
              fit: BoxFit.fill,
            ),
          ));
    } else {
      return Container(
        margin: EdgeInsets.only(left: 10),
        height: 30,
        width: 30,
      );
    }
  }
}
