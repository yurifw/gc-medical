import 'package:flutter/material.dart';
import 'package:gcmedicalapp/main.dart';
import 'package:gcmedicalapp/classes/tipo.dart';
import 'package:gcmedicalapp/classes/cirurgia.dart';

import 'globals.dart';

class TipoCirurgiaPage extends StatefulWidget {
  TipoCirurgiaPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _TipoCirurgiaState createState() => _TipoCirurgiaState();
}

class _TipoCirurgiaState extends State<TipoCirurgiaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG1.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top:20),
                              width: 30,
                              height: 30,
                              decoration: new BoxDecoration(
                                color: Colors.lightBlue[50],
                                borderRadius: new BorderRadius.only(
                                    topLeft: const Radius.circular(18.0),
                                    topRight: const Radius.circular(18.0),
                                    bottomLeft: const Radius.circular(18.0),
                                    bottomRight: const Radius.circular(18.0)),
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    navigateToListaSolicitacoes(context);
                                  },
                                  child: Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.black,
                                        size: 18.0,
                                        semanticLabel: 'Clear',
                                      ))),
                        ])),
                Flexible(
                    flex: 3,
                    fit: FlexFit.tight,
                    child: Center(
                      child: Container(
                          alignment: Alignment.bottomCenter,
                          margin: EdgeInsets.only(bottom: 20),
                          child: Stack(
                                    children: [
                                      new Positioned(
                                        top: 2.0,
                                        left: 2.0,
                                        child: new Text(
                                          'Tipo de Cirurgia',
                                          style: TextStyle(
                                              color:
                                                  Colors.black.withOpacity(0.5),
                                              fontFamily: 'Montserrat',
                                              fontSize: 28,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      new Text('Tipo de Cirurgia',
                                          style: TextStyle(
                                              fontFamily: 'Montserrat',
                                              color: Colors.white,
                                              fontSize: 28,
                                              fontWeight: FontWeight.bold)),
                                    ],
                                  )),
                    )),
                Flexible(
                    flex: 8,
                    fit: FlexFit.tight,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[700],
                              blurRadius: 2.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                1.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                            child: Column(children: <Widget>[
                          Container(
                            height: 300,
                            child: Column(children: <Widget>[
                              Padding(padding: EdgeInsets.only(top: 70)),
                              Flexible(
                                  flex: 2,
                                  child: Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      width: MediaQuery.of(context).size.width,
                                      height: 60,
                                      child: RaisedButton(
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      20.0),
                                              side: BorderSide(
                                                  color: hexToColor('#3fecc8'),
                                                  width: 1)),
                                          onPressed: () {
                                            chooseType('urgencia');
                                          },
                                          color: Colors.white,
                                          textColor: Colors.black,
                                          child: Stack(
                                            children: <Widget>[
                                              Center(
                                                child: Text("URGÊNCIA",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontFamily:
                                                            'Montserrat',
                                                        fontSize: 24,
                                                        color: hexToColor(
                                                            '#3fecc8'))),
                                              ),
                                              Align(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Icon(
                                                    Icons.arrow_forward_ios,
                                                    color:
                                                        hexToColor('#3fecc8'),
                                                    size: 24.0,
                                                    semanticLabel: 'Advance',
                                                  )),
                                            ],
                                          )))),
                              Padding(padding: EdgeInsets.only(top: 55)),
                              Flexible(
                                  flex: 2,
                                  child: Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      width: MediaQuery.of(context).size.width,
                                      height: 60,
                                      child: RaisedButton(
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      20.0),
                                              side: BorderSide(
                                                  color: hexToColor('#3fecc8'),
                                                  width: 1)),
                                          onPressed: () {
                                            chooseType('eletiva');
                                          },
                                          color: Colors.white,
                                          textColor: Colors.black,
                                          child: Stack(
                                            children: <Widget>[
                                              Center(
                                                child: Text("ELETIVA",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                      fontFamily: 'Montserrat',
                                                      fontSize: 24,
                                                      color:
                                                          hexToColor('#3fecc8'),
                                                    )),
                                              ),
                                              Align(
                                                  alignment:
                                                      Alignment.centerRight,
                                                  child: Icon(
                                                    Icons.arrow_forward_ios,
                                                    color:
                                                        hexToColor('#3fecc8'),
                                                    size: 24.0,
                                                    semanticLabel: 'Advance',
                                                  )),
                                            ],
                                          )))),
                              Padding(padding: EdgeInsets.only(top: 60)),
                            ]),
                          )
                        ])))),
                Flexible(flex: 4, fit: FlexFit.tight, child: Center()),
              ]))
        ])));
  }

  void chooseType(tipo) {
    limpar = true;
    if (tipo == "eletiva") {
      Tipo t = new Tipo(2, "eletiva");
      cirurgia = new Cirurgia.empty();
      cirurgia.tipo = t;
      navigateToAgendamentoCirurgia(context);
    } else {
      Tipo t = new Tipo(1, "urgência");
      cirurgia = new Cirurgia.empty();
      cirurgia.tipo = t;
      navigateToRelatoCirurgia(context);
    }
  }
}
