import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:gcmedicalapp/classes/cirurgiaEdicao.dart';
import 'package:gcmedicalapp/main.dart';
import 'package:http/http.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:masked_text/masked_text.dart';
import 'classes/caixa.dart';
import 'classes/materialCaixaEdicao.dart';
import 'classes/medico.dart';
import 'classes/paciente.dart';
import 'classes/tipo.dart';
import 'globals.dart';

class AgendamentoEdicaoPage extends StatefulWidget {
  AgendamentoEdicaoPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AgendamentoEdicaoState createState() => _AgendamentoEdicaoState();
}

final _nomePacienteController = TextEditingController();
final _dataNascimentoController = TextEditingController();
final _numeroPacientecontroller = TextEditingController();
final _dataCirurgiaController = TextEditingController();
final _horaCirurgiaController = TextEditingController();
final _procedimentosController = TextEditingController();
final _cidController = TextEditingController();
final _cidadeController = TextEditingController();
String _sexoPaciente;
var sexos = ["Feminino", "Masculino"];
Medico _medicoSelecionado;
String _sexoSelecionado;
List<Medico> _medicos = new List();

class _AgendamentoEdicaoState extends State<AgendamentoEdicaoPage> {
  @override
  void initState() {
    super.initState();
    var medicoId;
    get(url + '/api/cirurgia/' + idCirurgiaEdicao.toString(), headers: {
      "Content-Type": "application/json",
      "Authorization": token
    }).then((Response response) {
      if (response.statusCode == 200) {
        var obj = json.decode(response.body)["data"];
        caixasEdicao = new List<Caixa>();
        var item = obj;
        medicoId = item["medico"]["id"];
        var nomePaciente = item["paciente"]["nome"];
        var numeroPaciente = item["paciente"]["numero"];
        var nascPaciente = item["paciente"]["nascimento"];
        var cidade = item["paciente"]["cidade_residencia"];
        var sexo = item["paciente"]["sexo"];
        var procedimento = item["procedimento"];
        var data = item["data"];
        var pagamento = item['pagamento_realizado'];
        var horaCirurgia = data.substring(11);
        var dataCirurgia = data.substring(0, 10);
        var cid = item["cid"];
        List<Map<String, dynamic>> cxs =
            item["caixas"] != null ? List.from(item["caixas"]) : null;
        for (int i = 0; i < cxs.length; i++) {
          Caixa cx = new Caixa(
              cxs[i]["id"], cxs[i]["nome"], cxs[i]["proprietario_id"]);
          caixasEdicao.add(cx);
        }
        cirurgiaEdicao = new CirurgiaEdicao.empty();
        Tipo t = new Tipo(2, "eletiva");
        cirurgiaEdicao.tipo = t;
        print(pagamento);
        cirurgiaEdicao.pagamentoRealizado = pagamento;
        this.setState(() {
          _cidadeController.text = cidade;
          if (sexo == "M") {
            _sexoSelecionado = "Masculino";
          } else {
            _sexoSelecionado = "Feminino";
          }
          _nomePacienteController.text = nomePaciente;
          _numeroPacientecontroller.text = numeroPaciente.toString();
          _dataNascimentoController.text = nascPaciente.toString();
          _dataCirurgiaController.text = dataCirurgia;
          _horaCirurgiaController.text = horaCirurgia;
          _procedimentosController.text = procedimento;
          _cidadeController.text = cidade;
          _cidController.text = cid.toString();
        });
      }
    });
    _medicos.clear();
    _medicoSelecionado = new Medico(0, "Selecione um médico");
    _medicos.add(_medicoSelecionado);
    loadMedicos(medicoId);
  }

  @override
  Widget build(BuildContext context) {
    var bottom = MediaQuery.of(context).viewInsets.bottom;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Container(
            child: Stack(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: new DecoratedBox(
                  decoration: new BoxDecoration(
                      image: new DecorationImage(
                image: new AssetImage("img/BG1.png"),
                fit: BoxFit.fill,
              )))),
          Container(
              padding: EdgeInsets.all(15),
              child: Column(children: <Widget>[
                Flexible(
                    flex: 2,
                    fit: FlexFit.loose,
                    child: Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Stack(children: <Widget>[
                              Container(
                                  margin: EdgeInsets.only(top: 20),
                                  width: 30,
                                  height: 30,
                                  decoration: new BoxDecoration(
                                    color: Colors.lightBlue[50],
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(18.0),
                                        topRight: const Radius.circular(18.0),
                                        bottomLeft: const Radius.circular(18.0),
                                        bottomRight:
                                            const Radius.circular(18.0)),
                                  ),
                                  child: GestureDetector(
                                      onTap: () {
                                        navigateToListaSolicitacoes(context);
                                      },
                                      child: Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.black,
                                        size: 18.0,
                                        semanticLabel: 'Clear',
                                      ))),
                              Center(
                                child: Container(
                                    width:
                                        MediaQuery.of(context).size.width - 30,
                                    alignment: Alignment.center,
                                    child: Stack(
                                      children: [
                                        new Positioned(
                                          top: 2.0,
                                          left: 2.0,
                                          child: new Text(
                                            'Agendamento de Cirurgia',
                                            style: TextStyle(
                                                color: Colors.black
                                                    .withOpacity(0.5),
                                                fontFamily: 'Montserrat',
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        new Text('Agendamento de Cirurgia',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontFamily: 'Montserrat',
                                                color: Colors.white,
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    )),
                              )
                            ])
                          ],
                        ))),
                Flexible(
                    flex: 10,
                    fit: FlexFit.tight,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[700],
                              blurRadius: 2.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                0.0, // Move to right 10  horizontally
                                1.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                          reverse: true,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Column(children: <Widget>[
                                    new NomePaciente(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    new DataNascimento(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    new NumeroPaciente(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    criaDropDownButtonSexo(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    new CidadePaciente(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    new DataCirurgia(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    new HoraCirurgia(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    new Procedimentos(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    criaDropDownButton(),
                                    new Padding(
                                      padding: EdgeInsets.only(bottom: 10),
                                    ),
                                    new CID(),
                                    Padding(
                                        padding:
                                            EdgeInsets.only(bottom: bottom)),
                                  ]),
                                )
                              ]),
                        ))),
                Flexible(
                    flex: 2,
                    fit: FlexFit.tight,
                    child: Center(
                        child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      child: GestureDetector(
                          onTap: () {
                            performSchedule();
                          },
                          child: Container(
                              padding: EdgeInsets.only(right: 20),
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    Color.fromRGBO(203, 232, 228, 1),
                                    Colors.white
                                  ],
                                ),
                              ),
                              child: Stack(
                                children: <Widget>[
                                  Center(
                                    child: Text("Ver Materiais",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            fontFamily: 'Montserrat',
                                            fontSize: 24)),
                                  ),
                                  Align(
                                      alignment: Alignment.centerRight,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.black,
                                        size: 24.0,
                                        semanticLabel: 'Advance',
                                      ))
                                ],
                              ))),
                    ))),
              ]))
        ])));
  }

  criaDropDownButtonSexo() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Sexo Biológico do Paciente",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          DropdownButton<String>(
            iconSize: 40,
            isExpanded: true,
            items: sexos.map((String sex) {
              return DropdownMenuItem<String>(
                value: sex,
                child: Text(
                  sex,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontFamily: 'Montserrat', fontSize: 18),
                ),
              );
            }).toList(),
            onChanged: (String newValue) {
              setState(() {
                _sexoSelecionado = newValue;
                print(_sexoSelecionado);
              });
            },
            value: _sexoSelecionado,
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
        ],
      ),
    );
  }

  criaDropDownButton() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Nome do Médico",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
          DropdownButton<Medico>(
            iconSize: 40,
            isExpanded: true,
            items: _medicos.map((Medico medItem) {
              return DropdownMenuItem<Medico>(
                value: medItem,
                child: Text(
                  medItem.nome,
                  textAlign: TextAlign.left,
                  style: TextStyle(fontFamily: 'Montserrat', fontSize: 18),
                ),
              );
            }).toList(),
            onChanged: (Medico newValue) {
              setState(() {
                _medicoSelecionado = newValue;
              });
            },
            value: _medicoSelecionado,
          ),
          Padding(padding: EdgeInsets.only(bottom: 10)),
        ],
      ),
    );
  }

  Future loadMedicos(var medId) async {
    List<Medico> sublist = new List();
    sublist.clear();
    var index = 0;
    get(url + '/api/hospital/medicos', headers: {
      "Content-Type": "application/json",
      "Authorization": token
    }).then((Response response) {
      if (response.statusCode == 200) {
        var obj = json.decode(response.body)["data"];
        List<Map<String, dynamic>> tags = obj != null ? List.from(obj) : null;
        if (tags != null) {
          for (int i = 0; i < tags.length; i++) {
            var item = tags[i];
            var nomeMedico = item["nome"];
            var idMed = item["user_id"];
            var idFinal = item["medico_id"];
            int idMedico = num.parse(idMed.toString());
            Medico m = new Medico(idMedico, nomeMedico);
            if (idFinal == medId) {
              index = i;
            }
            sublist.add(m);
          }
        }
      }
      this.setState(() {
        _medicoSelecionado = sublist[index];
        _medicos += sublist;
      });
    });
  }

  void performSchedule() {
    if (_nomePacienteController.text == "") {
      _onBlankNome(context);
    } else if (_dataNascimentoController.text == "") {
      _onBlankNasc(context);
    } else if (_numeroPacientecontroller.text == "") {
      _onBlankNumero(context);
    } else if (_cidadeController.text == "") {
      _onBlankCidade(context);
    } else if (_dataCirurgiaController.text == "") {
      _onBlankDataCirurgia(context);
    } else if (_horaCirurgiaController.text == "") {
      _onBlankHoraCirurgia(context);
    } else if (_procedimentosController.text == "") {
      _onBlankProcedimento(context);
    } else if (_cidController.text == "") {
      _onBlankCID(context);
    } else {
      var nascValida = RegExp(
              r"^(?:(?:31(/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(/)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(/)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$")
          .hasMatch(_dataNascimentoController.text);
      var dataValida = RegExp(
              r"^(?:(?:31(/)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(/)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(/)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(/)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$")
          .hasMatch(_dataCirurgiaController.text);
      var horaValida = RegExp(r"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")
          .hasMatch(_horaCirurgiaController.text);

      if (!nascValida) {
        _onWrongNasc(context);
      } else {
        if (!dataValida) {
          _onWrongDataCirurgia(context);
        } else {
          if (!horaValida) {
            _onWrongHoraCirurgia(context);
          }
          if (_medicoSelecionado.id == 0) {
            _onBlankMedic(context);
          } else {
            if (_sexoSelecionado == "Feminino") {
              _sexoPaciente = "F";
            }
            if (_sexoSelecionado == "Masculino") {
              _sexoPaciente = "M";
            }
            var nome = _nomePacienteController.text;
            var dataNascimento = _dataNascimentoController.text;
            var numeroPaciente = int.parse(_numeroPacientecontroller.text);
            var dataCirurgia = _dataCirurgiaController.text;
            var procedimentos = _procedimentosController.text;
            var cid = _cidController.text;
            var cidade = _cidadeController.text;

            dataCirurgia += " " + _horaCirurgiaController.text;
            cirurgiaEdicao.data = dataCirurgia;
            cirurgiaEdicao.procedimento = procedimentos;
            cirurgiaEdicao.cid = cid;
            Paciente p = new Paciente(
                nome, dataNascimento, numeroPaciente, _sexoPaciente, cidade);
            cirurgiaEdicao.paciente = p;
            cirurgiaEdicao.idMedico = _medicoSelecionado.id;
            limpar = false;
            cirurgiaEdicao.materiais = new List<MaterialCaixaEdicao>();
            navigateToVisualizacaoFinalEdicao(context);
          }
        }
      }
    }
  }
}

_onBlankMedic(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Nome do médico não selecionado!",
    desc: "O nome do médico precisa ser informado.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onBlankCidade(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Nenhuma cidade preenchida!",
    desc: "A cidade do paciente precisa ser informada.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onBlankNome(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Nome do paciente não preenchido!",
    desc: "O nome do paciente precisa ser informado.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onBlankNasc(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Data de nascimento do paciente não preenchido!",
    desc: "A data de nascimento precisa ser informada.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onWrongNasc(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Data de nascimento do paciente inválida!",
    desc:
        "A data de nascimento possui caracteres inválidos. Digite somente números.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onBlankNumero(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Número do paciente não preenchido!",
    desc: "O número do paciente precisa ser informado.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onWrongDataCirurgia(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Data da cirurgia inválida!",
    desc:
        "A data de cirurgia possui caracteres inválidos. Digite somente números.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onBlankDataCirurgia(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Data da cirurgia não preenchida!",
    desc: "A data da cirurgia precisa ser informada.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onWrongHoraCirurgia(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Hora da cirurgia inválida!",
    desc:
        "A hora da cirurgia possui caracteres inválidos. Digite somente números.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onBlankHoraCirurgia(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Hora da cirurgia não preenchida!",
    desc: "A hora da cirurgia precisa ser informada.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onBlankProcedimento(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Nenhum procedimento preenchido!",
    desc: "Ao menos um procedimento precisa ser informado.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onBlankCID(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "CID não preenchido!",
    desc: "O CID precisa ser informado.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

class CidadePaciente extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Cidade do Paciente",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Stack(children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
                child: TextFormField(
                  controller: _cidadeController,
                  decoration: new InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
                      filled: true,
                      fillColor: hexToColor('#e9e9e9'),
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(19.0),
                        ),
                      ),
                      hintText: "Insira a cidade aqui"),
                  style: TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
                  validator: (value) {
                    return null;
                  },
                )),
            Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    onTap: () {
                      _cidadeController.text = "";
                    },
                    child: Container(
                        margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
                        child: Icon(
                          Icons.close,
                          size: 24.0,
                          semanticLabel: 'Clear',
                        )))),
          ])
        ],
      ),
    );
  }
}

class NomePaciente extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Nome do Paciente",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Stack(children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
                child: TextFormField(
                  controller: _nomePacienteController,
                  decoration: new InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
                      filled: true,
                      fillColor: Colors.grey[300],
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(19.0),
                        ),
                      ),
                      hintText: "Insira o nome do paciente aqui"),
                  style: TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
                  validator: (value) {
                    return null;
                  },
                )),
            Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    onTap: () {
                      _nomePacienteController.text = "";
                    },
                    child: Container(
                        margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
                        child: Icon(
                          Icons.close,
                          size: 24.0,
                          semanticLabel: 'Clear',
                        )))),
          ])
        ],
      ),
    );
  }
}

class DataNascimento extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Data de Nascimento do Paciente",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Stack(children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                child: MaskedTextField(
                  mask: "xx/xx/xxxx",
                  maxLength: 10,
                  keyboardType: TextInputType.numberWithOptions(
                      decimal: false, signed: false),
                  maskedTextFieldController: _dataNascimentoController,
                  inputDecoration: new InputDecoration(
                      labelStyle:
                          TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
                      contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
                      filled: true,
                      fillColor: Colors.grey[300],
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(19.0),
                        ),
                      ),
                      hintText: "DD/MM/AAAA"),
                )),
            Align(
                alignment: Alignment.centerRight,
                child: Container(
                    margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
                    child: Icon(
                      Icons.calendar_today,
                      size: 24.0,
                      semanticLabel: 'Clear',
                    ))),
          ])
        ],
      ),
    );
  }
}

class NumeroPaciente extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Número Paciente",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Stack(children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  controller: _numeroPacientecontroller,
                  decoration: new InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
                      filled: true,
                      fillColor: Colors.grey[300],
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(19.0),
                        ),
                      ),
                      hintText: "Insira o número do paciente aqui"),
                  style: TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
                  validator: (value) {
                    return null;
                  },
                )),
            Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    onTap: () {
                      _numeroPacientecontroller.text = "";
                    },
                    child: Container(
                        margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
                        child: Icon(
                          Icons.close,
                          size: 24.0,
                          semanticLabel: 'Clear',
                        )))),
          ])
        ],
      ),
    );
  }
}

class DataCirurgia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Data da Cirurgia",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Stack(children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                child: MaskedTextField(
                  mask: "xx/xx/xxxx",
                  maxLength: 10,
                  keyboardType: TextInputType.number,
                  maskedTextFieldController: _dataCirurgiaController,
                  inputDecoration: new InputDecoration(
                      labelStyle:
                          TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
                      contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
                      filled: true,
                      fillColor: Colors.grey[300],
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(19.0),
                        ),
                      ),
                      hintText: "DD/MM/AAAA"),
                )),
            Align(
                alignment: Alignment.centerRight,
                child: Container(
                    margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
                    child: Icon(
                      Icons.calendar_today,
                      size: 24.0,
                      semanticLabel: 'Clear',
                    ))),
          ])
        ],
      ),
    );
  }
}

class HoraCirurgia extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Hora da Cirurgia",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Stack(children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                child: MaskedTextField(
                  mask: "xx:xx",
                  maxLength: 5,
                  keyboardType: TextInputType.number,
                  maskedTextFieldController: _horaCirurgiaController,
                  inputDecoration: new InputDecoration(
                      labelStyle:
                          TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
                      contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
                      filled: true,
                      fillColor: Colors.grey[300],
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(19.0),
                        ),
                      ),
                      hintText: "HH:MM"),
                )),
            Align(
                alignment: Alignment.centerRight,
                child: Container(
                    margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
                    child: Icon(
                      Icons.access_time,
                      size: 24.0,
                      semanticLabel: 'Clear',
                    ))),
          ])
        ],
      ),
    );
  }
}

class Procedimentos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Procedimentos",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Stack(children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
                child: TextFormField(
                  controller: _procedimentosController,
                  decoration: new InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
                      filled: true,
                      fillColor: Colors.grey[300],
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(19.0),
                        ),
                      ),
                      hintText: "Insira o procedimento aqui"),
                  style: TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
                  validator: (value) {
                    return null;
                  },
                )),
            Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    onTap: () {
                      _procedimentosController.text = "";
                    },
                    child: Container(
                        margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
                        child: Icon(
                          Icons.close,
                          size: 24.0,
                          semanticLabel: 'Clear',
                        )))),
          ])
        ],
      ),
    );
  }
}

// class CRMMedico extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           Text(
//             "CRM do Médico Solicitante",
//             style: TextStyle(
//                 fontFamily: 'Montserrat',
//                 fontSize: 18,
//                 fontWeight: FontWeight.bold,
//                 fontStyle: FontStyle.italic),
//           ),
//           Padding(padding: EdgeInsets.only(bottom: 5)),
//           Stack(children: <Widget>[
//             Container(
//                 padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
//                 child: TextFormField(
//                   controller: _crmMedicoController,
//                   decoration: new InputDecoration(
//                       contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
//                       filled: true,
//                       fillColor: Colors.grey[300],
//                       border: new OutlineInputBorder(
//                         borderRadius: const BorderRadius.all(
//                           const Radius.circular(19.0),
//                         ),
//                       ),
//                       hintText: "00000000-0"),
//                   style: TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
//                   validator: (value) {
//                     return null;
//                   },
//                 )),
//             Align(
//                 alignment: Alignment.centerRight,
//                 child: Container(
//                     margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
//                     child: Icon(
//                       Icons.close,
//                       size: 24.0,
//                       semanticLabel: 'Clear',
//                     ))),
//           ])
//         ],
//       ),
//     );
//   }
// }

class CID extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "CID",
            style: TextStyle(
                fontFamily: 'Montserrat',
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontStyle: FontStyle.italic),
          ),
          Padding(padding: EdgeInsets.only(bottom: 5)),
          Stack(children: <Widget>[
            Container(
                padding: EdgeInsets.fromLTRB(0, 5, 0, 15),
                child: TextFormField(
                  controller: _cidController,
                  decoration: new InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(15, 20, 50, 20),
                      filled: true,
                      fillColor: Colors.grey[300],
                      border: new OutlineInputBorder(
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(19.0),
                        ),
                      ),
                      hintText: "Insira o CID aqui"),
                  style: TextStyle(fontFamily: 'Montserrat', fontSize: 18.0),
                  validator: (value) {
                    return null;
                  },
                )),
            Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    onTap: () {
                      _cidController.text = "";
                    },
                    child: Container(
                        margin: EdgeInsets.fromLTRB(0, 25, 20, 0),
                        child: Icon(
                          Icons.close,
                          size: 24.0,
                          semanticLabel: 'Clear',
                        )))),
          ])
        ],
      ),
    );
  }
}
