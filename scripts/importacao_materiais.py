from openpyxl import Workbook
from openpyxl import load_workbook
import pymysql

CAMINHO_PLANILHA = "planilha_modelo_materiais.xlsx"

COLUNAS = {
	'TIPO_CIRURGIA':'A',
	'NOME_CAIXA': 'B',
	'NOME_PRODUTO': 'C',
	'VALOR':'D',
	'CODIGO':'E',
	'PROPRIETARIO': 'F',
	'QUANTIDADE': 'G'
}

def connect():
	conn = pymysql.connect(
        host='localhost',
        db='gcmedical',
        user='gctest',
        password='gTHNjAziR',
        cursorclass=pymysql.cursors.DictCursor
    )
	conn.autocommit(False)
	cursor = conn.cursor()
	return (conn, cursor)

def get_caixa_id(nome, cursor):
	cursor.execute("select * from caixa where nome = %s", [nome])
	caixa = cursor.fetchone()
	if caixa is None:
		return None
	else:
		return caixa['id']

def cadastrar_caixa(nome, proprietario, cursor):
	cursor.execute("insert into caixa (nome, proprietario_id) values (%s, %s)", [nome, proprietario])
	return cursor.lastrowid

def cadastrar_material(nome, grupo, valor, codigo, cursor):
	cursor.execute("insert into material (nome, grupo_cirurgia, valor, codigo) values (%s, %s, %s, %s)", [nome, grupo, valor, codigo])
	return cursor.lastrowid

def cadastrar_conteudo(caixa_id, material_id, qtd, cursor):
	cursor.execute("insert into conteudo_caixa (caixa_id, material_id, quantidade) values (%s, %s, %s)", [caixa_id, material_id, qtd])
	return cursor.lastrowid

if __name__ == '__main__':
	wb = load_workbook(filename = CAMINHO_PLANILHA)
	ws = wb.active
	qtd_linhas = len(ws['A'])
	conn, cursor = connect()
	try:
		for i in range(2,len(ws['A'])+1):

			nome_caixa = ws['%s%s'% (COLUNAS['NOME_CAIXA'], i)].value
			proprietario_id = ws['%s%s'% (COLUNAS['PROPRIETARIO'], i)].value
			nome_material = ws['%s%s'% (COLUNAS['NOME_PRODUTO'], i)].value
			grupo = ws['%s%s'% (COLUNAS['TIPO_CIRURGIA'], i)].value
			valor = ws['%s%s'% (COLUNAS['VALOR'], i)].value
			codigo = ws['%s%s'% (COLUNAS['CODIGO'], i)].value
			qtd = ws['%s%s'% (COLUNAS['QUANTIDADE'], i)].value

			id_caixa = get_caixa_id(nome_caixa, cursor)
			if id_caixa is None:
				id_caixa = cadastrar_caixa(nome_caixa, proprietario_id, cursor)
			id_material = None
			try:
				id_material = cadastrar_material(nome_material, grupo, valor, codigo, cursor)
				print("Material cadastrado: %s" % codigo)
			except pymysql.err.IntegrityError:
				print("Material com o codigo duplicado: %s" % codigo)
				continue

			cadastrar_conteudo(id_caixa, id_material, qtd, cursor)

		conn.commit()
	finally:
		cursor.close()
		conn.close()
