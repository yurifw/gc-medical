cd ~/gc-medical
git reset --hard HEAD
git pull
sed -i 's-export const SERVER = "https://yuriwaki\.com/api";-export const SERVER = "https://app.gcmedical.com.br/api";-g' ~/gc-medical/webapp/src/app/commons.ts
cd webapp
ng build
rm -rf ../api/public/*[!*.png]
cp -r dist/webapp/* ../api/public
rm -rf dist/webapp/
