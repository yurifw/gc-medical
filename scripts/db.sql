drop database gcmedical;
create database if not exists gcmedical;
use gcmedical;

create table if not exists setor(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table if not exists usuario(
	id int auto_increment primary key,
	nome varchar(100) not null,
	email varchar(100) not null unique,
	senha varchar(128) not null,
	foto longblob,
	foto_mime varchar(20),
	setor_id int not null,
	codigo_reset varchar(20),
	foreign key (setor_id) references setor(id)
);

create table if not exists perfil(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table if not exists acao(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table if not exists acoes_perfil(
	id int auto_increment primary key,
	acao_id int not null,
	perfil_id int not null,
	foreign key (acao_id) references acao(id),
	foreign key (perfil_id) references perfil(id)
);

create table if not exists perfis_usuario (
	id int auto_increment primary key,
	usuario_id int not null,
	perfil_id int not null,
	foreign key (usuario_id) references usuario(id),
	foreign key (perfil_id) references perfil(id)
);

create table if not exists hospital(
	id int auto_increment primary key,
	id_auvo int not null,
	nome varchar(100) not null
);

create table if not exists medico(
	id int auto_increment primary key,
	crm varchar(9) not null,
	hospital_id int not null,
	usuario_id int not null,
	foreign key (hospital_id) references hospital(id),
	foreign key (usuario_id) references usuario(id)
);

create table if not exists gestor_hospitalar(
	id int auto_increment primary key,
	hospital_id int not null,
	usuario_id int not null,
	foreign key (hospital_id) references hospital(id),
	foreign key (usuario_id) references usuario(id)
);

create table if not exists paciente(
	id int auto_increment primary key,
	nome varchar(100) not null,
	nascimento date,
	numero int not null,
	cidade_residencia varchar(100),
	sexo varchar(1)
);

create table if not exists tipo_cirurgia(
	id int auto_increment primary key,
	nome varchar(50) not null
);

create table if not exists cirurgia(
	id int auto_increment primary key,
	paciente_id int not null,
	data datetime not null,
	tipo_cirurgia_id int not null, -- urgencia ou eletiva
	procedimento varchar(200),
	medico int not null,
	cid varchar(100),
	pagamento_realizado bool default 0;
	foreign key (medico) references usuario(id),
	foreign key (paciente_id) references paciente(id),
	foreign key (tipo_cirurgia_id) references tipo_cirurgia(id)
);

create table if not exists material(
	id int auto_increment primary key,
	nome varchar(100) not null,
	grupo_cirurgia varchar(50),
	valor decimal (10, 2),
	codigo varchar(50) not null unique
);

create table if not exists proprietario_caixa(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table if not exists caixa(
	id int auto_increment primary key,
	nome varchar(100) not null,
	proprietario_id int not null,
	hospital_id int,
	foreign key (proprietario_id) references proprietario_caixa(id),
	foreign key (hospital_id) references hospital(id)
);

create table if not exists conteudo_caixa(
	id int auto_increment primary key,
	caixa_id int not null,
	material_id int not null,
	quantidade int not null,
	foreign key (caixa_id) references caixa(id),
	foreign key (material_id) references material(id)
);

create table if not exists solicitacao(
	id int auto_increment primary key,
	cirurgia_id int not null,
	conteudo_caixa_id int not null,
	solicitado int not null,
	consumido int default 0, -- 0= não consumido, 1=consumido, 2=extraviado
	usuario_solicitante_id int not null,
	foreign key (usuario_solicitante_id) references usuario(id),
	foreign key (conteudo_caixa_id) references conteudo_caixa(id),
	foreign key (cirurgia_id) references cirurgia(id)
);

create table if not exists status_encaminhamento(
	id int auto_increment primary key,
	nome varchar(100) not null
);

create table if not exists encaminhamento(
	id int auto_increment primary key,
	data_encaminhamento datetime not null,
	cirurgia_id int not null,
	status_encaminhamento_id int not null,
	observacao varchar(1000),
	usuario_id int not null,  -- usuario que fez o encaminhamento
	foreign key (status_encaminhamento_id) references status_encaminhamento(id),
	foreign key (usuario_id) references usuario(id),
	foreign key (cirurgia_id) references cirurgia(id)
);

create table documentos(
	id int auto_increment primary key,
	documento longblob not null,
	descricao varchar(1000),
	nome_arquivo varchar(100) not null,
	nome_exibicao varchar(100),
	cirurgia_id int not null,
	data_upload datetime not null,
	uploader_id int not null,
	foreign key (uploader_id) references usuario(id),
	foreign key (cirurgia_id) references cirurgia(id)
);


drop FUNCTION if exists ultimo_status_cirurgia;
DELIMITER $$
CREATE FUNCTION ultimo_status_cirurgia (cirurgia_requisitada INT) RETURNS INT
BEGIN
	DECLARE status int DEFAULT null;

	select
		status_encaminhamento_id
	into
		status
	from
		encaminhamento
	where
		cirurgia_id = cirurgia_requisitada and
		status_encaminhamento_id <> 5 and  -- ignora os status 'comercial encaminha para instrumentacao' e 'comercial encaminha para logistica'
		status_encaminhamento_id <> 6
	order by
		data_encaminhamento desc
	limit 1;

	RETURN status;
END $$
DELIMITER ;

insert into perfil(nome) values ('Administrador');
insert into perfil(nome) values ('Médico');
insert into perfil(nome) values ('Gestor Hospitalar');
insert into acao(nome) values ('Aprovar Materiais');					-- 1
insert into acao(nome) values ('Solicitar Materiais');					-- 2
insert into acao(nome) values ('Listar Cirurgias Próprias');			-- 3
insert into acao(nome) values ('Atualizar Cirurgia');					-- 4
insert into acao(nome) values ('Cadastrar Usuário');					-- 5
insert into acao(nome) values ('Listar Pacientes');						-- 6
insert into acao(nome) values ('Atualizar Paciente');					-- 7
insert into acao(nome) values ('Deletar Paciente');						-- 8
insert into acao(nome) values ('Cadastrar Paciente');					-- 9
insert into acao(nome) values ('Listar Médicos');						-- 10
insert into acao(nome) values ('Atualizar Médico');						-- 11
insert into acao(nome) values ('Deletar Médico');						-- 12
insert into acao(nome) values ('Cadastrar Médico');						-- 13
insert into acao(nome) values ('Listar Hospitais');						-- 14
insert into acao(nome) values ('Atualizar Hospital');					-- 15
insert into acao(nome) values ('Deletar Hospital');						-- 16
insert into acao(nome) values ('Cadastrar Hospital');					-- 17
insert into acao(nome) values ('Listar Gestores');						-- 18
insert into acao(nome) values ('Atualizar Gestor');						-- 19
insert into acao(nome) values ('Deletar Gestor');						-- 20
insert into acao(nome) values ('Cadastrar Gestor');						-- 21
insert into acao(nome) values ('Cadastrar Documento');					-- 22
insert into acao(nome) values ('Deletar Documento');					-- 23
insert into acao(nome) values ('Visualizar Documentos');				-- 24
insert into acao(nome) values ('Cadastrar Permissões');					-- 25
insert into acao(nome) values ('Atualizar Permissões');					-- 26
insert into acao(nome) values ('Deletar Permissões');					-- 27
insert into acao(nome) values ('Listar Permissões');					-- 28
insert into acao(nome) values ('Listar Encaminhamentos');				-- 29
insert into acao(nome) values ('Encaminhar Cirurgia');					-- 30
insert into acao(nome) values ('Listar Cirurgias');						-- 31
insert into acao(nome) values ('Visualizar Histórico');					-- 32
insert into acao(nome) values ('Listar Usuários');						-- 33
insert into acao(nome) values ('Atualizar Usuários');					-- 34
insert into acao(nome) values ('Deletar Usuários');						-- 35
insert into acoes_perfil (perfil_id, acao_id) values (1,1);
insert into acoes_perfil (perfil_id, acao_id) values (1,2);
insert into acoes_perfil (perfil_id, acao_id) values (1,3);
insert into acoes_perfil (perfil_id, acao_id) values (1,4);
insert into acoes_perfil (perfil_id, acao_id) values (1,5);
insert into acoes_perfil (perfil_id, acao_id) values (1,6);
insert into acoes_perfil (perfil_id, acao_id) values (1,7);
insert into acoes_perfil (perfil_id, acao_id) values (1,8);
insert into acoes_perfil (perfil_id, acao_id) values (1,9);
insert into acoes_perfil (perfil_id, acao_id) values (1,10);
insert into acoes_perfil (perfil_id, acao_id) values (1,11);
insert into acoes_perfil (perfil_id, acao_id) values (1,12);
insert into acoes_perfil (perfil_id, acao_id) values (1,13);
insert into acoes_perfil (perfil_id, acao_id) values (1,14);
insert into acoes_perfil (perfil_id, acao_id) values (1,15);
insert into acoes_perfil (perfil_id, acao_id) values (1,16);
insert into acoes_perfil (perfil_id, acao_id) values (1,17);
insert into acoes_perfil (perfil_id, acao_id) values (1,18);
insert into acoes_perfil (perfil_id, acao_id) values (1,19);
insert into acoes_perfil (perfil_id, acao_id) values (1,20);
insert into acoes_perfil (perfil_id, acao_id) values (1,21);
insert into acoes_perfil (perfil_id, acao_id) values (1,22);
insert into acoes_perfil (perfil_id, acao_id) values (1,23);
insert into acoes_perfil (perfil_id, acao_id) values (1,24);
insert into acoes_perfil (perfil_id, acao_id) values (1,25);
insert into acoes_perfil (perfil_id, acao_id) values (1,26);
insert into acoes_perfil (perfil_id, acao_id) values (1,27);
insert into acoes_perfil (perfil_id, acao_id) values (1,28);
insert into acoes_perfil (perfil_id, acao_id) values (1,29);
insert into acoes_perfil (perfil_id, acao_id) values (1,30);
insert into acoes_perfil (perfil_id, acao_id) values (1,31);
insert into acoes_perfil (perfil_id, acao_id) values (1,32);
insert into acoes_perfil (perfil_id, acao_id) values (1,33);
insert into acoes_perfil (perfil_id, acao_id) values (1,34);
insert into acoes_perfil (perfil_id, acao_id) values (1,35);
insert into acoes_perfil (perfil_id, acao_id) values (2,2);
insert into acoes_perfil (perfil_id, acao_id) values (2,3);
insert into acoes_perfil (perfil_id, acao_id) values (2,4);
insert into acoes_perfil (perfil_id, acao_id) values (2,6);
insert into acoes_perfil (perfil_id, acao_id) values (2,7);
insert into acoes_perfil (perfil_id, acao_id) values (2,9);
insert into acoes_perfil (perfil_id, acao_id) values (3,18);
insert into setor(nome) values ("Gestor Hospitalar");
insert into setor(nome) values ("Comercial");
insert into setor(nome) values ("Logística");
insert into setor(nome) values ("Instrumentação");
insert into setor(nome) values ("Médicos");

insert into status_encaminhamento(nome) values ("Pendente Pagamento");
insert into status_encaminhamento(nome) values ("Encaminhado para Gestor Hospitalar");
insert into status_encaminhamento(nome) values ("Aprovado pelo Gestor Hospitalar");
insert into status_encaminhamento(nome) values ("Reprovado pelo Gestor Hospitalar");
insert into status_encaminhamento(nome) values ("Comercial encaminhou para Logística");
insert into status_encaminhamento(nome) values ("Comercial encaminhou para Instrumentação");
insert into status_encaminhamento(nome) values ("Reprovado pelo Comercial");
insert into status_encaminhamento(nome) values ("Faturamento Solicitado");
insert into status_encaminhamento(nome) values ("Faturamento Aprovado");
insert into status_encaminhamento(nome) values ("Faturamento Reprovado");

insert into proprietario_caixa (nome) values ('GC Medical');

-- teste:
insert into paciente (nome, numero, nascimento) values ('Bia', 123,  STR_TO_DATE("31/05/1992", "%d/%m/%Y"));
insert into paciente (nome, numero, nascimento) values ('Diego', 863,  STR_TO_DATE("06/09/1986", "%d/%m/%Y"));
insert into paciente (nome, numero, nascimento) values ('Lucas', 726,  STR_TO_DATE("15/11/1976", "%d/%m/%Y"));
insert into paciente (nome, numero, nascimento) values ('Ana', 726,  STR_TO_DATE("11/11/1999", "%d/%m/%Y"));
insert into perfil(nome) values ('GC Medical');
insert into perfil(nome) values ('Hospital');
insert into acoes_perfil (perfil_id, acao_id) values (2,1);
insert into acoes_perfil (perfil_id, acao_id) values (3,2);
insert into acoes_perfil (perfil_id, acao_id) values (3,3);
insert into acoes_perfil (perfil_id, acao_id) values (3,4);
insert into usuario (nome, email, setor_id, senha) values ('Teste Admin', 'teste@teste.com', 1, '$2b$12$G/7WvMFWVeP/vHjdOtHa..wn81jeoxRkIIJQA8UspOijeBDUyJrle');  -- senha 123456
insert into perfis_usuario (perfil_id, usuario_id) values (1,1);
insert into usuario (nome, email, setor_id, senha) values ('usuario chm', 'chm@teste.com', 1, '$2b$12$G/7WvMFWVeP/vHjdOtHa..wn81jeoxRkIIJQA8UspOijeBDUyJrle');  -- senha 123456
insert into perfis_usuario (perfil_id, usuario_id) values (1,2);
insert into usuario (nome, email, setor_id, senha) values ('usuario comercial', 'comercial@teste.com', 2, '$2b$12$G/7WvMFWVeP/vHjdOtHa..wn81jeoxRkIIJQA8UspOijeBDUyJrle');  -- senha 123456
insert into perfis_usuario (perfil_id, usuario_id) values (1,3);
insert into hospital (nome, id_auvo) values ("SOCIEDADE ASSISTENCIAL BANDEIRANTES", 253945);
insert into hospital (nome, id_auvo) values ("BOSQUE DA SAÚDE", 253946);
insert into medico (crm, hospital_id, usuario_id) values ("123456789", 1,1);
insert into tipo_cirurgia (nome) values('urgência');
insert into tipo_cirurgia (nome) values('eletiva');


SET old_passwords=0;
create user IF NOT EXISTS 'gctest'@'localhost' identified by 'gTHNjAziR';
GRANT ALL PRIVILEGES ON gcmedical . * TO 'gctest'@'localhost';
FLUSH PRIVILEGES;
