export const SERVER = "https://yuriwaki.com/api";

export interface Resposta {
	msg: string;
	success: boolean;
	data: any;
}

export const DATE_OPTIONS = {
	dateRange: false,
	dateFormat: 'dd/mm/yyyy',
	dayLabels: {su: 'D', mo: 'S', tu: 'T', we: 'Q', th: 'Q', fr: 'S', sa: 'S'},
	monthLabels: { 1: 'Jan', 2: 'Fev', 3: 'Mar', 4: 'Abr', 5: 'Mai', 6: 'Jun', 7: 'Jul', 8: 'Ago', 9: 'Set', 10: 'Out', 11: 'Nov', 12: 'Dez' },
	inline: false,
	showDecreaseDateBtn: true,
	editableDateField: true,
	openSelectorOnInputClick: true
}
