import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabelaCirurgiasComponent } from './components/tabela-cirurgias/tabela-cirurgias.component'
import { TelaLoginComponent } from './components/tela-login/tela-login.component'
import { TelaEsqueciSenhaComponent } from './components/tela-esqueci-senha/tela-esqueci-senha.component'
import { TelaResetarSenhaComponent } from './components/tela-resetar-senha/tela-resetar-senha.component'
import { HomeComponent } from './components/home/home.component'
import { TelaCadastroUsuarioComponent } from './components/tela-cadastro-usuario/tela-cadastro-usuario.component'
import { TelaUsuariosComponent } from './components/tela-usuarios/tela-usuarios.component'
import { TelaPerfilComponent } from './components/tela-perfil/tela-perfil.component'
import { TelaPacientesComponent } from './components/tela-pacientes/tela-pacientes.component'
import { TelaFormPacienteComponent } from './components/tela-form-paciente/tela-form-paciente.component'
import { TelaMedicosComponent } from './components/tela-medicos/tela-medicos.component'
import { TelaFormMedicoComponent } from './components/tela-form-medico/tela-form-medico.component'
import { TelaHospitaisComponent } from './components/tela-hospitais/tela-hospitais.component'
import { TelaFormHospitalComponent } from './components/tela-form-hospital/tela-form-hospital.component'
import { TelaGestorComponent } from './components/tela-gestor/tela-gestor.component'
import { TelaFormGestorComponent } from './components/tela-form-gestor/tela-form-gestor.component'
import { TelaPermissoesComponent } from './components/tela-permissoes/tela-permissoes.component'
import { TelaFormPermissoesComponent } from './components/tela-form-permissoes/tela-form-permissoes.component'
import { TelaDetalhesCirurgiaComponent } from './components/tela-detalhes-cirurgia/tela-detalhes-cirurgia.component'
import { TelaHistoricoComponent } from './components/tela-historico/tela-historico.component'
import { TelaMateriaisComponent } from './components/tela-materiais/tela-materiais.component'

const routes: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: 'teste-tabela', component: TabelaCirurgiasComponent },
	{ path: '', component: TelaLoginComponent },
	{ path: 'login', component: TelaLoginComponent },
	{ path: 'recuperar-senha', component: TelaEsqueciSenhaComponent },
	{ path: 'resetar-senha/:codigo', component: TelaResetarSenhaComponent },
	{ path: 'perfil', component: TelaPerfilComponent },
	{ path: 'usuario', component: TelaUsuariosComponent },
	{ path: 'usuario/:acao', component: TelaCadastroUsuarioComponent },
	{ path: 'usuario/editar/:id', component: TelaCadastroUsuarioComponent },
	{ path: 'paciente', component: TelaPacientesComponent },
	{ path: 'paciente/:acao', component: TelaFormPacienteComponent },
	{ path: 'paciente/editar/:id', component: TelaFormPacienteComponent },
	{ path: 'medico', component: TelaMedicosComponent },
	{ path: 'medico/:acao', component: TelaFormMedicoComponent },
	{ path: 'medico/editar/:id', component: TelaFormMedicoComponent },
	{ path: 'hospital', component: TelaHospitaisComponent },
	{ path: 'hospital/:acao', component: TelaFormHospitalComponent },
	{ path: 'hospital/editar/:id', component: TelaFormHospitalComponent },
	{ path: 'gestor-hospitalar', component: TelaGestorComponent },
	{ path: 'gestor-hospitalar/:acao', component: TelaFormGestorComponent },
	{ path: 'gestor-hospitalar/editar/:id', component: TelaFormGestorComponent },
	{ path: 'permissoes', component: TelaPermissoesComponent },
	{ path: 'permissoes/:acao', component: TelaFormPermissoesComponent },
	{ path: 'permissoes/editar/:id', component: TelaFormPermissoesComponent },
	{ path: 'cirurgia/:id', component: TelaDetalhesCirurgiaComponent },
	{ path: 'cirurgia/:encaminhamento_id/materiais', component: TelaMateriaisComponent },
	{ path: 'cirurgia/cadastrar/materiais', component: TelaMateriaisComponent },	
	{ path: 'cirurgia/:cirurgia_id/historico', component: TelaHistoricoComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
