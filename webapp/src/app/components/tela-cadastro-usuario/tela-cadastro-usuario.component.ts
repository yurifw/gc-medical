import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';
import { SetorService } from '../../services/setor.service'
import { UsuarioService } from '../../services/usuario.service'
import { PerfilService } from '../../services/perfil.service'

@Component({
	selector: 'app-tela-cadastro-usuario',
	templateUrl: './tela-cadastro-usuario.component.html',
	styleUrls: ['./tela-cadastro-usuario.component.css']
})
export class TelaCadastroUsuarioComponent implements OnInit {

	usuario = {
		id: 0,
		nome:"",
		email:"",
		senha:"",
		setor: {id:0, nome:null},
		perfis: null,
		setor_id:null
	}
	foto:null
	header = ""
	usuarioId = null
	cadastrando = false
	atualizando = false
	setores = []
	perfis = []
	perfisSelecionado = []

	@ViewChild('selectSetor', {static: false}) selectSetor;
	@ViewChild('selectPerfis', {static: false}) selectPerfis;
	@ViewChild('thumbnail', {static: false}) thumbnail;

	constructor(
		private route:ActivatedRoute,
		private setorService: SetorService,
		private perfilService: PerfilService,
		private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		let acao = this.route.snapshot.params['acao'];
		let id = this.route.snapshot.params['id'];

		if (acao){
			this.header = "Cadastrar Usuário"
			this.cadastrando = true
		}

		if (id){
			this.header = "Alterar Usuário"
			this.usuarioId = id
			this.atualizando = true
			this.usuarioService.buscarPorId(this.usuarioId).subscribe(res => {
				if (res.success){
					this.usuario = res.data
					console.log("this.usuario",this.usuario)
					this.usuario.perfis.forEach(p => {
						this.perfisSelecionado.push(p.id)
					})
				}
			})
		}


		this.setorService.getAll().subscribe( (res) =>{
			for(let i = 0; i<res.data.length; i++){
				this.setores.push(res.data[i])
			}
		})

		this.perfilService.getAll().subscribe( (res) =>{
			for(let i = 0; i<res.data.length; i++){
				if(res.data[i].id == 2 || res.data[i].id == 3){
					continue
				}
				this.perfis.push(res.data[i])
			}
			console.log("this.perfis",this.perfis)
		})

	}

	toggle(perfil){
		if (this.perfisSelecionado.includes(perfil.id)){
			let index = this.perfisSelecionado.indexOf(perfil.id);
			this.perfisSelecionado.splice(index, 1);
		} else {
			this.perfisSelecionado.push(perfil.id)
		}
	}
	cadastrar(){
		let perfis = []
		for( let i=0; i<this.perfisSelecionado.length; i++){
			perfis.push({"id":this.perfisSelecionado[i]})
		}
		this.usuario.setor_id = this.usuario.setor.id
		this.usuario.perfis = JSON.stringify(perfis)
		let formData = new FormData();
		for ( var key in this.usuario) {
			formData.append(key, this.usuario[key]);
		}
		formData.append("foto", this.foto)

		this.usuarioService.cadastrar(formData).subscribe( resp =>{
			if (resp.success){
				this.perfisSelecionado = []
				this.usuario = {
					id:0,
					nome:"",
					email:"",
					senha:"",
					setor: {id:0, nome:null},
					perfis: null,
					setor_id:null
				}
			}
		})

	}

	alterar(){
		let perfis = []
		for( let i=0; i<this.perfisSelecionado.length; i++){
			perfis.push({"id":this.perfisSelecionado[i]})
		}
		this.usuario.setor_id = this.usuario.setor.id
		this.usuario.perfis = JSON.stringify(perfis)
		let formData = new FormData();
		for ( var key in this.usuario) {
			formData.append(key, this.usuario[key]);
		}
		formData.append("foto", this.foto)

		this.usuarioService.update(this.usuario.id, formData).subscribe()
	}

	fotoSelecionada(event){
		this.foto = event.target.files[0]

		let reader = new FileReader();
		reader.onload = function (e) {
			(<HTMLImageElement>document.getElementById("thumbnail")).src = ""+e.target.result;
			// document.getElementById("thumbnail").src = e.target.result;
		};
		reader.readAsDataURL(event.target.files[0])
	}
}
