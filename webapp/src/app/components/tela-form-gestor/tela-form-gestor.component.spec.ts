import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaFormGestorComponent } from './tela-form-gestor.component';

describe('TelaFormGestorComponent', () => {
  let component: TelaFormGestorComponent;
  let fixture: ComponentFixture<TelaFormGestorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaFormGestorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaFormGestorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
