import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';
import { SetorService } from '../../services/setor.service'
import { HospitalService } from '../../services/hospital.service'
import { GestorService } from '../../services/gestor.service'
import { ToastrService } from 'ngx-toastr';


@Component({
	selector: 'app-tela-form-gestor',
	templateUrl: './tela-form-gestor.component.html',
	styleUrls: ['./tela-form-gestor.component.css']
})
export class TelaFormGestorComponent implements OnInit {

	header:string
	cadastrando = false
	atualizando = false
	foto:null
	gestorId: null
	gestor = {
		nome:"",
		email:"",
		senha:"",
		senhaConfirmada:"",
		setor_id: null,
		hospital_id: null
	}
	@ViewChild('selectHospital', {static: false}) selectHospital;
	@ViewChild('selectSetor', {static: false}) selectSetor;
	@ViewChild('thumbnail', {static: false}) thumbnail;

	constructor(
		private route: ActivatedRoute,
		private hospitalService: HospitalService,
		private setorService: SetorService,
		private gestorService: GestorService,
		private toastr: ToastrService
	) { }

	ngOnInit(): void {
		let acao = this.route.snapshot.params['acao'];
		let id = this.route.snapshot.params['id'];
		this.setorService.getAll().subscribe(res => {
			if (res.success){
				this.gestor.setor_id = res.data[0].id
				res.data.forEach(setor => {
					let html = "<option value = "+setor.id+">"+setor.nome+"</option>"
					this.selectSetor.nativeElement.insertAdjacentHTML('beforeend', html);
				});
			}
		})

		this.hospitalService.getAll().subscribe(res => {
			if (res.success){
				this.gestor.hospital_id = res.data[0].id
				res.data.forEach(hospital => {
					let html = "<option value = "+hospital.id+">"+hospital.nome+"</option>"
					this.selectHospital.nativeElement.insertAdjacentHTML('beforeend', html);
				});
			}
		})

		if (acao){
			this.header = "Cadastrar Gestor Hospitalar"
			this.cadastrando = true
		}


		if (id){
			this.header = "Atualizar Gestor Hospitalar"
			this.gestorId = id
			this.atualizando = true
			this.gestorService.buscarPorId(id).subscribe(res =>{
				this.gestor.hospital_id = res.data.hospital.id
				this.gestor.nome = res.data.nome
				this.gestor.email = res.data.email
				this.gestor.setor_id = res.data.setor_id
				console.log(res.data)
			})
		}
	}

	cadastrar(){
		if(this.gestor.senha != this.gestor.senhaConfirmada){
			this.toastr.warning("As senhas não conferem")
			return
		}

		let formData = new FormData();
		for ( var key in this.gestor) {
			formData.append(key, this.gestor[key]);
		}
		formData.append("foto", this.foto)
		this.gestorService.cadastrar(formData).subscribe()
	}

	atualizar(){
		this.gestorService.atualizar(this.gestorId, this.gestor).subscribe()
	}

	fotoSelecionada(event){
		this.foto = event.target.files[0]

		let reader = new FileReader();
		reader.onload = function (e) {
			(<HTMLImageElement>document.getElementById("thumbnail")).src = ""+e.target.result;
			// document.getElementById("thumbnail").src = e.target.result;
		};
		reader.readAsDataURL(event.target.files[0])
	}

}
