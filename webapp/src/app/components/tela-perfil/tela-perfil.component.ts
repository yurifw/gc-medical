import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from '../../services/usuario.service'
import * as $ from 'jquery';

@Component({
	selector: 'app-tela-perfil',
	templateUrl: './tela-perfil.component.html',
	styleUrls: ['./tela-perfil.component.css']
})
export class TelaPerfilComponent implements OnInit {
	foto = null
	image64 = null
	usuario = {
		nome:"",
		senha:"",
		novaSenha:"",
		confirmaSenha:""
	}
	constructor(private toastr: ToastrService, private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(res => {
			this.usuario.nome = res.data.nome
			this.image64 = 'data:'+res.data.foto_mime+';base64,' + res.data.foto
		})
	}

	fotoSelecionada(event){
		this.foto = event.target.files[0]

		let reader = new FileReader();
		reader.onload = function (e) {
			(<HTMLImageElement>document.getElementById("thumbnail")).src = ""+e.target.result;
		};
		reader.readAsDataURL(event.target.files[0])

	}
	atualizar(){
		if (this.usuario.novaSenha != this.usuario.confirmaSenha){
			this.toastr.warning("Senhas não conferem")
			return
		}
		let formData = new FormData();
		for ( var key in this.usuario) {
			formData.append(key, this.usuario[key]);
		}
		formData.append("foto", this.foto);
		this.usuarioService.atualizar(formData).subscribe()
		let $: any;
		$('#modal-confirm').modal('toggle');
		// ($('#modal-confirm') as any).modal('toggle');
	}

}
