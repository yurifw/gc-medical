import { Component, OnInit, Input } from '@angular/core';
import { EncaminhamentoService } from '../../services/encaminhamento.service'
import { AuvoService } from '../../services/auvo.service'
import {IAngularMyDpOptions, IMyDateModel} from 'angular-mydatepicker';
import { ToastrService } from 'ngx-toastr';
import {DATE_OPTIONS} from "../../commons"
declare var $: any;

@Component({
	selector: 'app-btn-acao-encaminhamento',
	templateUrl: './btn-acao-encaminhamento.component.html',
	styleUrls: ['./btn-acao-encaminhamento.component.css']
})
export class BtnAcaoEncaminhamentoComponent implements OnInit {
	@Input() encaminhamento: any;
	textoConfirma = ""
	observacao =  ""
	confirma:Function

	// dataEntregaAuvo = ""
	dataEntregaLogisitca = ""
	horaLogistica = 9
	minutoLogistica = 0
	numeroAgendamentoAuvo = ""
	tipoLogistica = 0
	horaEntrega = ""
	minutoEntrega = ""
	auvoUsers = []
	auvoUsuarioResponsavel: any

	nomesSetor = {
		"1": "Gestor Hospitalar",
		"2": "Comercial",
		"3": "Logística",
		"4": "Instrumentação",
		"5": "Hospital"
	}
	myDpOptions: IAngularMyDpOptions = DATE_OPTIONS;
	dateModel: IMyDateModel = null;
	dateRetiradaModel: IMyDateModel = null;
	horaRetirada = 9
	minutoRetirada = 0

	agendarRetirada = false

	constructor(
		private encaminhamentoService: EncaminhamentoService,
		private toastr: ToastrService,
		private auvoService: AuvoService
	) { }

	ngOnInit(): void {
		$('#modal-confirm-acao').on('hidden.bs.modal', function (e) {
			$(".select-acoes").val(0)
		})

		$('#modal-logistica').on('hidden.bs.modal', function (e) {
			$(".select-acoes").val(0)
		})

		$('#modal-instrumentacao').on('hidden.bs.modal', function (e) {
			$(".select-acoes").val(0)
		})
		this.updateAuvoUsers()
	}

	updateAuvoUsers(){
		if(sessionStorage.getItem("auvo_users") == null){
			this.auvoService.getUsers().subscribe((resp)=>{
				if(resp.success){
					this.auvoUsers = resp.data
					sessionStorage.setItem("auvo_users",JSON.stringify(resp.data))
				}
			})
		} else {
			this.auvoUsers = JSON.parse(sessionStorage.getItem("auvo_users"))
		}
	}

	modalAprovaSolicitacao(aprovado){
		this.textoConfirma = aprovado? "Tem certeza que deseja aprovar os materiais solicitados?" : "Tem certeza que deseja retornar a solicitação de materiais?"
		this.confirma = () => {
			let avaliacao = {
				"cirurgia_id":this.encaminhamento.cirurgia.id,
				"status_id": aprovado? 3 : 4,
				"observacao": this.observacao
			}
			this.toastr.info("Aguarde enquanto encaminhamos a solicitação")
			this.encaminhamentoService.moverEncaminhamento(avaliacao).subscribe( res => {
				if (res.success){
					window.location.reload()
				}
			})
		}
	}

	modalAprovaFaturamento(aprovado){
		this.textoConfirma = aprovado? "Tem certeza que deseja aprovar o faturamento?" : "Tem certeza que deseja reprovar o faturamento?"
		this.confirma = () => {
			let avaliacao = {
				"cirurgia_id":this.encaminhamento.cirurgia.id,
				"status_id": aprovado? 9 : 10,
				"observacao": this.observacao
			}
			this.toastr.info("Aguarde enquanto encaminhamos a solicitação")
			this.encaminhamentoService.moverEncaminhamento(avaliacao).subscribe( res => {
				if (res.success){
					window.location.reload()
				}
			})
		}
	}

	updateDataLogistica(){

		let dataCirurgia = this.stringToJsDate(this.encaminhamento.cirurgia.data)

		let dataCorrigida = null
		let horario = this.encaminhamento.cirurgia.data.split(" ")[1]

		console.log("dataCirurgia", dataCirurgia)
		let dataRetirada = this.stringToJsDate(this.encaminhamento.cirurgia.data)
		dataRetirada.setDate(dataRetirada.getDate()+1)
		console.log("dataRetirada", dataRetirada)
		this.dateRetiradaModel = {isRange: false, singleDate: {jsDate: dataRetirada}};


		if(this.tipoLogistica == 9224){
			dataCorrigida = dataCirurgia.getDate() -1
		} else {
			dataCorrigida = dataCirurgia.getDate()+1
		}
		console.log("dataCorrigida", dataCorrigida)
		dataCirurgia.setDate(dataCorrigida)
		this.dateModel = {isRange: false, singleDate: {jsDate: dataCirurgia}};

	}

	comercialEncaminha(status){
		if(status == 0){	return }

		if (status != 0){
			if (status==5){
				this.textoConfirma = "Tem certeza que deseja encaminhar a demanda para Logística ?"
			}
			if (status==6){
				this.textoConfirma = "Tem certeza que deseja encaminhar a demanda para Instrumentação ?"
			}
			if (status==7){
				this.textoConfirma = "Tem certeza que deseja retornar a demanda para o Gestor Hospitalar ?"
			}
			if (status==8 || status==10){
				this.textoConfirma = "Tem certeza que deseja solicitar o faturamento ?"
			}

			if (status==1){
				this.textoConfirma = "Tem certeza que deseja encerrar a solicitação ?"
			}
			this.confirma = () =>{


				let taskType = 0
				if (status==5){
					taskType = this.tipoLogistica
				}
				if (status==6){
					if(this.encaminhamento.cirurgia.tipo.id == 1){
						taskType = 9218
					} else {
						taskType = 9217
					}
				}
				if ( (status==6 || status==5) &&  taskType == 0){
					this.toastr.warning("Selecione o tipo da tarefa")
					return
				}

				let data_auvo = null

				if (status==6){
					data_auvo = this.encaminhamento.cirurgia.data
				} else {
					if(this.dateModel){
						if(this.dateModel.singleDate.formatted){
							data_auvo = this.dateModel.singleDate.formatted
						}else {
							data_auvo = this.JsDateToString(this.dateModel.singleDate.jsDate)
						}
						let horaRetirada = ("0"+this.horaRetirada).slice(-2)
						let minutoRetirada = ("0"+this.minutoRetirada).slice(-2)
						data_auvo = data_auvo + " "+horaRetirada+":"+minutoRetirada
					}
				}

				let data = {
					"cirurgia_id": this.encaminhamento.cirurgia.id,
					"status_id": status,
					"observacao": this.observacao,
					"data_auvo": data_auvo,
					"agendamento_auvo": this.numeroAgendamentoAuvo,
					"task_type": parseInt(""+taskType),
					"id_to_auvo": this.auvoUsuarioResponsavel || ''
				}
				this.toastr.info("Aguarde enquanto encaminhamos a solicitação")
				this.encaminhamentoService.moverEncaminhamento(data).subscribe( res => {
					console.log("entrega", data)
					if (res.success){
						if(this.tipoLogistica == 9224 && this.agendarRetirada){
							let dataHoraRetirada = ""
							if(this.dateRetiradaModel.singleDate.formatted){
								dataHoraRetirada = this.dateRetiradaModel.singleDate.formatted
							} else {
								dataHoraRetirada = this.JsDateToString(this.dateRetiradaModel.singleDate.jsDate)
							}
							let hora = ("0"+this.horaRetirada).slice(-2)
							let minuto = ("0"+this.minutoRetirada).slice(-2)
							dataHoraRetirada = dataHoraRetirada + " "+hora+":"+minuto
							data.data_auvo = dataHoraRetirada
							data.task_type = 9225
							this.encaminhamentoService.moverEncaminhamento(data).subscribe(res =>{
								if(res.success){
									setTimeout(() => {window.location.reload()}, 10);
								}
							})
						} else {
							setTimeout(() => {window.location.reload()}, 10);
						}
					}
				})
			}

		}

		if (status == 5){
			$('#modal-logistica').modal('show');
		} else if(status == 6){
			$('#modal-instrumentacao').modal('show');
		} else {
			$('#modal-confirm-acao').modal('show');
		}


	}

	stringToJsDate(str){
		let date = null
		let dia = str.split("/")[0]
		let mes = str.split("/")[1] -1
		let ano = str.split("/")[2].split(" ")[0]

		if(str.split("/")[2].split(" ").length > 1){
			let hora = str.split("/")[2].split(" ")[1].split(":")[0]
			let min = str.split("/")[2].split(" ")[1].split(":")[1]
			date = new Date(ano, mes, dia, hora, min)
		} else {
			date = new Date(ano, mes, dia)
		}
		return date
	}

	JsDateToString(date){
		let dia = "0"+date.getDate()
		dia = dia.slice(-2)
		let mes = "0"+(date.getMonth()+1)+""
		mes = mes.slice(-2)
		let ano = date.getFullYear()
		return  ""+dia+"/"+mes+"/"+ano
	}
	// onDataEntregaChanged(event){
	// 	this.dataEntregaAuvo = event.singleDate.formatted
	// }
}
