import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BtnAcaoEncaminhamentoComponent } from './btn-acao-encaminhamento.component';

describe('BtnAcaoEncaminhamentoComponent', () => {
  let component: BtnAcaoEncaminhamentoComponent;
  let fixture: ComponentFixture<BtnAcaoEncaminhamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BtnAcaoEncaminhamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BtnAcaoEncaminhamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
