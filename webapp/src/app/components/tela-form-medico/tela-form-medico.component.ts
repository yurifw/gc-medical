import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';
import { SetorService } from '../../services/setor.service'
import { HospitalService } from '../../services/hospital.service'
import { MedicoService } from '../../services/medico.service'
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-tela-form-medico',
	templateUrl: './tela-form-medico.component.html',
	styleUrls: ['./tela-form-medico.component.css']
})
export class TelaFormMedicoComponent implements OnInit {
	header:string
	cadastrando = false
	atualizando = false
	foto:null
	medicoId: null
	medico = {
		nome:"",
		crm:"",
		email:"",
		senha:"",
		senhaConfirmada:"",
		setor_id: null,
		hospital_id: null
	}
	@ViewChild('selectHospital', {static: false}) selectHospital;
	@ViewChild('selectSetor', {static: false}) selectSetor;
	@ViewChild('thumbnail', {static: false}) thumbnail;

	constructor(
		private route: ActivatedRoute,
		private hospitalService: HospitalService,
		private setorService: SetorService,
		private medicoService: MedicoService,
		private toastr: ToastrService
	) { }

	ngOnInit(): void {
		let acao = this.route.snapshot.params['acao'];
		let id = this.route.snapshot.params['id'];
		this.setorService.getAll().subscribe(res => {
			if (res.success){
				this.medico.setor_id = res.data[0].id
				res.data.forEach(setor => {
					let html = "<option value = "+setor.id+">"+setor.nome+"</option>"
					this.selectSetor.nativeElement.insertAdjacentHTML('beforeend', html);
				});
			}
		})
		if (acao){
			this.header = "Cadastrar Médico"
			this.cadastrando = true

		}



		this.hospitalService.getAll().subscribe(res => {
			if (res.success){
				this.medico.hospital_id = res.data[0].id
				res.data.forEach(hospital => {
					let html = "<option value = "+hospital.id+">"+hospital.nome+"</option>"
					this.selectHospital.nativeElement.insertAdjacentHTML('beforeend', html);
				});
			}
		})

		if (id){
			this.header = "Atualizar Médico"
			this.medicoId = id
			this.atualizando = true
			this.medicoService.buscarPorId(id).subscribe(res =>{
				this.medico.nome = res.data.nome
				this.medico.crm = res.data.crm
				this.medico.hospital_id = res.data.hospital.id
				this.medico.email = res.data.email
				this.medico.setor_id = res.data.setor_id
			})
		}
	}

	cadastrar(){
		if(this.medico.senha != this.medico.senhaConfirmada){
			this.toastr.warning("As senhas não conferem")
			return
		}

		let formData = new FormData();
		for ( var key in this.medico) {
			formData.append(key, this.medico[key]);
		}
		formData.append("foto", this.foto)
		this.medicoService.cadastrar(formData).subscribe()
	}

	atualizar(){
		this.medicoService.atualizar(this.medicoId, this.medico).subscribe()
	}

	fotoSelecionada(event){
		this.foto = event.target.files[0]

		let reader = new FileReader();
		reader.onload = function (e) {
			(<HTMLImageElement>document.getElementById("thumbnail")).src = ""+e.target.result;
			// document.getElementById("thumbnail").src = e.target.result;
		};
		reader.readAsDataURL(event.target.files[0])
	}

}
