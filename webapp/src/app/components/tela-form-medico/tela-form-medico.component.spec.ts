import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaFormMedicoComponent } from './tela-form-medico.component';

describe('TelaFormMedicoComponent', () => {
  let component: TelaFormMedicoComponent;
  let fixture: ComponentFixture<TelaFormMedicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaFormMedicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaFormMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
