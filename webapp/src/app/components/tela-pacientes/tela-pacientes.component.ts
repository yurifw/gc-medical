import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../../services/paciente.service'
import { UsuarioService } from '../../services/usuario.service'
import { Router } from "@angular/router"
import * as $ from 'jquery';

@Component({
	selector: 'app-tela-pacientes',
	templateUrl: './tela-pacientes.component.html',
	styleUrls: ['./tela-pacientes.component.css']
})
export class TelaPacientesComponent implements OnInit {
	acoesPermitidas = []

	filter: string
	p: number = 1;
	pacientes = []
	pacienteDeletado: any = {}

	constructor(private pacienteService: PacienteService, private router: Router, private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(res => {
			this.acoesPermitidas = res.data.permissoes.map( p =>{return p.acao_id})
		})

		this.atualizar()
	}

	atualizar(){
		this.usuarioService.getInfo().subscribe(res => {
			this.acoesPermitidas = res.data.permissoes.map( p =>{return p.acao_id})
		})
		this.pacienteService.getAll().subscribe( res =>{
			this.pacientes = res.data
		})
	}

	cadastrar(){
		this.router.navigate(['/paciente/cadastrar'])
	}

	editar(id){
		this.router.navigate(['/paciente/editar/'+id])
	}

	abrirModal(paciente){
		this.pacienteDeletado = paciente
	}

	deletar(id){
		this.pacienteService.delete(id).subscribe(res => {
			if (res.success){
				this.atualizar()
			}
		})
	}

}
