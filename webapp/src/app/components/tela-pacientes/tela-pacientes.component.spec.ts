import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaPacientesComponent } from './tela-pacientes.component';

describe('TelaPacientesComponent', () => {
  let component: TelaPacientesComponent;
  let fixture: ComponentFixture<TelaPacientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaPacientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaPacientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
