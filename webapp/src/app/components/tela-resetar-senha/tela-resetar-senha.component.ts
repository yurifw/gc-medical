import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import {Router} from "@angular/router"

@Component({
	selector: 'app-tela-resetar-senha',
	templateUrl: './tela-resetar-senha.component.html',
	styleUrls: ['./tela-resetar-senha.component.css']
})
export class TelaResetarSenhaComponent implements OnInit {
	senha: string;
	cSenha: string;
	constructor(
		private route:ActivatedRoute,
		private toastr: ToastrService,
		private auth: AuthService,
		private router: Router
	) { }

	ngOnInit(): void {


	}

	resetar(){
		if (!this.senha){
			this.toastr.warning("Digite a senha")
			return
		}
		if (this.senha != this.cSenha){
			this.toastr.warning("As senhas não conferem")
			return
		}
		let codigo = this.route.snapshot.params['codigo'];
		
		this.auth.resetarSenha(codigo, this.senha).subscribe((res) =>{
			if (res.success){
				localStorage.setItem('jwt',res.data)
				this.router.navigate(['/home'])
			}
		})

	}

}
