import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaResetarSenhaComponent } from './tela-resetar-senha.component';

describe('TelaResetarSenhaComponent', () => {
  let component: TelaResetarSenhaComponent;
  let fixture: ComponentFixture<TelaResetarSenhaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaResetarSenhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaResetarSenhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
