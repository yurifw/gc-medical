import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaFormPacienteComponent } from './tela-form-paciente.component';

describe('TelaFormPacienteComponent', () => {
  let component: TelaFormPacienteComponent;
  let fixture: ComponentFixture<TelaFormPacienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaFormPacienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaFormPacienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
