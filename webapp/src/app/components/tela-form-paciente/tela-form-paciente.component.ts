import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PacienteService } from '../../services/paciente.service'
import {IAngularMyDpOptions, IMyDateModel} from 'angular-mydatepicker';
import {DATE_OPTIONS} from "../../commons"


@Component({
	selector: 'app-tela-form-paciente',
	templateUrl: './tela-form-paciente.component.html',
	styleUrls: ['./tela-form-paciente.component.css']
})
export class TelaFormPacienteComponent implements OnInit {
	header:string
	paciente = {
		nome:"",
		numero: null,
		nascimento: null,
		cidade_residencia: null,
		sexo: null
	}
	cadastrando = false
	atualizando = false
	pacienteId = null
	myDpOptions: IAngularMyDpOptions = DATE_OPTIONS;
	dateModel: IMyDateModel = null;

	constructor(private route:ActivatedRoute, private pacienteService: PacienteService) { }

	ngOnInit(): void {
		let acao = this.route.snapshot.params['acao'];
		let id = this.route.snapshot.params['id'];
		if (acao){
			this.header = "Cadastrar Paciente"
			this.cadastrando = true
		}
		if (id){
			this.header = "Alterar Paciente"
			this.pacienteId = id
			this.atualizando = true
			this.pacienteService.buscarPorId(this.pacienteId).subscribe(res => {
				if (res.success){
					this.paciente = res.data
					let dia = this.paciente.nascimento.split("/")[0]
					let mes = this.paciente.nascimento.split("/")[1] -1
					let ano = this.paciente.nascimento.split("/")[2]
					let date = new Date(ano, mes, dia)
					this.dateModel =  {isRange: false, singleDate: {jsDate: date}};
				}
			})
		}
	}

	limpar(){
		this.paciente = {
			nome:"",
			numero: null,
			nascimento: null,
			cidade_residencia: null,
			sexo: null
		}
	}
	cadastrar(){
		this.pacienteService.cadastrar(this.paciente).subscribe(res =>{
			if(res.success){
				this.limpar()
			}
		})
	}

	atualizar(){
		this.pacienteService.atualizar(this.pacienteId, this.paciente).subscribe()
	}

	onDateChanged(event){
		this.paciente.nascimento = event.singleDate.formatted
	}

}
