import { Component, OnInit, Input  } from '@angular/core';
import { DocumentosService } from '../../services/documentos.service'
// import * as $ from 'jquery';
declare var $: any;

@Component({
	selector: 'app-documentos',
	templateUrl: './documentos.component.html',
	styleUrls: ['./documentos.component.css']
})
export class DocumentosComponent implements OnInit {
	@Input() cirurgiaId: number;
	documentos = []
	docToDelete:any = {}
	novoDoc:any = {}
	novoDocBlob:any
	constructor(private documentosService: DocumentosService) { }

	ngOnInit(): void {
		this.buscarDocumentos()
	}

	buscarDocumentos(){
		if(this.cirurgiaId){
			this.documentosService.buscarPorCirurgia(this.cirurgiaId).subscribe(res => {
				this.documentos = res.data
			})
		} else {
			setTimeout(()=>{
				this.buscarDocumentos()
			}, 1000);
		}
	}

	cadastrar(){
		console.log("this.cirurgiaId", this.cirurgiaId)
		let formData = new FormData();
		for ( var key in this.novoDoc) {
			formData.append(key, this.novoDoc[key]);
		}
		formData.append("documento", this.novoDocBlob)
		formData.append("cirurgia_id", ""+this.cirurgiaId)
		this.documentosService.cadastrar(formData).subscribe(res => {
			if(res.success){
				this.novoDoc = {}
				this.novoDocBlob = null
				this.buscarDocumentos()
			}
		})
	}

	download(id){
		this.documentosService.download(id)
	}

	arquivoSelecionado(event){
		this.novoDocBlob = event.target.files[0]
		this.novoDoc.nome_arquivo = event.target.files[0].name
	}

	stageForDelete(documento){
		this.docToDelete = documento
	}

	dismiss(){
		$('#modal-confirm-doc').modal('hide');
	}

	delete(id){
		this.documentosService.delete(id).subscribe( res => {
			if (res.success){
				this.dismiss()
				this.buscarDocumentos()
			}
		})
	}

}
