import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EncaminhamentoService } from '../../services/encaminhamento.service'
import { CirurgiaService } from '../../services/cirurgia.service'
import { UsuarioService } from '../../services/usuario.service'
import { MedicoService } from '../../services/medico.service'
import { RouterModule } from '@angular/router';
import { Router } from "@angular/router"
import {IAngularMyDpOptions, IMyDateModel} from 'angular-mydatepicker';
import {DATE_OPTIONS} from "../../commons"

@Component({
	selector: 'app-tela-detalhes-cirurgia',
	templateUrl: './tela-detalhes-cirurgia.component.html',
	styleUrls: ['./tela-detalhes-cirurgia.component.css']
})
export class TelaDetalhesCirurgiaComponent implements OnInit {
	encaminhamento:any = {
		cirurgia: {
			paciente: {},
			medico:{},
			tipo: {}
		}
	}

	medicos = []

	editando = false
	cadastrando = false
	mostrarStatusPagamento = false
	myDpOptions: IAngularMyDpOptions = DATE_OPTIONS;
	nascimentoPaciente: IMyDateModel = null;
	dataCirurgia: IMyDateModel = null;
	horaCirurgia= null
	minutoCirurgia= null

	constructor(
		private route:ActivatedRoute,
		private encaminhamentoService: EncaminhamentoService,
		private cirurgiaService: CirurgiaService,
		private medicoService: MedicoService,
		private usuarioService: UsuarioService,
		private router: Router
	) { }

	ngOnInit(): void {
		let encaminhamentoId = this.route.snapshot.params['id'];

		if(encaminhamentoId && encaminhamentoId != 'cadastrar'){
			this.encaminhamentoService.getById(encaminhamentoId).subscribe(res => {
				if(res.success){
					this.encaminhamento = res.data
					console.log(this.encaminhamento)
					this.editando = true
					this.nascimentoPaciente = {isRange: false, singleDate: {jsDate: this.stringToJsDate(this.encaminhamento.cirurgia.paciente.nascimento)}};
					this.dataCirurgia = {isRange: false, singleDate: {jsDate: this.stringToJsDate(this.encaminhamento.cirurgia.data)}};
					console.log("this.encaminhamento.cirurgia.data", this.encaminhamento.cirurgia.data)
					this.horaCirurgia = parseInt(this.encaminhamento.cirurgia.data.split(" ")[1].split(":")[0])
					this.minutoCirurgia = parseInt(this.encaminhamento.cirurgia.data.split(" ")[1].split(":")[1])
					setTimeout(() => {
						this.editando = false
					}, 0.5);
				}
			})
		}

		if(encaminhamentoId == 'cadastrar'){
			this.cadastrando = true
		}

		if(encaminhamentoId == 'cadastrar' && sessionStorage.getItem("cirurgia_cadastro")){
			this.cadastrando = true
			this.encaminhamento.cirurgia = JSON.parse(sessionStorage.getItem("cirurgia_cadastro"))
		}

		this.medicoService.getAll().subscribe(resp => {
			if(resp.success){
				this.medicos = resp.data
			}
		})

		this.usuarioService.getInfo().subscribe((resp) => {
			if (resp.success && [1,2].includes(resp.data.setor.id) && resp.data.medico_id == null){
				this.mostrarStatusPagamento = true
			}
		})
		console.log(this.encaminhamento.cirurgia)
	}

	toggleEditando(){
		this.editando = !this.editando
	}

	atualizar(){
		// this.encaminhamento.cirurgia.medico = this.encaminhamento.cirurgia.medico.usuario_id
		this.cirurgiaService.atualizar(this.encaminhamento.cirurgia.id, this.encaminhamento.cirurgia).subscribe((resp) =>{
			if(resp.success){
				this.toggleEditando()
			}
		})
	}

	cadastrar(){
		sessionStorage.setItem("cirurgia_cadastro", JSON.stringify(this.encaminhamento.cirurgia))
		this.router.navigate(['/cirurgia/cadastrar/materiais'])
	}

	onNascimentoPacienteChanged(event){
		this.encaminhamento.cirurgia.paciente.nascimento = event.singleDate.formatted
	}

	onDataCirurgiaChanged(event){
		let data = null
		if(event){
			data = event.singleDate.formatted
		} else {
			data = this.dataCirurgia.singleDate.formatted
		}
		if (data == undefined){ //data vem undefined quando o usuario edita a hora e o minuto sem editar o dia
			data = this.encaminhamento.cirurgia.data.split(" ")[0]
		}

		let hora = "0"+this.horaCirurgia
		hora = hora.slice(-2)

		let min = "0"+this.minutoCirurgia
		min = min.slice(-2)

		data = data + " "+hora+":"+min

		this.encaminhamento.cirurgia.data = data
	}

	stringToJsDate(str){
		let dia = str.split("/")[0]
		let mes = str.split("/")[1] -1
		let ano = str.split("/")[2].split(" ")[0]
		let date = new Date(ano, mes, dia)
		return date
	}

}
