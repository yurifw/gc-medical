import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaDetalhesCirurgiaComponent } from './tela-detalhes-cirurgia.component';

describe('TelaDetalhesCirurgiaComponent', () => {
  let component: TelaDetalhesCirurgiaComponent;
  let fixture: ComponentFixture<TelaDetalhesCirurgiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaDetalhesCirurgiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaDetalhesCirurgiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
