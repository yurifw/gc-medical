import { Component, OnInit } from '@angular/core';
import { PacienteService } from '../../services/paciente.service'
import { UsuarioService } from '../../services/usuario.service'
import { Router } from "@angular/router"

@Component({
	selector: 'app-tela-usuarios',
	templateUrl: './tela-usuarios.component.html',
	styleUrls: ['./tela-usuarios.component.css']
})
export class TelaUsuariosComponent implements OnInit {
	acoesPermitidas = []
	filter: string
	p: number = 1;
	usuarios = []
	usuarioDeletado: any = {}

	constructor(private router: Router, private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(res => {
			this.acoesPermitidas = res.data.permissoes.map( p =>{return p.acao_id})
		})

		this.atualizar()
	}

	atualizar(){
		this.usuarioService.selectAll().subscribe(resp =>{
			if(resp.success){
				this.usuarios = resp.data
			}
		})
	}

	cadastrar(){
		this.router.navigate(['/usuario/cadastrar'])
	}

	editar(id){
		this.router.navigate(['/usuario/editar/'+id])
	}

	abrirModal(usuario){
		this.usuarioDeletado = usuario
	}

	deletar(id){
		this.usuarioService.delete(id).subscribe(res => {
			if (res.success){
				this.atualizar()
			}
		})
	}

}
