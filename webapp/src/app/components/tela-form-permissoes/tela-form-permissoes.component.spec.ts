import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaFormPermissoesComponent } from './tela-form-permissoes.component';

describe('TelaFormPermissoesComponent', () => {
  let component: TelaFormPermissoesComponent;
  let fixture: ComponentFixture<TelaFormPermissoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaFormPermissoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaFormPermissoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
