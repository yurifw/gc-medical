import { Component, OnInit } from '@angular/core';
import { PermissoesService } from '../../services/permissoes.service'
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-tela-form-permissoes',
	templateUrl: './tela-form-permissoes.component.html',
	styleUrls: ['./tela-form-permissoes.component.css']
})
export class TelaFormPermissoesComponent implements OnInit {
	header:string
	nomePerfil:string;
	cadastrando = false
	atualizando = false
	perfilId = null
	acoesExistentes = []
	acoesEscolhidas = []
	constructor(private permissoesService: PermissoesService, private route:ActivatedRoute) { }

	ngOnInit(): void {
		let acao = this.route.snapshot.params['acao'];
		let id = this.route.snapshot.params['id'];
		if (acao){
			this.header = "Cadastrar Perfil"
			this.cadastrando = true
		}
		if (id){
			this.header = "Alterar Perfil"
			this.perfilId = id
			this.atualizando = true
			this.permissoesService.getById(id).subscribe(res => {
				this.nomePerfil = res.data.nome
				res.data.permissoes.forEach(permissao => {
					this.acoesEscolhidas.push(permissao.id)
				})
			})
		}
		this.permissoesService.getAcoes().subscribe(res => {
			if (res.success){
				this.acoesExistentes = res.data
			}
		})
	}


	toggle(acao){
		console.log(acao.id)
		if (this.acoesEscolhidas.includes(acao.id)){
			let index = this.acoesEscolhidas.indexOf(acao.id);
			this.acoesEscolhidas.splice(index, 1);
		} else {
			this.acoesEscolhidas.push(acao.id)
		}
	}

	atualizar(){
		let perfil = {
			'nome': this.nomePerfil,
			'permissoes': this.acoesExistentes.filter(a => this.acoesEscolhidas.includes(a.id))
		}
		this.permissoesService.atualizar(this.perfilId, perfil).subscribe()
	}
	cadastrar(){
		let perfil = {
			'nome': this.nomePerfil,
			'permissoes': this.acoesExistentes.filter(a => this.acoesEscolhidas.includes(a.id))
		}
		this.permissoesService.cadastrar(perfil).subscribe()
	}
}
