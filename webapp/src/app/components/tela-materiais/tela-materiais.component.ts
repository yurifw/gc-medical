import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EncaminhamentoService } from '../../services/encaminhamento.service'
import { CirurgiaService } from '../../services/cirurgia.service'
import { UsuarioService } from '../../services/usuario.service'
import { CaixaService } from '../../services/caixa.service'
import { Router } from "@angular/router"
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-tela-materiais',
	templateUrl: './tela-materiais.component.html',
	styleUrls: ['./tela-materiais.component.css']
})
export class TelaMateriaisComponent implements OnInit {
	encaminhamento:any = {
		cirurgia: {
			paciente: {},
			medico:{},
			tipo: {},
			observacao:""
		}

	}
	valor_total:number = 0
	editando = false;
	cadastrando = false;
	termoBuscado = ""
	resultadosBusca = []
	materiaisBuscados:any = {materiais:[]}
	exibicaoCaixas = []
	isMedico = true
	selectAll = false


	constructor(
		private route:ActivatedRoute,
		private encaminhamentoService: EncaminhamentoService,
		private cirurgiaService: CirurgiaService,
		private caixaService: CaixaService,
		private usuarioService: UsuarioService,
		private router: Router,
		private toastr: ToastrService
	) { }

	ngOnInit(): void {
		let encaminhamentoId = this.route.snapshot.params['encaminhamento_id'];
		if(encaminhamentoId == "cadastrar"){
			this.encaminhamento.cirurgia = JSON.parse(sessionStorage.getItem("cirurgia_cadastro"))
			this.cadastrando = true
			this.encaminhamento.cirurgia.solicitacoes = []
		}else {
			this.encaminhamentoService.getById(encaminhamentoId).subscribe(res => {

				if(res.success){
					this.encaminhamento = res.data
					console.log("encaminhamento",this.encaminhamento)

					for(let i =0; i< this.encaminhamento.cirurgia.solicitacoes.length; i++){
						this.valor_total = this.valor_total + this.encaminhamento.cirurgia.solicitacoes[i].valor
					}
					this.valor_total = Math.round(this.valor_total * 100) / 100

					this.encaminhamento.cirurgia.solicitacoes.forEach( s => {
						this.exibeCaixaMaterial(s.caixa_id, s)
					})
				}
			})
		}

		this.usuarioService.getInfo().subscribe(resp => {
			this.isMedico = resp.data.medico_id != null
		})
	}

	exibeCaixaMaterial(caixaId, material){

		this.caixaService.searchById(caixaId).subscribe(resp => {

			let caixaJaSelecionada = false
			this.exibicaoCaixas.forEach( caixa => {
				console.log('caixa')
				if (caixa.id == caixaId){
					caixaJaSelecionada = true
					console.log('push 1')
					caixa.materiais.push(material)
				}
			})

			if (!caixaJaSelecionada){
				this.exibicaoCaixas.push({
					"id": caixaId,
					"nome": resp.data.nome,
					"materiais":[
						material
					]
				})
			}
		})
	}

	toggleEditando(){
		this.editando = !this.editando

	}

	toggleSelectAll(){
		this.selectAll = !this.selectAll

		this.materiaisBuscados.materiais.forEach(material => {
			material.solicitado = this.selectAll? material.quantidade : 0
		})
	}

	buscar(){
		this.caixaService.search(this.termoBuscado).subscribe((resp) => {
			if (resp.success){
				this.resultadosBusca = resp.data
				console.log("resultadosBusca", this.resultadosBusca)
			}
		})
	}

	buscaCaixa(caixa){
		this.caixaService.searchById(caixa.id).subscribe((resp) => {
			if (resp.success){
				this.materiaisBuscados = resp.data
				this.materiaisBuscados.materiais.forEach(m=>{
					m.solicitado = 0
				})
				console.log("this.materiaisBuscados",this.materiaisBuscados)
			}
		})
	}

	adicionarCaixa(){
		console.log("materiaisBuscados",this.materiaisBuscados)
		this.materiaisBuscados.materiais.forEach(material => {
			if(material.solicitado){
				let novoMaterial = {
					"caixa_id": this.materiaisBuscados.id,
					"codigo": material.codigo,
					"consumido": 0,
					"conteudo_caixa_id": material.conteudo_caixa_id,
					"material_id": material.material_id,
					"nome": material.nome,
					"solicitado": material.solicitado,
					"valor":material.valor,
					"proprietario_id": this.materiaisBuscados.proprietario_id
				}

				this.encaminhamento.cirurgia.solicitacoes.push(novoMaterial)
				this.exibeCaixaMaterial(this.materiaisBuscados.id, novoMaterial)

			}
		});
	}

	atualizar(){
		if(this.encaminhamento.cirurgia.solicitacoes.length == 0){
			this.toastr.warning("Adicione pelo menos uma caixa")
			return
		}
		this.encaminhamento.cirurgia.materiais = []
		this.encaminhamento.cirurgia.solicitacoes.forEach(s =>{
			this.encaminhamento.cirurgia.materiais.push({
				"conteudo_caixa_id":s.conteudo_caixa_id,
				"solicitado":s.solicitado,
				"consumido": s.consumido
			})
		})

		this.cirurgiaService.atualizar(this.encaminhamento.cirurgia.id, this.encaminhamento.cirurgia).subscribe((resp) =>{
			if(resp.success){
				this.toggleEditando()

			}
		})
		console.log("encaminhamento.cirurgia", JSON.stringify(this.encaminhamento.cirurgia))
	}

	cadastrar(){
		if(this.encaminhamento.cirurgia.solicitacoes.length == 0){
			this.toastr.warning("Adicione pelo menos uma caixa")
			return
		}
		this.encaminhamento.cirurgia.materiais = []
		this.encaminhamento.cirurgia.solicitacoes.forEach(s =>{
			this.encaminhamento.cirurgia.materiais.push({
				"conteudo_caixa_id":s.conteudo_caixa_id,
				"solicitado":s.solicitado,
				"consumido": s.consumido
			})
		})
		let medAux = this.encaminhamento.cirurgia.medico
		this.encaminhamento.cirurgia.medico = this.encaminhamento.cirurgia.medico.usuario_id
		this.cirurgiaService.cadastrar(this.encaminhamento.cirurgia).subscribe(resp=>{
			if(resp.success){
				sessionStorage.removeItem("cirurgia_cadastro")
				this.router.navigate(['/home'])
			} else {
				this.encaminhamento.cirurgia.medico = medAux
			}
		})
		console.log(this.encaminhamento.cirurgia)
	}

	removeSolicitacao(solicitacao){
		let index = this.encaminhamento.cirurgia.solicitacoes.indexOf(solicitacao);
		if (index !== -1) {
			this.encaminhamento.cirurgia.solicitacoes.splice(index, 1);
		}

		this.exibicaoCaixas.forEach(caixa => {
			index = caixa.materiais.indexOf(solicitacao);
			if (index !== -1) {
				caixa.materiais.splice(index, 1);
				return
			}
		})
	}
}
