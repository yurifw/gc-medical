import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaMateriaisComponent } from './tela-materiais.component';

describe('TelaMateriaisComponent', () => {
  let component: TelaMateriaisComponent;
  let fixture: ComponentFixture<TelaMateriaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaMateriaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaMateriaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
