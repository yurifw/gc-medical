import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaCirurgiasComponent } from './tabela-cirurgias.component';

describe('TabelaCirurgiasComponent', () => {
  let component: TabelaCirurgiasComponent;
  let fixture: ComponentFixture<TabelaCirurgiasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaCirurgiasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaCirurgiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
