import { Component, OnInit } from '@angular/core';
import { EncaminhamentoService } from '../../services/encaminhamento.service'
import { SetorService } from '../../services/setor.service'
import { UsuarioService } from '../../services/usuario.service'
import { Router } from "@angular/router"
import {IAngularMyDpOptions, IMyDateModel} from 'angular-mydatepicker';
import {DATE_OPTIONS} from "../../commons"

@Component({
	selector: 'app-tabela-cirurgias',
	templateUrl: './tabela-cirurgias.component.html',
	styleUrls: ['./tabela-cirurgias.component.css']
})
export class TabelaCirurgiasComponent implements OnInit {
	filter: string
	p: number = 1

	setores = []
	encaminhamentos = []
	encaminhamentosFiltrados = []

	myDpOptions: IAngularMyDpOptions = DATE_OPTIONS
	dateModelInicio: IMyDateModel = null;
	dateModelFim: IMyDateModel = null;

	statusPossiveis = []
	statusSelecionado = 0

	constructor(
		private encaminhamentoService: EncaminhamentoService,
		private usuarioService: UsuarioService,
		private setorService: SetorService,
		private router: Router
	) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(res =>{
			this.listEncaminhamentos(res.data.setor.id)
		})
		this.setorService.getAll().subscribe(res => {
			if(res.success){
				this.setores = res.data
			}
		})
		this.encaminhamentoService.getStatus().subscribe(res => {
			if(res.success){
				this.statusPossiveis = res.data
				console.log(this.statusPossiveis)
			}
		})
		this.filter = sessionStorage.getItem('filtro')
	}

	detalhes(id){
		this.router.navigate(['/cirurgia/'+id])
	}

	salvaFiltro(f){
		console.log('filtro',f)
		sessionStorage.setItem('filtro',f)
	}

	filtrar(dataInicio, dataFim, novoStatus){
		localStorage.setItem('status',novoStatus)
		console.log(dataInicio, dataFim, novoStatus)
		this.encaminhamentosFiltrados = this.encaminhamentos.filter(( e) =>{
			let ano = parseInt(e.cirurgia.data.split("/")[2])
			let mes = parseInt(e.cirurgia.data.split("/")[1]) - 1
			let dia = parseInt(e.cirurgia.data.split("/")[0])
			let jsDate = new Date(ano, mes, dia)

			let inicio = true
			if(dataInicio && dataInicio.singleDate.formatted != ""){
				inicio = jsDate > dataInicio.singleDate.jsDate
			}
			let fim = true
			if(dataFim && dataFim.singleDate.formatted != ""){
				fim = jsDate < dataFim.singleDate.jsDate
			}

			let statusCerto = true
			if(novoStatus){
				statusCerto = e.status.id == novoStatus || novoStatus == 0
			}

			return inicio && fim && statusCerto
		})

	}

	checkStatusSalvos(){
		let status = parseInt(localStorage.getItem('status'))
		if (status){
			this.statusSelecionado = status
			this.filtrar(this.dateModelInicio, this.dateModelFim, this.statusSelecionado)
		}
	}
	onDataInicioChanged(event){
		this.filtrar(event, this.dateModelFim, this.statusSelecionado)
	}

	onDataFimChanged(event){
		this.filtrar(this.dateModelInicio, event, this.statusSelecionado)
	}

	onStatusChanged(){
		console.log(this.statusSelecionado)
		this.filtrar(this.dateModelInicio, this.dateModelFim, this.statusSelecionado)
	}
	listEncaminhamentos(setorId){
		if (setorId == 1){
			this.encaminhamentoService.getCHM().subscribe(res => {
				if (res.success){
					this.encaminhamentos = res.data
					this.encaminhamentosFiltrados = res.data
					this.checkStatusSalvos()
				}
			})
		}

		if (setorId == 2){
			this.encaminhamentoService.getComercial().subscribe(res => {
				if (res.success){
					this.encaminhamentos = res.data
					this.encaminhamentosFiltrados = res.data
					this.checkStatusSalvos()
				}
			})
		}

		if (setorId == 5){
			this.encaminhamentoService.getMedicos().subscribe(res => {
				if (res.success){
					this.encaminhamentos = res.data
					this.encaminhamentosFiltrados = res.data
					this.checkStatusSalvos()
				}
			})
		}

	}

	cadastrar(){
		this.router.navigate(['/cirurgia/cadastrar'])
	}
}
