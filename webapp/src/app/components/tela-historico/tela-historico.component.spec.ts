import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaHistoricoComponent } from './tela-historico.component';

describe('TelaHistoricoComponent', () => {
  let component: TelaHistoricoComponent;
  let fixture: ComponentFixture<TelaHistoricoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaHistoricoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaHistoricoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
