import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EncaminhamentoService } from '../../services/encaminhamento.service'

@Component({
	selector: 'app-tela-historico',
	templateUrl: './tela-historico.component.html',
	styleUrls: ['./tela-historico.component.css']
})
export class TelaHistoricoComponent implements OnInit {
	filter: string

	historico: any = []

	constructor(private route:ActivatedRoute, private encaminhamentoService: EncaminhamentoService) { }

	ngOnInit(): void {
		let cirurgiaId = this.route.snapshot.params['cirurgia_id'];
		this.encaminhamentoService.getHistorico(cirurgiaId).subscribe(res => {
			console.log(res)
			this.historico = res.data
		})
	}



}
