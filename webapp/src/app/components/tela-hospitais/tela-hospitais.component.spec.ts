import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaHospitaisComponent } from './tela-hospitais.component';

describe('TelaHospitaisComponent', () => {
  let component: TelaHospitaisComponent;
  let fixture: ComponentFixture<TelaHospitaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaHospitaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaHospitaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
