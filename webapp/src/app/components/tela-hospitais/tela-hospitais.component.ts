import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { HospitalService } from '../../services/hospital.service'
import { UsuarioService } from '../../services/usuario.service'

@Component({
	selector: 'app-tela-hospitais',
	templateUrl: './tela-hospitais.component.html',
	styleUrls: ['./tela-hospitais.component.css']
})
export class TelaHospitaisComponent implements OnInit {
	acoesPermitidas = []

	filter: string
	hospitalDeletado: any = {}
	hospitais = []
	p: number = 1

	constructor(private router: Router, private hospitalService: HospitalService, private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(res => {
			this.acoesPermitidas = res.data.permissoes.map( p =>{return p.acao_id})
		})
		this.atualizar()
	}

	atualizar(){
		this.hospitalService.getAll().subscribe(res => {
			this.hospitais = res.data
		})
	}

	cadastrar(){
		this.router.navigate(['/hospital/cadastrar'])
	}

	editar(id){
		this.router.navigate(['/hospital/editar/'+id])
	}

	abrirModal(hospital){
		this.hospitalDeletado = hospital
	}

	deletar(id){
		this.hospitalService.delete(id).subscribe(res => {
			if (res.success){
				this.atualizar()
			}
		})
	}

}
