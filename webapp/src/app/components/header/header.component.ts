import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router"
import { UsuarioService } from '../../services/usuario.service'

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
	nome = ""
	setor:any = {}
	image64 = ""
	acoesPermitidas = []
	constructor(
		private router: Router,
		private usuarioService: UsuarioService
	) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(res => {
			this.acoesPermitidas = res.data.permissoes.map( p =>{return p.acao_id})
			this.nome = res.data.nome
			this.setor = res.data.setor
			this.image64 = 'data:'+res.data.foto_mime+';base64,' + res.data.foto
		})
	}

	openPerfil(){
		this.closeMenu()
		document.getElementById("sidemenu-perfil").classList.add("sidemenu-view");
	}

	closePerfil(){
		document.getElementById("sidemenu-perfil").classList.remove("sidemenu-view");
	}

	openMenu(){
		this.closePerfil()
		document.getElementById("sidemenu-menu").classList.add("sidemenu-view");
	}

	closeMenu(){
		document.getElementById("sidemenu-menu").classList.remove("sidemenu-view");
	}

	logout(){
		localStorage.removeItem('jwt')
		this.router.navigate(['/login'])
	}

	navigate(route){
		this.router.navigate([route])
	}


}
