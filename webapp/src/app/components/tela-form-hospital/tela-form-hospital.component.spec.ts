import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaFormHospitalComponent } from './tela-form-hospital.component';

describe('TelaFormHospitalComponent', () => {
  let component: TelaFormHospitalComponent;
  let fixture: ComponentFixture<TelaFormHospitalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaFormHospitalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaFormHospitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
