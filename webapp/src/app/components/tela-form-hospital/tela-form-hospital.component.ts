import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HospitalService } from '../../services/hospital.service'
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-tela-form-hospital',
	templateUrl: './tela-form-hospital.component.html',
	styleUrls: ['./tela-form-hospital.component.css']
})
export class TelaFormHospitalComponent implements OnInit {
	header:string
	hospital = {
		nome:"",
		id_auvo:""
	}
	cadastrando = false
	atualizando = false
	hospitalId = null

	constructor(private route:ActivatedRoute,
		private hospitalService: HospitalService,
		private toastr: ToastrService) { }

	ngOnInit(): void {
		let acao = this.route.snapshot.params['acao'];
		let id = this.route.snapshot.params['id'];
		if (acao){
			this.header = "Cadastrar Hospital"
			this.cadastrando = true
		}

		if (id){
			this.header = "Alterar Hospital"
			this.hospitalId = id
			this.atualizando = true
			this.hospitalService.buscarPorId(this.hospitalId).subscribe(res => {
				if (res.success){
					this.hospital = res.data
				}
			})
		}
	}

	limpar(){
		this.hospital = { nome:"", id_auvo:"" }
	}
	cadastrar(){
		if(this.hospital.id_auvo == ""){
			this.toastr.warning("Digite o ID Auvo")
			return
		}
		this.hospitalService.cadastrar(this.hospital).subscribe(res =>{
			if(res.success){
				this.limpar()
			}
		})
	}

	atualizar(){
		this.hospitalService.atualizar(this.hospitalId, this.hospital).subscribe()
	}

}
