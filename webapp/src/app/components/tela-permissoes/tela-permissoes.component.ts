import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { PermissoesService } from '../../services/permissoes.service'
import { UsuarioService } from '../../services/usuario.service'

@Component({
	selector: 'app-tela-permissoes',
	templateUrl: './tela-permissoes.component.html',
	styleUrls: ['./tela-permissoes.component.css']
})
export class TelaPermissoesComponent implements OnInit {
	acoesPermitidas = []

	filter: string
	permissoes = []
	permissaoDeletada:any = {}

	constructor(private permissoesService: PermissoesService, private router: Router, private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(res => {
			this.acoesPermitidas = res.data.permissoes.map( p =>{return p.acao_id})
		})

		this.atualizar()
	}

	cadastrar(){
		this.router.navigate(['/permissoes/cadastrar'])
	}

	editar(id){
		this.router.navigate(['/permissoes/editar/'+id])
	}

	atualizar(){
		this.permissoesService.getAll().subscribe(res => {
			if (res.success){
				this.permissoes = res.data
			}
		})
	}

	abrirModal(permissao){
		this.permissaoDeletada = permissao
	}

	deletar(id){
		this.permissoesService.delete(id).subscribe(res => {
			if (res.success){
				this.atualizar()
			}
		})
	}

}
