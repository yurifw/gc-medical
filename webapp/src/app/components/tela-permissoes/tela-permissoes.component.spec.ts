import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaPermissoesComponent } from './tela-permissoes.component';

describe('TelaPermissoesComponent', () => {
  let component: TelaPermissoesComponent;
  let fixture: ComponentFixture<TelaPermissoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaPermissoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaPermissoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
