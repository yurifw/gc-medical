import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { MedicoService } from '../../services/medico.service'
import { UsuarioService } from '../../services/usuario.service'

@Component({
	selector: 'app-tela-medicos',
	templateUrl: './tela-medicos.component.html',
	styleUrls: ['./tela-medicos.component.css']
})
export class TelaMedicosComponent implements OnInit {
	acoesPermitidas = []

	filter: string
	medicoDeletado: any = {}
	medicos = []
	p: number = 1

	constructor(private router: Router, private medicoService: MedicoService, private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		this.usuarioService.getInfo().subscribe(res => {
			this.acoesPermitidas = res.data.permissoes.map( p =>{return p.acao_id})
		})

		this.atualizar()
	}

	atualizar(){
		this.medicoService.getAll().subscribe(res => {
			if(res.success){
				this.medicos = res.data
			}
		})
	}

	deletar(medicoId){
		this.medicoService.delete(medicoId).subscribe(res => {
			if (res.success){
				this.atualizar()
			}
		})
	}

	abrirModal(medico){
		this.medicoDeletado = medico
	}

	editar(id){
		this.router.navigate(['/medico/editar/'+id])
	}

	cadastrar(){
		this.router.navigate(['/medico/cadastrar'])
	}

}
