import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaMedicosComponent } from './tela-medicos.component';

describe('TelaMedicosComponent', () => {
  let component: TelaMedicosComponent;
  let fixture: ComponentFixture<TelaMedicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaMedicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaMedicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
