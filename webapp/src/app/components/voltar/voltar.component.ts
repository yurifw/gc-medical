import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
	selector: 'app-voltar',
	templateUrl: './voltar.component.html',
	styleUrls: ['./voltar.component.css']
})
export class VoltarComponent implements OnInit {

	constructor(private location: Location) { }

	ngOnInit(): void {
	}

	voltar(){
		this.location.back()
	}

}
