import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaGestorComponent } from './tela-gestor.component';

describe('TelaGestorComponent', () => {
  let component: TelaGestorComponent;
  let fixture: ComponentFixture<TelaGestorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaGestorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaGestorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
