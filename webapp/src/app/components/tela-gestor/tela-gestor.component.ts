import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { GestorService } from '../../services/gestor.service'
import { UsuarioService } from '../../services/usuario.service'

@Component({
	selector: 'app-tela-gestor',
	templateUrl: './tela-gestor.component.html',
	styleUrls: ['./tela-gestor.component.css']
})
export class TelaGestorComponent implements OnInit {
	acoesPermitidas = []

	filter: string
	gestorDeletado: any = {}
	gestores = []
	p: number = 1

	constructor(private router: Router, private gestorService: GestorService, private usuarioService: UsuarioService) { }

	ngOnInit(): void {
		this.atualizar()
	}

	atualizar(){
		this.usuarioService.getInfo().subscribe(res => {
			this.acoesPermitidas = res.data.permissoes.map( p =>{return p.acao_id})
		})

		this.gestorService.getAll().subscribe(res => {
			if(res.success){
				this.gestores = res.data
			}
		})
	}

	deletar(gestorId){
		this.gestorService.delete(gestorId).subscribe(res => {
			if (res.success){
				this.atualizar()
			}
		})
	}

	abrirModal(gestor){
		this.gestorDeletado = gestor
	}

	editar(id){
		this.router.navigate(['/gestor-hospitalar/editar/'+id])
	}

	cadastrar(){
		this.router.navigate(['/gestor-hospitalar/cadastrar'])
	}

}
