import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaEsqueciSenhaComponent } from './tela-esqueci-senha.component';

describe('TelaEsqueciSenhaComponent', () => {
  let component: TelaEsqueciSenhaComponent;
  let fixture: ComponentFixture<TelaEsqueciSenhaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelaEsqueciSenhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaEsqueciSenhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
