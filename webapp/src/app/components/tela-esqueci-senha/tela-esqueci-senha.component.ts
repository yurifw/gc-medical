import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { Resposta } from '../../commons'
import { ToastrService } from 'ngx-toastr';

@Component({
	selector: 'app-tela-esqueci-senha',
	templateUrl: './tela-esqueci-senha.component.html',
	styleUrls: ['./tela-esqueci-senha.component.css']
})
export class TelaEsqueciSenhaComponent implements OnInit {
	email:string

	constructor(private auth: AuthService, private toastr: ToastrService) { }

	ngOnInit(): void {
	}

	enviarEmail(){
		if (!this.email){
			this.toastr.warning("Digite o email")
			return
		}
		this.auth.recuperarSenha(this.email).subscribe()
	}
}
