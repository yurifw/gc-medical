import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { Resposta } from '../../commons'
import { ToastrService } from 'ngx-toastr';
import {Router} from "@angular/router"

@Component({
	selector: 'app-tela-login',
	templateUrl: './tela-login.component.html',
	styleUrls: ['./tela-login.component.css']
})
export class TelaLoginComponent implements OnInit {
	email: string
	senha: string

	constructor(
		private auth: AuthService,
		private toastr: ToastrService,
		private router: Router
	) { }

	ngOnInit(): void {
	}

	login(){
		console.log("logando")
		if (!this.email){
			this.toastr.warning('Digite o email');
			return
		}
		if (!this.senha){
			this.toastr.warning('Digite a senha');
			return
		}


		this.auth.getToken(this.email, this.senha).subscribe((res:Resposta) => {
			if (res.success){
				localStorage.setItem('jwt', res.data);
				this.router.navigate(['/home'])
			} else {
				this.toastr.error(res.msg)
			}
		})
	}

}
