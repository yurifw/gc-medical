import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';

import { ApiInterceptor } from './api.interceptor'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabelaCirurgiasComponent } from './components/tabela-cirurgias/tabela-cirurgias.component';
import { TelaLoginComponent } from './components/tela-login/tela-login.component';
import { TelaEsqueciSenhaComponent } from './components/tela-esqueci-senha/tela-esqueci-senha.component';
import { TelaResetarSenhaComponent } from './components/tela-resetar-senha/tela-resetar-senha.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { TelaCadastroUsuarioComponent } from './components/tela-cadastro-usuario/tela-cadastro-usuario.component';
import { TelaPerfilComponent } from './components/tela-perfil/tela-perfil.component';
import { TelaPacientesComponent } from './components/tela-pacientes/tela-pacientes.component';
import { TelaFormPacienteComponent } from './components/tela-form-paciente/tela-form-paciente.component';
import { TelaMedicosComponent } from './components/tela-medicos/tela-medicos.component';
import { TelaFormMedicoComponent } from './components/tela-form-medico/tela-form-medico.component';
import { VoltarComponent } from './components/voltar/voltar.component';
import { TelaHospitaisComponent } from './components/tela-hospitais/tela-hospitais.component';
import { TelaFormHospitalComponent } from './components/tela-form-hospital/tela-form-hospital.component';
import { TelaFormGestorComponent } from './components/tela-form-gestor/tela-form-gestor.component';
import { TelaGestorComponent } from './components/tela-gestor/tela-gestor.component';
import { DocumentosComponent } from './components/documentos/documentos.component';
import { TelaPermissoesComponent } from './components/tela-permissoes/tela-permissoes.component';
import { TelaFormPermissoesComponent } from './components/tela-form-permissoes/tela-form-permissoes.component';
import { TelaDetalhesCirurgiaComponent } from './components/tela-detalhes-cirurgia/tela-detalhes-cirurgia.component';
import { BtnAcaoEncaminhamentoComponent } from './components/btn-acao-encaminhamento/btn-acao-encaminhamento.component';
import { TelaHistoricoComponent } from './components/tela-historico/tela-historico.component';
import { TelaMateriaisComponent } from './components/tela-materiais/tela-materiais.component';
import { TelaUsuariosComponent } from './components/tela-usuarios/tela-usuarios.component';

@NgModule({
	declarations: [
		AppComponent,
		TabelaCirurgiasComponent,
		TelaLoginComponent,
		TelaEsqueciSenhaComponent,
		TelaResetarSenhaComponent,
		HomeComponent,
		HeaderComponent,
		TelaCadastroUsuarioComponent,
		TelaPerfilComponent,
		TelaPacientesComponent,
		TelaFormPacienteComponent,
		TelaMedicosComponent,
		TelaFormMedicoComponent,
		VoltarComponent,
		TelaHospitaisComponent,
		TelaFormHospitalComponent,
		TelaFormGestorComponent,
		TelaGestorComponent,
		DocumentosComponent,
		TelaPermissoesComponent,
		TelaFormPermissoesComponent,
		TelaDetalhesCirurgiaComponent,
		BtnAcaoEncaminhamentoComponent,
		TelaHistoricoComponent,
		TelaMateriaisComponent,
		TelaUsuariosComponent
	],
	imports: [
		AngularMyDatePickerModule,	//https://github.com/kekeh/angular-mydatepicker
		Ng2SearchPipeModule,		//https://www.npmjs.com/package/ng2-search-filter
		NgxPaginationModule,		//https://www.npmjs.com/package/ngx-pagination
		BrowserModule,
		FormsModule,
		HttpClientModule,
		AppRoutingModule,
		ToastrModule.forRoot({ positionClass: 'toast-bottom-center', preventDuplicates: true }),
		BrowserAnimationsModule
	],
	providers: [{ provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true }],
	bootstrap: [AppComponent]
})
export class AppModule { }
