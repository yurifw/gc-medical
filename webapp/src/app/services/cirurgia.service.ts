import { Injectable } from '@angular/core';
import {SERVER, Resposta} from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class CirurgiaService {

	constructor(private http: HttpClient) { }

	getAll(){
		return this.http.get<Resposta>(SERVER+"/cirurgia")
	}

	getById(cirurgiaId){
		return this.http.get<Resposta>(SERVER+"/cirurgia/"+cirurgiaId)
	}

	atualizar(id, cirurgia){
		return this.http.patch<Resposta>(SERVER+"/cirurgia/"+id, cirurgia)
	}

	cadastrar(cirurgia){
		return this.http.post<Resposta>(SERVER+"/cirurgia", cirurgia)
	}

}
