import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../commons"

@Injectable({
	providedIn: 'root'
})
export class PacienteService {

	constructor(private http: HttpClient) { }

	getAll(){
		return this.http.get<Resposta>(SERVER+"/pacientes")
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/pacientes/"+id)
	}

	cadastrar(paciente){
		return this.http.post<Resposta>(SERVER+"/pacientes", paciente)
	}

	atualizar(id, paciente){
		return this.http.patch<Resposta>(SERVER+"/pacientes/"+id, paciente)
	}

	buscarPorId(id){
		return this.http.get<Resposta>(SERVER+"/pacientes/"+id)
	}
}
