import { Injectable } from '@angular/core';
import {SERVER, Resposta} from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class PermissoesService {

	constructor(private http: HttpClient) { }

	getAll(){
		return this.http.get<Resposta>(SERVER+"/permissoes")
	}

	getById(perfilId){
		return this.http.get<Resposta>(SERVER+"/permissoes/"+perfilId)
	}

	getAcoes(){
		return this.http.get<Resposta>(SERVER+"/permissoes/acoes")
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/permissoes/"+id)
	}

	cadastrar(permissao){
		return this.http.post<Resposta>(SERVER+"/permissoes", permissao)
	}

	atualizar(permissaoId, permissao){
		return this.http.patch<Resposta>(SERVER+"/permissoes/"+permissaoId, permissao)
	}
}
