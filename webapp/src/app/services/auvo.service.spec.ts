import { TestBed } from '@angular/core/testing';

import { AuvoService } from './auvo.service';

describe('AuvoService', () => {
  let service: AuvoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuvoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
