import { TestBed } from '@angular/core/testing';

import { CirurgiaService } from './cirurgia.service';

describe('CirurgiaService', () => {
  let service: CirurgiaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CirurgiaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
