import { Injectable } from '@angular/core';
import {SERVER, Resposta} from "../commons"
import { HttpClient } from '@angular/common/http';


@Injectable({
	providedIn: 'root'
})
export class EncaminhamentoService {

	constructor(private http: HttpClient) { }

	getById(id){
		return this.http.get<Resposta>(SERVER+"/encaminhamentos/"+id)
	}

	getCHM(){
		return this.http.get<Resposta>(SERVER+"/encaminhamentos/chm")
	}

	getMedicos(){
		return this.http.get<Resposta>(SERVER+"/encaminhamentos/medicos")
	}

	getComercial(){
		return this.http.get<Resposta>(SERVER+"/encaminhamentos/comercial")
	}

	moverEncaminhamento(encaminhamento){
		return this.http.post<Resposta>(SERVER+"/encaminhamentos/mover-para", encaminhamento)
	}

	getHistorico(cirurgiaId){
		return this.http.get<Resposta>(SERVER+"/historico/"+cirurgiaId)
	}

	getStatus(){
		return this.http.get<Resposta>(SERVER+"/encaminhamentos/status-possiveis")
	}

}
