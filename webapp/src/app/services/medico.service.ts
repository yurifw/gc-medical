import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../commons"

@Injectable({
	providedIn: 'root'
})
export class MedicoService {

	constructor(private http: HttpClient) { }

	getAll(){
		return this.http.get<Resposta>(SERVER+"/medico")
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/medico/"+id)
	}

	cadastrar(medico){
		return this.http.post<Resposta>(SERVER+"/medico", medico)
	}

	atualizar(id, medico){
		return this.http.patch<Resposta>(SERVER+"/medico/"+id, medico)
	}

	buscarPorId(id){
		return this.http.get<Resposta>(SERVER+"/medico/"+id)
	}
}
