import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../commons"

@Injectable({
	providedIn: 'root'
})
export class UsuarioService {

	constructor(private http: HttpClient) { }

	cadastrar(usuario){
		return this.http.post<Resposta>(SERVER+"/usuario", usuario)
	}

	getInfo(){
		return this.http.get<Resposta>(SERVER+"/usuario")
	}

	atualizar(usuario){ //usuario alterando seus proprios dados
		return this.http.patch<Resposta>(SERVER+"/usuario", usuario)
	}

	update(id, usuario){ //usuario alterando dados de outros usuarios
		return this.http.patch<Resposta>(SERVER+"/usuario/"+id, usuario)
	}

	selectAll(){
		return this.http.get<Resposta>(SERVER+"/usuarios")
	}

	buscarPorId(id){
		return this.http.get<Resposta>(SERVER+"/usuario/"+id)
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/usuario/"+id)
	}
}
