import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../commons"

@Injectable({
	providedIn: 'root'
})
export class DocumentosService {

	constructor(private http: HttpClient) { }

	buscarPorCirurgia(cirurgiaId){
		return this.http.get<Resposta>(SERVER+"/cirurgia/"+cirurgiaId+"/documentos")
	}

	delete(documentoId){
		return this.http.delete<Resposta>(SERVER+"/documento/"+documentoId)
	}

	download(documentoId){
		this.http.get(SERVER+"/documento/"+documentoId, { responseType: 'blob',observe:'response', reportProgress: true }).subscribe(resp => {
			if (resp.headers.get('content-type') == "application/json"){
				console.log("Ação não permitida")
			} else {
				let name = resp.headers.get('content-disposition').split('=')[1]
				name = name.replace(/\"/g,"")
				let url = window.URL.createObjectURL(resp.body);
				var a = window.document.createElement("a");
				a.href = url
				a.download = name;
				console.log(a)
				console.log(name)
				document.body.appendChild(a);
				a.click();  // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
				document.body.removeChild(a);
			}

		})

	}

	cadastrar(documento){
		return this.http.post<Resposta>(SERVER+"/documento", documento)
	}
}
