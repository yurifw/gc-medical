import { Injectable } from '@angular/core';
import {SERVER, Resposta} from "../commons"
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class CaixaService {

	constructor(private http: HttpClient) { }

	search(term){
		return this.http.get<Resposta>(SERVER+"/caixas/"+term)
	}

	searchById(id){
		return this.http.get<Resposta>(SERVER+"/caixa/"+id)
	}

}
