import { TestBed } from '@angular/core/testing';

import { EncaminhamentoService } from './encaminhamento.service';

describe('EncaminhamentoService', () => {
  let service: EncaminhamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EncaminhamentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
