import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../commons"

@Injectable({
	providedIn: 'root'
})
export class AuvoService {

	constructor(private http: HttpClient) { }

	getUsers(){
		return this.http.get<Resposta>(SERVER+"/auvo/usuarios")
	}
}
