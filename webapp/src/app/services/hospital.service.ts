import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../commons"

@Injectable({
	providedIn: 'root'
})
export class HospitalService {

	constructor(private http: HttpClient) { }

	getAll(){
		return this.http.get<Resposta>(SERVER+"/hospital")
	}

	delete(id){
		return this.http.delete<Resposta>(SERVER+"/hospital/"+id)
	}

	buscarPorId(id){
		return this.http.get<Resposta>(SERVER+"/hospital/"+id)
	}

	cadastrar(hospital){
		return this.http.post<Resposta>(SERVER+"/hospital", hospital)
	}

	atualizar(hospitalId, hospital){
		return this.http.patch<Resposta>(SERVER+"/hospital/"+hospitalId, hospital)
	}
}
