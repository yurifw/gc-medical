import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {SERVER, Resposta} from "../commons"

@Injectable({
	providedIn: 'root'
})
export class GestorService {

	constructor(private http: HttpClient) { }

	delete(gestorId){
		return this.http.delete<Resposta>(SERVER+"/gestor-hospitalar/"+gestorId)
	}

	getAll(){
		return this.http.get<Resposta>(SERVER+"/gestor-hospitalar")
	}

	cadastrar(gestor){
		return this.http.post<Resposta>(SERVER+"/gestor-hospitalar", gestor)
	}

	atualizar(id, gestor){
		return this.http.patch<Resposta>(SERVER+"/gestor-hospitalar/"+id, gestor)
	}

	buscarPorId(id){
		return this.http.get<Resposta>(SERVER+"/gestor-hospitalar/"+id)
	}
}
