import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router"
import {SERVER, Resposta} from "../commons"


@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private http: HttpClient) { }

	getToken(email:string, password:string){
		let body = {"email":email, "senha":password}
		return this.http.post<Resposta>(SERVER+"/login", body)
	}

	recuperarSenha(email:string){
		let body = {"email":email}
		return this.http.post<Resposta>(SERVER+"/esqueci-senha", body)
	}

	resetarSenha(codigo:string, senha:string){
		let body = {"codigo":codigo, "senha":senha}
		return this.http.patch<Resposta>(SERVER+"/resetar-senha", body)
	}
}
