from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, LISTAR_PACIENTES, ATUALIZAR_PACIENTE, DELETAR_PACIENTE, CADASTRAR_PACIENTE

def _load_paciente_id(paciente_id, cursor):
	sql = "select id, nome, numero, DATE_FORMAT(nascimento, '%%d/%%m/%%Y') AS nascimento, sexo, cidade_residencia from paciente where id = %s"
	cursor.execute(sql,[paciente_id])
	return cursor.fetchone()

@requires(LISTAR_PACIENTES)
def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select id, nome, numero, DATE_FORMAT(nascimento, '%d/%m/%Y') AS nascimento, sexo, cidade_residencia from paciente order by nome")
		pacientes = cursor.fetchall()
		return make_response({"msg":None,"data":pacientes, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_PACIENTES)
def select_id(paciente_id):
	conn, cursor = db.ready()
	try:
		paciente = _load_paciente_id(paciente_id, cursor)
		return make_response({"msg":None,"data":paciente, "success":True})
	finally:
		cursor.close()
		conn.close()


@requires(DELETAR_PACIENTE)
def deletar(paciente_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("delete from paciente where id = %s", [paciente_id])
		conn.commit()
		return make_response({"msg":"Paciente deletado","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_PACIENTE)
def atualizar(paciente_id):
	conn, cursor = db.ready()
	try:
		paciente = request.json
		data = [paciente['nome'], paciente['numero'], paciente['nascimento'], paciente['sexo'], paciente['cidade_residencia'], paciente_id]
		cursor.execute("update paciente set nome=%s, numero=%s, nascimento=STR_TO_DATE(%s, '%%d/%%m/%%Y'), sexo=%s, cidade_residencia=%s where id=%s", data)
		conn.commit()
		return make_response({"msg":"Paciente atualizado","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()

@requires(CADASTRAR_PACIENTE)
def insert():
	conn, cursor = db.ready()
	try:
		paciente = request.json
		data = [paciente['nome'], paciente['numero'], paciente['nascimento'], paciente['cidade_residencia'], paciente['sexo']]
		cursor.execute("insert into paciente (nome, numero, nascimento, cidade_residencia, sexo) values (%s, %s, STR_TO_DATE(%s, '%%d/%%m/%%Y'), %s, %s)", data)
		conn.commit()
		return make_response({"msg":"Paciente cadastrado","data":None, "success":True})
	finally:
		cursor.close()
		conn.close()
