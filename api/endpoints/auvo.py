import requests
import json
from datetime import datetime, timedelta
from flask import Flask, make_response
import codecs
from .auth import requires, decode_token, ENCAMINHAR_CIRURGIA

# https://auvoapiv2.docs.apiary.io/
API_URL = "https://api.auvo.com.br/v2"
APP_KEY = "l1xubcHs10hrFW1KmWCS5JLcneCmrC9"
TOKEN = "l1xubcHs10g0iZhJkNGnSZbPphZenO87"

def update_task_date():
	pass

def authenticate():
	url = "%s/login/" % API_URL
	response = requests.post(url, json = {"apiKey":APP_KEY, "apiToken":TOKEN})
	return json.loads(response.text)['result']['accessToken']

def create_task(task_type, orientation, task_date, client_id, id_user_to):
	auth = authenticate()
	client = list_client(auth, client_id)

	# url = "https://private-anon-8d8437b38c-auvoapiv2.apiary-mock.com/v2/tasks/"
	url = "%s/tasks/" % API_URL
	headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer %s' % auth
	}

	group_id = 0
	questionario_id = 0
	if task_type == 9224:  # entrega equipamento
		questionario_id = 7813
	if task_type == 9225:  # retirada equip
		questionario_id = 7814
	if task_type == 9217:  # instrumentacao cirurgia eletiva
		questionario_id = 7809
	if task_type == 9218:  # Instrumentação de cirurgia de urgência
		questionario_id = 7809


	data = {
		"taskType": task_type,
		"idUserFrom": 6048,  														# required
		"taskDate": task_date.strftime("%Y-%m-%dT%H:%M:%S"),
		"latitude": client['latitude'],  													# required
		"longitude": client['longitude'],  													# required
		"address": client['address'],  												# required
		"orientation": orientation,  												# required
		"pendency" : "PENDENCIA TESTE",
		"priority": 0,											# required
		"questionnaireId": questionario_id
	}

	if task_type == 9217 or task_type == 9218:
		data['idUserTo'] = id_user_to
	if task_type == 9224 or task_type == 9225:
		# data['TeamId'] = 2086
		data['idUserTo'] = 20806

	response = requests.post(url, headers = headers, json = data)
	response = json.loads(response.text)

	if 'result' in response:
		return response["result"]
	else:
		msg = json.dumps(response)
		if "taskDate" in msg:
			raise ValueError("Erro no auvo: data inválida")
		else:
			raise Exception(msg)

def list_tasks(auth, start_date, end_date):

	start_date = start_date.strftime("%Y-%m-%dT%H:%M:%S")
	end_date = end_date.strftime("%Y-%m-%dT%H:%M:%S")

	headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer %s' % auth
	}
	page = 1
	page_size = 20
	filters = json.dumps({
		'startDate': start_date,
		'endDate': end_date
	})
	url = 'https://private-anon-8d8437b38c-auvoapiv2.apiary-mock.com/v2/tasks/?page=%s&pageSize=%s' % (page, page_size)
	url = "%s/tasks/?page=%s&pageSize=%s&paramFilter=%s" % (API_URL, page, page_size, filters)

	response = requests.get(url, headers=headers)
	if len(response.text) == 0:
		return []
	response = json.loads(response.text)
	tasks = response['result']['entityList']
	return_data = response['result']['pagedSearchReturnData']

	while return_data['pageSize'] * return_data['page'] < return_data['totalItems']:
		links = response['result']['links']
		for link in links:
			if link['rel'] == 'nextPage':
				response = requests.get(link['href'], headers=headers)
				response = json.loads(response.text)
				tasks.extend(response['result']['entityList'])
				return_data = response['result']['pagedSearchReturnData']

	return tasks

def list_task_types(auth):
	headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer %s' % auth
	}
	page = 1
	page_size = 20
	url = 'https://private-anon-8d8437b38c-auvoapiv2.apiary-mock.com/v2/tasks/?page=%s&pageSize=%s' % (page, page_size)
	url = "%s/taskTypes/?page=%s&pageSize=%s" % (API_URL, page, page_size)

	response = requests.get(url, headers=headers)
	response = json.loads(response.text)
	return response['result']['entityList']

def delete_task(auth, task_id):
	url = "https://private-anon-8d8437b38c-auvoapiv2.apiary-mock.com/v2/tasks/%s" % (task_id)
	url = "%s/tasks/%s" % (API_URL, task_id)
	headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer %s' % auth
	}
	response = requests.delete(url, headers=headers)
	if response.status_code != 204:
		raise Exception("Erro ao deletar Auvo Task")

def list_client(auth, id):
	headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer %s' % auth
	}
	url = "%s/customers/%s" % (API_URL, id)
	response = requests.get(url, headers=headers)
	response = json.loads(response.text)
	return response['result']

@requires(ENCAMINHAR_CIRURGIA)
def list_users():
	auth = authenticate()
	headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bearer %s' % auth
	}
	url = "%s/users/" % (API_URL)
	users = []
	has_next = True

	while has_next:
		try:
			response = requests.get(url, headers=headers)
			response = json.loads(response.text)
			users.extend(response['result']['entityList'])
			if len(response['result']['links']) > 1:
				url = response['result']['links'][-1]['href']
			else:
				has_next = False
		except:
			return make_response({"msg":None,"data":users, "success":True})
	return make_response({"msg":None,"data":users, "success":True})
