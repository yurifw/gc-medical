import pymysql

MYSQL_HOST = 'localhost'
MYSQL_USER = 'gctest'
MYSQL_PASSWORD = 'gTHNjAziR'
MYSQL_DB = 'gcmedical'
MYSQL_PORT = 3306

def ready():
	conn = pymysql.connect(
        host=MYSQL_HOST,
        db=MYSQL_DB,
        user=MYSQL_USER,
        password=MYSQL_PASSWORD,
        cursorclass=pymysql.cursors.DictCursor
    )
	conn.autocommit(False)
	cursor = conn.cursor()
	return (conn, cursor)

def connect_gc():
	# mysql -u gcmbi -p'GcM1@bi' -h vsvimanvm01.eastus.cloudapp.azure.com -P 3306 -D gcmvsbi
	conn = pymysql.connect(
        host="vsvimanvm01.eastus.cloudapp.azure.com",
        db="gcmvsbi",
        user="gcmbi",
        password="GcM1@bi",
        cursorclass=pymysql.cursors.DictCursor
    )
	conn.autocommit(False)
	cursor = conn.cursor()
	return (conn, cursor)
