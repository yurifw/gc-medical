from datetime import datetime
from flask import Flask, make_response
from flask import request
import bcrypt
from .send_email import send_mail, TEMPLATES
from . import database as db
import endpoints.cirurgia
from . import auvo
from .auth import requires, decode_token, LISTAR_ENCAMINHAMENTOS_SETOR, ENCAMINHAR_CIRURGIA
from .setor import CHM, COMERCIAL, LOGISTICA, INSTRUMENTACAO, HOSPITAL


STATUS_FINALIZADO_ID = 1  # FINALIZADO = alterado para 'Pendente Pagamento'
STATUS_ENCAMINHADO_PARA_CHM = 2
STATUS_APROVADO_PELA_CHM = 3
STATUS_REPROVADO_PELA_CHM = 4
STATUS_COMERCIAL_PARA_LOGISTICA = 5
STATUS_COMERCIAL_PARA_INSTRUMENTACAO = 6
STATUS_REPROVADO_PELO_COMERCIAL = 7
STATUS_FATURAMENTO_SOLICITADO = 8
STATUS_FATURAMENTO_APROVADO = 9
STATUS_FATURAMENTO_REPROVADO = 10

STATUS_CHM = [
	STATUS_ENCAMINHADO_PARA_CHM,
	STATUS_REPROVADO_PELO_COMERCIAL,
	STATUS_FATURAMENTO_SOLICITADO,
	STATUS_FINALIZADO_ID
]

STATUS_MEDICOS = [
	STATUS_REPROVADO_PELA_CHM
]

STATUS_COMERCIAL = [
	STATUS_APROVADO_PELA_CHM,
	STATUS_FATURAMENTO_APROVADO,
	STATUS_FATURAMENTO_REPROVADO,
	STATUS_FINALIZADO_ID
]

def _encaminhar_cirurgia(cirurgia_id, status_encaminhamento_id, observacao, usuario_id, cursor):
	sql = "insert into encaminhamento (data_encaminhamento, cirurgia_id, status_encaminhamento_id, observacao, usuario_id) values (now(), %s, %s, %s, %s)"
	data = [cirurgia_id, status_encaminhamento_id, observacao, usuario_id]
	cursor.execute(sql, data)

	cursor.execute("select nome from usuario where id = %s", [usuario_id])
	u = cursor.fetchone()
	usuario = u['nome']

	data_alteracao = datetime.now()
	data_alteracao = data_alteracao.strftime("%d/%m/%Y %H:%M:%S")

	setor_id = 0
	if status_encaminhamento_id in [STATUS_ENCAMINHADO_PARA_CHM, STATUS_REPROVADO_PELO_COMERCIAL, STATUS_FATURAMENTO_SOLICITADO]:
		setor_id = CHM
	if status_encaminhamento_id == STATUS_REPROVADO_PELA_CHM:
		setor_id = HOSPITAL
	if status_encaminhamento_id in [STATUS_APROVADO_PELA_CHM, STATUS_FATURAMENTO_APROVADO, STATUS_FATURAMENTO_REPROVADO]:
		setor_id = COMERCIAL

	if setor_id != 0:
		cursor.execute("select procedimento, date_format(data, '%%d/%%m/%%Y %%H:%%i') as data from cirurgia where id =%s", [cirurgia_id])
		c = cursor.fetchone()
		cursor.execute("select email from usuario where setor_id = %s", [setor_id])
		r = cursor.fetchall()
		cursor.execute("select * from status_encaminhamento where id = %s", [status_encaminhamento_id])
		s = cursor.fetchone()
		texto_status = s['nome']
		emails = []
		for email in r:
			emails.append(email['email'])
		corpo = TEMPLATES['NOVO_STATUS'].format(
			procedimento = c['procedimento'],
			data = c['data'],
			usuario=usuario,
			data_alteracao=data_alteracao,
			status = texto_status)
		send_mail(emails, "Cirurgia atualizada!", corpo)

def _load_encaminhamento(encaminhamento, cursor):
	encaminhamento['data_encaminhamento'] = encaminhamento['data_encaminhamento'].strftime("%d/%m/%Y %H:%M:%S")

	cirurgia_id = encaminhamento['cirurgia_id']
	del encaminhamento['cirurgia_id']
	encaminhamento['cirurgia'] = endpoints.cirurgia._load_cirurgia_id(cirurgia_id, cursor)
	cursor.execute("select * from status_encaminhamento where id = %s", [encaminhamento['status_encaminhamento_id']])
	s = cursor.fetchone()
	encaminhamento['status'] = s
	del encaminhamento['status_encaminhamento_id']

	for s in encaminhamento['cirurgia']['solicitacoes']:
		s['valor'] = float(s['valor'])
	return encaminhamento

def _filter_encaminhamentos(encaminhamentos, cursor):
	filtrados = []
	cirurgia_id_adicionados = []
	for encaminhamento in encaminhamentos:


		if encaminhamento['cirurgia_id'] not in cirurgia_id_adicionados:
			cirurgia_id_adicionados.append(encaminhamento['cirurgia_id'])
			encaminhamento = _load_encaminhamento(encaminhamento, cursor)
			filtrados.append(encaminhamento)

			for s in encaminhamento['cirurgia']['solicitacoes']:
				s['valor'] = float(s['valor'])

	return filtrados

@requires(LISTAR_ENCAMINHAMENTOS_SETOR)
def list_id(encaminhamento_id):
	conn, cursor = db.ready()
	try:
		sql = "select * from encaminhamento where id = %s order by data_encaminhamento desc"
		cursor.execute(sql, [encaminhamento_id])
		encaminhamento = _load_encaminhamento(cursor.fetchone(), cursor)

		return make_response({"msg":None, "success":True, "data":encaminhamento})

	# except Exception as e:
	# 	msg = str(e)
	# 	return make_response({"msg":msg, "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(ENCAMINHAR_CIRURGIA)
def mover_encaminhamento():
	conn, cursor = db.ready()
	try:
		usuario_id = decode_token(request.headers['Authorization'])['payload']['user_id']
		encaminhamento = request.json
		encaminhamento['status_id'] = int(encaminhamento['status_id'])
		print(encaminhamento)
		if 'observacao' not in encaminhamento:
			encaminhamento['observacao'] = ''
			
		_encaminhar_cirurgia(encaminhamento['cirurgia_id'], encaminhamento['status_id'], encaminhamento['observacao'], usuario_id, cursor)

		if encaminhamento['status_id'] in [STATUS_COMERCIAL_PARA_LOGISTICA, STATUS_COMERCIAL_PARA_INSTRUMENTACAO]:
			sql = "select h.id_auvo	from hospital h, usuario u, medico m, cirurgia c where c.medico = u.id and m.usuario_id = u.id and m.hospital_id = h.id and c.id = %s"
			cursor.execute(sql, [encaminhamento['cirurgia_id']])
			cliente_id = cursor.fetchone()['id_auvo']
			data_tarefa = datetime.strptime(encaminhamento['data_auvo'], "%d/%m/%Y %H:%M")

			sql = "select m.nome, s.solicitado from conteudo_caixa cc, solicitacao s, material m where cc.material_id = m.id and s.conteudo_caixa_id=cc.id and s.cirurgia_id = %s"
			cursor.execute(sql, [encaminhamento['cirurgia_id']])
			conteudos = cursor.fetchall()
			obs = ""
			for conteudo in conteudos:
				if conteudo['solicitado'] > 0:
					obs = "%s + %s (%s solictado)" % (obs, conteudo['nome'], conteudo['solicitado'])
			obs = "%s - %s" % (obs[2:], encaminhamento['observacao'])

			auvo.create_task(encaminhamento['task_type'], obs, data_tarefa, cliente_id, encaminhamento['id_to_auvo'])

		conn.commit()
		return make_response({"msg":"Cirurgia encaminhada com sucesso", "success":True, "data":None})

	# except ValueError as e:
	# 	if 'time data' in str(e):
	# 		return make_response({"msg":"Data inválida", "success":False, "data":None})
	# 	if 'auvo' in str(e):
	# 		return make_response({"msg":str(e), "success":False, "data":None})
	# except Exception as e:
	# 	return make_response({"msg":str(e), "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_ENCAMINHAMENTOS_SETOR)
def list_chm():  #CHM = Gestor Hospitalar
	d = decode_token(request.headers['Authorization'])['payload']
	setor_id = d['setor_id']
	usuario_id = d['user_id']
	if setor_id != CHM:
		return make_response({"msg":None, "success":False, "data":"Sem permissão para visualizar os encaminhamentos deste setor"})
	conn, cursor = db.ready()
	try:

		hospital_id = None
		cursor.execute("select * from gestor_hospitalar where usuario_id = %s", [usuario_id])
		gestor = cursor.fetchone()
		hospital_id = None if gestor is None else gestor['hospital_id']
		if hospital_id is None:
			cursor.execute("select * from medico where usuario_id = %s", [usuario_id])
			medico = cursor.fetchone()
			hospital_id = None if medico is None else medico['hospital_id']

		if hospital_id is None:
			sql = "select * from encaminhamento where ultimo_status_cirurgia(cirurgia_id) in (%s, %s, %s, %s) order by data_encaminhamento desc"
			cursor.execute(sql, STATUS_CHM)
		else:
			sql = "select e.* from encaminhamento e, cirurgia c, medico m where ultimo_status_cirurgia(cirurgia_id) in (%s, %s, %s, %s) and e.cirurgia_id = c.id and c.medico = m.usuario_id and m.hospital_id = %s order by e.data_encaminhamento desc"
			args = []
			args.extend(STATUS_CHM)
			args.append(hospital_id)
			cursor.execute(sql, args)

		encaminhamentos = _filter_encaminhamentos(cursor.fetchall(), cursor)
		return make_response({"msg":None, "success":True, "data":encaminhamentos})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_ENCAMINHAMENTOS_SETOR)
def list_medicos():
	d = decode_token(request.headers['Authorization'])['payload']
	setor_id = d['setor_id']
	usuario_id = d['user_id']
	if setor_id != HOSPITAL:
		return make_response({"msg":None, "success":False, "data":"Sem permissão para visualizar os encaminhamentos deste setor"})
	conn, cursor = db.ready()
	try:
		hospital_id = None
		cursor.execute("select * from gestor_hospitalar where usuario_id = %s", [usuario_id])
		gestor = cursor.fetchone()
		hospital_id = None if gestor is None else gestor['hospital_id']
		if hospital_id is None:
			cursor.execute("select * from medico where usuario_id = %s", [usuario_id])
			medico = cursor.fetchone()
			hospital_id = None if medico is None else medico['hospital_id']

		if hospital_id is None:
			sql = "select * from encaminhamento where ultimo_status_cirurgia(cirurgia_id) =  %s order by data_encaminhamento desc"
			cursor.execute(sql, STATUS_MEDICOS)
		else:
			sql = "select e.* from encaminhamento e, cirurgia c, medico m where ultimo_status_cirurgia(cirurgia_id) =  %s and e.cirurgia_id = c.id and c.medico = m.usuario_id and m.hospital_id = %s order by data_encaminhamento desc"
			args = []
			args.extend(STATUS_MEDICOS)
			args.append(hospital_id)
			cursor.execute(sql, args)

		encaminhamentos = _filter_encaminhamentos(cursor.fetchall(), cursor)
		return make_response({"msg":None, "success":True, "data":encaminhamentos})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_ENCAMINHAMENTOS_SETOR)
def list_comercial():
	d = decode_token(request.headers['Authorization'])['payload']
	setor_id = d['setor_id']
	usuario_id = d['user_id']

	if setor_id != COMERCIAL:
		return make_response({"msg":None, "success":False, "data":"Sem permissão para visualizar os encaminhamentos deste setor"})
	conn, cursor = db.ready()
	try:
		hospital_id = None
		cursor.execute("select * from gestor_hospitalar where usuario_id = %s", [usuario_id])
		gestor = cursor.fetchone()
		hospital_id = None if gestor is None else gestor['hospital_id']
		if hospital_id is None:
			cursor.execute("select * from medico where usuario_id = %s", [usuario_id])
			medico = cursor.fetchone()
			hospital_id = None if medico is None else medico['hospital_id']

		if hospital_id is None:
			sql = "select * from encaminhamento where ultimo_status_cirurgia(cirurgia_id) in (%s, %s, %s, %s) order by data_encaminhamento desc"
			cursor.execute(sql, STATUS_COMERCIAL)
		else:
			sql = "select * from encaminhamento e, cirurgia c, medico m where ultimo_status_cirurgia(cirurgia_id) in (%s, %s, %s, %s) and e.cirurgia_id = c.id and c.medico = m.usuario_id and m.hospital_id = %s order by data_encaminhamento desc"
			args = []
			args.extend(STATUS_COMERCIAL)
			args.append(hospital_id)
			cursor.execute(sql, args)
		encaminhamentos = _filter_encaminhamentos(cursor.fetchall(), cursor)

		return make_response({"msg":None, "success":True, "data":encaminhamentos})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_ENCAMINHAMENTOS_SETOR)
def list_status():
	setor_id = decode_token(request.headers['Authorization'])['payload']['setor_id']
	conn, cursor = db.ready()
	try:
		if setor_id == COMERCIAL:
			cursor.execute("select * from status_encaminhamento where id in (%s, %s, %s, %s)",STATUS_COMERCIAL)
			return make_response({"msg":None, "success":True, "data":cursor.fetchall()})
		if setor_id == HOSPITAL:
			cursor.execute("select * from status_encaminhamento where id in (%s)",STATUS_MEDICOS)
			return make_response({"msg":None, "success":True, "data":cursor.fetchall()})
		if setor_id == CHM:
			cursor.execute("select * from status_encaminhamento where id in (%s, %s, %s, %s)",STATUS_CHM)
			return make_response({"msg":None, "success":True, "data":cursor.fetchall()})
		return make_response({"msg":"Setor não identificado", "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()
