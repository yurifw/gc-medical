from flask import Flask, make_response
from flask import request
from . import database as db
import endpoints.cirurgia
from .auth import requires, VISUALIZAR_HISTORICO

@requires(VISUALIZAR_HISTORICO)
def select_id(cirurgia_id):
	conn, cursor = db.ready()
	try:
		sql = """
			select
				date_format(e.data_encaminhamento, '%%d/%%m/%%Y %%H:%%i') as data_encaminhamento,
				e.observacao,
				se.nome as status_encaminhamento,
				s.nome as setor,
				u.nome as usuario
			from
				encaminhamento e,
				status_encaminhamento se,
				setor s,
				usuario u
			where
				e.cirurgia_id = %s and
				se.id = e.status_encaminhamento_id and
				u.id = e.usuario_id and
				s.id = u.setor_id
			order by e.data_encaminhamento asc
		"""
		cursor.execute(sql, [cirurgia_id])
		historico = cursor.fetchall()
		return make_response({"msg":None, "success":True, "data":historico})

	except Exception as e:
		msg = str(e)
		return make_response({"msg":msg, "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()
