from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .send_email import send_mail, TEMPLATES
import jwt
from jwt.exceptions import ExpiredSignatureError
import bcrypt
from datetime import datetime, timedelta
from functools import wraps
import string
import random
import smtplib
# acoes:
APROVAR_MATERIAL = 1
SOLICITAR_MATERIAL = 2
LISTAR_CIRURGIAS_PROPRIAS = 3
ATUALIZAR_CIRURGIA = 4
CADASTRAR_USUARIO = 5
LISTAR_PACIENTES = 6
ATUALIZAR_PACIENTE = 7
DELETAR_PACIENTE = 8
CADASTRAR_PACIENTE = 9
LISTAR_MEDICOS = 10
ATUALIZAR_MEDICO = 11
DELETAR_MEDICO = 12
CADASTRAR_MEDICO = 13
LISTAR_HOSPITAIS = 14
ATUALIZAR_HOSPITAL = 15
DELETAR_HOSPITAL = 16
CADASTRAR_HOSPITAL = 17
LISTAR_GESTOR = 18
ATUALIZAR_GESTOR = 19
DELETAR_GESTOR = 20
CADASTRAR_GESTOR = 21
CADASTRAR_DOCUMENTO = 22
DELETAR_DOCUMENTO = 23
VISUALIZAR_DOCUMENTOS = 24
CADASTRAR_PERMISSOES = 25
ATUALIZAR_PERMISSOES = 26
DELETAR_PERMISSOES = 27
LISTAR_PERMISSOES = 28
LISTAR_ENCAMINHAMENTOS_SETOR = 29
ENCAMINHAR_CIRURGIA = 30
LISTAR_CIRURGIAS = 31
VISUALIZAR_HISTORICO = 32
LISTAR_USUARIOS = 33
ATUALIZAR_USUARIOS = 34
DELETAR_USUARIOS = 35

def generate_token(user_id, email, nome, setor_id, quick_expire = False):
	private_key = None
	with open('./keys/jwtRS256.key', 'r') as priv:
		private_key = priv.read().encode()
	if quick_expire:
		expiration = datetime.utcnow() + timedelta(minutes=15)
	else:
		expiration = datetime.utcnow() + timedelta(days=30)
	payload = {"user_id":user_id, "email":email,"nome":nome, "setor_id":setor_id, "exp":expiration}
	token = jwt.encode(payload, private_key, algorithm='RS256')
	return token.decode()

def get_token():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		senha = request.json['senha']

		cursor.execute('select * from usuario where email = %s', [email])
		usuario = cursor.fetchone()

		if usuario is None:
			print(usuario is None)
			return make_response({"msg":'Erro de autenticação', "success":False, "data":None})

		hash = usuario['senha']

		if bcrypt.checkpw(senha.encode(), hash.encode()):
			jwt = generate_token(usuario['id'], email, usuario['nome'], usuario['setor_id'])
			return make_response({"msg":'', "success":True, "data":jwt})
		else:
			print("senha errada")
			return make_response({"msg":'Erro de autenticação', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def decode_token(token):
	conn, cursor = db.ready()
	try:
		public_key = None
		payload = None
		valid = False
		with open('./keys/public.key', 'r') as pub:
			public_key = pub.read().encode()
		payload = jwt.decode(token, public_key, algorithms=['RS256'], verify=True)
		valid = True
		cursor.execute("select count(*) as qtd from usuario where id = %s", [payload['user_id']])
		result = cursor.fetchone()
		if result['qtd'] == 0:
			valid = False
			payload = None
	except:
		pass
	finally:
		cursor.close()
		conn.close()

	decoded_token = {"isValid":valid, "payload":payload}
	return decoded_token

def verify_token():
	token = None
	try:
		token = request.headers['Authorization']
	except KeyError:
		return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})

	return make_response({"msg":'', "success":True, "data":decode_token(token)})

def esqueci_senha():
	conn, cursor = db.ready()
	try:
		email = request.json['email']
		cursor.execute("select * from usuario where email = %s", [email])
		usuario = cursor.fetchone()
		if usuario is not None:
			string.printable[0:36]
			codigo = ''.join([random.choice(string.printable[0:36]) for a in range(20)])
			cursor.execute("update usuario set codigo_reset = %s where id = %s", [codigo, usuario['id']])
			corpo = TEMPLATES['RESET_SENHA'].format(nome = usuario['nome'], codigo = codigo)
			send_mail([usuario['email']], "Reset da senha GC", corpo)
			conn.commit()
			return make_response({"msg":'Email enviado para %s' % email, "success":True, "data":None})
		else:
			return make_response({"msg":'Email não cadastrado', "success":False, "data":None})
	except smtplib.SMTPAuthenticationError:
		return make_response({"msg":'Email não enviado, favor entrar em contato com o webadmin', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def resetar_senha():
	conn, cursor = db.ready()
	try:
		codigo = request.json['codigo']
		senha = request.json['senha']

		cursor.execute("select * from usuario where codigo_reset = %s", [codigo])
		usuario = cursor.fetchone()
		if usuario is not None:
			hash = bcrypt.hashpw(senha.encode(), bcrypt.gensalt())

			cursor.execute("update usuario set codigo_reset = null, senha = %s where id = %s", [hash, usuario['id']])
			conn.commit()
			jwt = generate_token(usuario['id'], usuario['email'], usuario['nome'], usuario['setor_id'])
			return make_response({"msg":'Senha resetada', "success":True, "data":jwt})
		else:
			return make_response({"msg":'Código inválido', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def requires(acao_requerida_id):  # use como decorator, acao_requerida_id eh o id da acao que o usuario deve ter permissao de realizar
	def decorator(func):
		@wraps(func)
		def decorated_func(*args, **kws):
			token = None
			try:
				token = request.headers['Authorization']
			except KeyError:
				return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})
			decoded = decode_token(token)
			if not decoded['isValid']:
				return make_response({"msg":'Ação não permitida', "success":False, "data":None})

			conn, cursor = db.ready()
			try:
				acoes_usuario = []
				cursor.execute("select * from perfis_usuario where usuario_id = %s", [decoded['payload']['user_id']])
				perfis = map(lambda p: p['perfil_id'], cursor.fetchall())
				for perfil in perfis:
					cursor.execute("select * from acoes_perfil where perfil_id = %s", [perfil])
					acoes = map(lambda a: a['acao_id'], cursor.fetchall())
					acoes_usuario.extend(acoes)
			finally:
				cursor.close()
				conn.close()

			if decoded['isValid'] and acao_requerida_id in acoes_usuario:
				return func(*args, **kws)
			else:
				return make_response({"msg":'Ação não permitida', "success":False, "data":None})
		return decorated_func
	return decorator
