from flask import Flask, make_response
from flask import request
from . import database as db

def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from perfil order by nome")
		perfis = cursor.fetchall()
		return make_response({"msg":None, "success":True, "data":perfis})
	finally:
		cursor.close()
		conn.close()
