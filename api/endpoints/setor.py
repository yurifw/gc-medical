from flask import Flask, make_response
from flask import request
from . import database as db

CHM = 1
COMERCIAL = 2
LOGISTICA = 3
INSTRUMENTACAO = 4
HOSPITAL = 5

def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from setor order by nome")
		setores = cursor.fetchall()
		return make_response({"msg":None, "success":True, "data":setores})
	finally:
		cursor.close()
		conn.close()

def select_by_id(id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from setor where id = %s order by name", [id])
		setor = cursor.fetchone()
		return make_response({"msg":None, "success":True, "data":setor})
	finally:
		cursor.close()
		conn.close()
