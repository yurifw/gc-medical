from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, LISTAR_GESTOR, ATUALIZAR_GESTOR, DELETAR_GESTOR, CADASTRAR_GESTOR
from .usuario import _cadastrar_usuario
from base64 import b64encode

@requires(CADASTRAR_GESTOR)
def insert():
	conn, cursor = db.ready()
	try:
		form = {}
		for key in request.form:
			form[key] = request.form[key]

		form['perfis'] = '[{"id":3,"nome":"Gestor Hospitalar"}]'
		usuario_id = _cadastrar_usuario(form, request.files, cursor)
		sql = "insert into gestor_hospitalar (hospital_id, usuario_id) values (%s, %s)"
		data = [request.form['hospital_id'], usuario_id]
		cursor.execute(sql, data)
		conn.commit()
		return make_response({"msg":"Gestor cadastrado com sucesso!", "success":True, "data":None})
	except Exception as e:
		msg = str(e)
		if "Duplicate" in msg and "email" in msg:
			msg = "Email já cadastrado"
		return make_response({"msg":msg, "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_GESTOR)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = """
			select
				u.id as user_id,
				u.nome as nome,
				u.email as email,
				s.nome as setor,
				s.id as setor_id,
				h.nome as hospital,
				h.id as hospital_id,
				g.id as gestor_id
			from
				usuario u,
				setor s,
				hospital h,
				gestor_hospitalar g
			where
				u.id = g.usuario_id and
				s.id = u.setor_id and
				h.id = g.hospital_id
			order by u.nome
			""".replace("\n"," ").replace("\t"," ")
		cursor.execute(sql)
		gestores = cursor.fetchall()
		for gestor in gestores:

			gestor['hospital'] = {'id':gestor['hospital_id'], 'nome':gestor['hospital']}
			del gestor['hospital_id']
			gestor['setor'] = {'id':gestor['setor_id'], 'nome':gestor['setor']}
			del gestor['setor_id']


		return make_response({"msg":None, "success":True, "data":gestores})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_GESTOR)
def atualizar(gestor_id):
	conn, cursor = db.ready()
	try:
		gestor = request.json
		print(gestor)
		cursor.execute("update gestor_hospitalar set hospital_id = %s where id = %s", [gestor['hospital_id'], gestor_id])
		cursor.execute("select usuario_id from gestor_hospitalar where id = %s",[gestor_id])
		user_id = cursor.fetchone()['usuario_id']
		cursor.execute("update usuario set nome =%s, email=%s, setor_id=%s where id = %s", [gestor['nome'], gestor['email'], gestor['setor_id'], user_id])
		conn.commit()
		return make_response({"msg":"Gestor atualizado com sucesso!", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_GESTOR)
def info(gestor_id):
	conn, cursor = db.ready()
	try:
		sql = """
			select
				h.nome as hospital,
				h.id as hospital_id,
				g.id as gestor_id,
				g.usuario_id as user_id,
				u.nome as nome,
				u.email as email,
				u.setor_id as setor_id
			from
				hospital h,
				gestor_hospitalar g,
				usuario u
			where
				h.id = g.hospital_id and
				u.id = g.usuario_id and
				g.id = %s
			""".replace("\n"," ").replace("\t"," ")
		cursor.execute(sql, [gestor_id])
		gestor = cursor.fetchone()
		gestor['hospital'] = {'id':gestor['hospital_id'], 'nome':gestor['hospital']}
		del gestor['hospital_id']
		return make_response({"msg":None, "success":True, "data":gestor})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_GESTOR)
def delete(gestor_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from gestor_hospitalar where id = %s", [gestor_id])
		gestor = cursor.fetchone()
		if gestor is None:
			return make_response({"msg":"Gestor inexistente", "success":False, "data":None})

		cursor.execute("delete from perfis_usuario where id = %s", [gestor['usuario_id']])
		cursor.execute("delete from gestor_hospitalar where id = %s", [gestor_id])
		cursor.execute("delete from usuario where id = %s", [gestor['usuario_id']])
		conn.commit()
		return make_response({"msg":"Gestor deletado com sucesso", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()
