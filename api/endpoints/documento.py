from flask import Flask, make_response
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import CADASTRAR_DOCUMENTO, DELETAR_DOCUMENTO, VISUALIZAR_DOCUMENTOS

@requires(CADASTRAR_DOCUMENTO)
def insert():
	conn, cursor = db.ready()
	try:
		usuario_id = decode_token(request.headers['Authorization'])['payload']['user_id']
		blob = request.files['documento'].read()
		nome_arquivo = request.files['documento'].filename
		sql = "insert into documentos (documento, descricao, nome_arquivo, nome_exibicao, cirurgia_id, uploader_id, data_upload) values (%s, %s, %s, %s, %s, %s, now())"
		data = [blob, request.form['descricao'], nome_arquivo, request.form['nome_exibicao'], request.form['cirurgia_id'], usuario_id]
		cursor.execute(sql, data)
		conn.commit()
		return make_response({"msg":"Documento cadastrado com sucesso!", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_DOCUMENTO)
def delete(documento_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("delete from documentos where id = %s", [documento_id])
		conn.commit()
		return make_response({"msg":"Documento deletado com sucesso!", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()


@requires(VISUALIZAR_DOCUMENTOS)
def select_cirurgia(cirurgia_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select id, descricao, nome_exibicao, nome_arquivo, DATE_FORMAT(data_upload,'%%d/%%m/%%Y %%H:%%i:%%s') as data_upload, uploader_id from documentos where cirurgia_id = %s", [cirurgia_id])
		documentos = cursor.fetchall()
		return make_response({"msg":None, "success":True, "data":documentos})
	finally:
		cursor.close()
		conn.close()

@requires(VISUALIZAR_DOCUMENTOS)
def download(documento_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select documento, nome_arquivo from documentos where id = %s", [documento_id])
		documentos = cursor.fetchone()
		response = make_response(documentos['documento'])
		response.headers.set('Content-Type', 'application/x-binary')
		response.headers.set('Content-Disposition', 'attachment', filename=documentos['nome_arquivo'])
		response.headers.set('Access-Control-Expose-Headers', 'Content-Disposition')
		return response
	finally:
		cursor.close()
		conn.close()
