from flask import Flask, make_response
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import CADASTRAR_PERMISSOES, ATUALIZAR_PERMISSOES, DELETAR_PERMISSOES, LISTAR_PERMISSOES

@requires(LISTAR_PERMISSOES)
def select_all():
	conn, cursor = db.ready()
	try:
		result = []
		cursor.execute("select * from perfil")
		perfis = cursor.fetchall()
		for p in perfis:
			perfil = {"id":p['id'],"nome":p['nome'], "permissoes":[]}
			cursor.execute("select a.nome, a.id from acao a, acoes_perfil ap where ap.perfil_id = %s and a.id = ap.acao_id", [p['id']])
			perfil['permissoes'] = cursor.fetchall()
			result.append(perfil)
		return make_response({"msg":None, "success":True, "data":result})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_PERMISSOES)
def select_id(perfil_id):
	conn, cursor = db.ready()
	try:
		result = []
		cursor.execute("select * from perfil where id = %s", [perfil_id])
		perfil = cursor.fetchone()
		cursor.execute("select a.nome, a.id from acao a, acoes_perfil ap where ap.perfil_id = %s and a.id = ap.acao_id", [perfil['id']])
		perfil['permissoes'] = cursor.fetchall()
		return make_response({"msg":None, "success":True, "data":perfil})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_PERMISSOES)
def acoes_disponiveis():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from acao")
		acoes = cursor.fetchall()
		return make_response({"msg":None, "success":True, "data":acoes})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_PERMISSOES)
def update(perfil_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("update perfil set nome = %s where id = %s", [request.json['nome'], perfil_id])
		cursor.execute("delete from acoes_perfil where perfil_id = %s", [perfil_id])
		for p in request.json['permissoes']:
			cursor.execute("insert into acoes_perfil (perfil_id, acao_id) values (%s,%s)",[perfil_id, p['id']])
		conn.commit()
		return make_response({"msg":"Perfil atualizado com sucesso", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(CADASTRAR_PERMISSOES)
def insert():
	conn, cursor = db.ready()
	try:
		cursor.execute("insert into perfil(nome) values (%s)", [request.json['nome']])
		perfil_id = cursor.lastrowid

		for p in request.json['permissoes']:
			cursor.execute("insert into acoes_perfil (perfil_id, acao_id) values (%s,%s)", [perfil_id, p['id']])

		conn.commit()
		return make_response({"msg":"Perfil cadastrado com sucesso", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_PERMISSOES)
def delete(perfil_id):
	conn, cursor = db.ready()
	try:
		nao_deletaveis = [1,2,3]
		if perfil_id in nao_deletaveis:
			return make_response({"msg":"Este perfil não pode ser deletado", "success":False, "data":None})

		cursor.execute("delete from acoes_perfil where perfil_id = %s", [perfil_id])
		cursor.execute("delete from perfil where id = %s", [perfil_id])
		conn.commit()
		return make_response({"msg":"Perfil deletado com sucesso", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()
