from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db

def search_caixa_item(pesquisa):
	conn, cursor = db.ready()
	try:
		pesquisa = "%"+pesquisa+"%"

		hospital_id = request.args.get('hospital')
		query_params = [pesquisa, pesquisa]

		hospital_query = ""
		if hospital_id is not None:
			hospital_query = " and hospital_id = %s"
			query_params.append(hospital_id)


		sql = """
		select * from caixa where id in (
			select
			cc.caixa_id
			from conteudo_caixa cc, material m, caixa c
			where
			cc.material_id = m.id and
			c.id = cc.caixa_id and
			(m.nome like %s or c.nome like %s)
		){hospital_query};
		""".format(hospital_query = hospital_query)

		cursor.execute(sql, query_params)
		caixas = cursor.fetchall()
		return make_response({"msg":None,"data":caixas, "success":True})
	finally:
		cursor.close()
		conn.close()

def search_caixa_id(caixa_id):
	conn, cursor = db.ready()
	try:
		sql = "select id, nome, proprietario_id from caixa where id =%s"
		cursor.execute(sql, [caixa_id])
		caixa = cursor.fetchone()
		sql = """
			select
				c.id as conteudo_caixa_id,
				c.quantidade as quantidade,
				c.caixa_id,
				m.id as material_id,
				m.nome as nome,
				m.valor,
				m.codigo
			from
				conteudo_caixa c,
				material m
			where
				caixa_id = %s and
				c.material_id = m.id
		"""
		cursor.execute(sql, [caixa_id])
		caixa['materiais'] = []
		materiais = cursor.fetchall()
		for material in materiais:
			material['valor'] = float(material['valor'])
			caixa['materiais'].append(material)
		return make_response({"msg":None,"data":caixa, "success":True})
	finally:
		cursor.close()
		conn.close()

def list_proprietarios():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from proprietario_caixa")
		proprietarios = cursor.fetchall()
		return make_response({"msg":None,"data":proprietarios, "success":True})
	finally:
		cursor.close()
		conn.close()
