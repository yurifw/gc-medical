from flask import Flask, make_response
from flask import request
import bcrypt
from . import database as db
from .auth import requires, decode_token
from .auth import CADASTRAR_USUARIO, LISTAR_USUARIOS, ATUALIZAR_USUARIOS, DELETAR_USUARIOS
import json
from base64 import b64encode

def _cadastrar_usuario(usuario_form, request_files, cursor, verificar_senha = True):
	usuario = {}
	for key in usuario_form:
		usuario[key] = usuario_form[key]
	mime = ""
	if len(request_files) < 1:
		with open("./public/default_foto.png","rb") as img:
			usuario['foto'] = img.read()
			mime = "image/png"
	else:
		if 'image' not in request_files['foto'].content_type:
			raise AttributeError("Arquivo inválido")
		usuario['foto'] = request_files['foto'].read()
		mime = request_files['foto'].content_type

	usuario['perfis'] = json.loads(usuario_form['perfis'])

	password = usuario['senha']
	hashed = bcrypt.hashpw(password.encode(), bcrypt.gensalt())

	if len(usuario['email']) < 3:
		raise AttributeError("Email inválido")

	if len(usuario['nome']) < 3:
		raise AttributeError("Nome muito curto")

	if len(usuario['perfis']) < 1:
		raise AttributeError("Selecione ao menos um perfil")

	if len(usuario['senha']) < 6:
		raise AttributeError("Senha muito curta")

	if usuario['setor_id'] is None:
		raise AttributeError("Selecione um setor")

	sql = "insert into usuario (email, senha, nome, setor_id, foto, foto_mime) VALUES (%s, %s, %s, %s, %s, %s)";
	cursor.execute(sql, [usuario['email'], hashed, usuario['nome'], usuario['setor_id'], usuario['foto'], mime])
	usuario_id = cursor.lastrowid

	for perfil in usuario['perfis']:
		sql = "insert into perfis_usuario (perfil_id, usuario_id) values (%s, %s)"
		cursor.execute(sql, [perfil['id'], usuario_id])
	return usuario_id

def _atualizar_usuario(usuario_id, usuario_form, request_files, cursor, request_pwd = True):
	sql = "select * from usuario where id = %s"
	cursor.execute(sql, [usuario_id])
	usuario = cursor.fetchone()
	if request_pwd:
		if "senha" not in usuario_form:
			raise AttributeError('Senha inválida')

		if not bcrypt.checkpw(usuario_form["senha"].encode(), usuario['senha'].encode()):
			raise AttributeError('Senha inválida')

	if "foto" in request_files:
		nova_foto = request_files['foto'].read()
		mime = request_files['foto'].content_type
		cursor.execute("update usuario set foto = %s, foto_mime = %s where id = %s", [nova_foto, mime, usuario['id']])

	if "nome" in usuario_form and usuario_form["nome"] is not None and len(usuario_form["nome"])>0:
		cursor.execute("update usuario set nome = %s where id = %s", [usuario_form["nome"], usuario['id']])

	if "email" in usuario_form and "@" in usuario_form['email']:
		cursor.execute("update usuario set email = %s where id = %s", [usuario_form["email"], usuario['id']])

	if "setor_id" in usuario_form:
		cursor.execute("update usuario set setor_id = %s where id = %s", [usuario_form["setor_id"], usuario['id']])

	usuario['perfis'] = json.loads(usuario_form['perfis']) if 'perfis' in usuario_form else []
	for perfil in usuario['perfis']:
		cursor.execute("delete from perfis_usuario where usuario_id = %s", [usuario_id])
		sql = "insert into perfis_usuario (perfil_id, usuario_id) values (%s, %s)"
		cursor.execute(sql, [perfil['id'], usuario_id])

	if "novaSenha" in usuario_form and usuario_form["novaSenha"] is not None and len(usuario_form["novaSenha"])>0:
		if len(usuario_form["novaSenha"]) < 6:
			raise AttributeError('A nova senha é muito curta')

		hashed = bcrypt.hashpw(usuario_form["novaSenha"].encode(), bcrypt.gensalt())
		cursor.execute("update usuario set senha = %s where id = %s", [hashed, usuario['id']])

def _listar_permissoes(usuario_id, cursor):
	sql = """
		select ap.acao_id, a.nome
		from acao a, acoes_perfil ap, perfis_usuario pu
		where ap.perfil_id = pu.perfil_id and pu.usuario_id = %s and a.id = ap.acao_id;
	"""
	cursor.execute(sql, [usuario_id])
	acoes = cursor.fetchall()
	return acoes

def _load_usuario(usuario, cursor):
	cursor.execute("select * from setor where id = %s", [usuario['setor_id']])
	usuario['setor'] = cursor.fetchone()
	del usuario['codigo_reset']
	del usuario['senha']
	del usuario['setor_id']
	usuario['permissoes'] = _listar_permissoes(usuario['id'], cursor)
	if usuario['foto'] is None or len(usuario['foto']) < 2:
		with open("./public/default_foto.png","rb") as img:
			usuario['foto'] = b64encode(img.read())
		usuario['foto_mime'] = "image/png"
	else:
		usuario['foto'] = b64encode(usuario['foto'])
	usuario['foto'] = usuario['foto'].decode("utf-8")

	cursor.execute("select * from perfil where id in (select perfil_id from perfis_usuario where usuario_id = %s)",[usuario['id']])
	usuario['perfis'] = cursor.fetchall()
	return usuario

@requires(CADASTRAR_USUARIO)
def insert():
	conn, cursor = db.ready()
	try:

		usuario_id = _cadastrar_usuario(request.form, request.files, cursor)
		prefis = json.loads(request.form['perfis'])
		conn.commit()

		return make_response({"msg":"Usuário cadastrado com sucesso!", "success":True, "data":None})
	except Exception as e:
		msg = str(e)
		if "Duplicate" in msg and "email" in msg:
			msg = "Email já cadastrado"
		return make_response({"msg":msg, "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

def info():
	token = None
	conn, cursor = db.ready()
	try:
		token = request.headers['Authorization']
		decoded = decode_token(token)
		usuario_id = decoded['payload']['user_id']
		sql = "select * from usuario where id = %s"
		cursor.execute(sql, [decoded['payload']['user_id']])
		usuario = _load_usuario(cursor.fetchone(), cursor)

		cursor.execute("select * from medico where usuario_id = %s", [usuario_id])
		m = cursor.fetchone()
		usuario['medico_id'] = m['id'] if m is not None else None

		return make_response({"msg":None, "success":True, "data":usuario})
	except KeyError:
		return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_USUARIOS)
def update(usuario_id):  # usuario alterando dados de outros usuarios
	conn, cursor = db.ready()
	try:
		_atualizar_usuario(usuario_id, request.form, request.files, cursor, False)
		conn.commit()
		return make_response({"msg":'Dados atualizados com sucesso', "success":True, "data":None})
	except KeyError:
		return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})
	except AttributeError as e:
		return make_response({"msg":str(e), "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_USUARIOS)
def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from usuario where id not in (select usuario_id from medico) and id not in (select usuario_id from gestor_hospitalar)")
		usuarios = cursor.fetchall()
		for usuario in usuarios:
			usuario = _load_usuario(usuario, cursor)
		return make_response({"msg":None, "success":True, "data":usuarios})

	except AttributeError as e:
		return make_response({"msg":str(e), "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_USUARIOS)
def select_id(usuario_id):
	conn, cursor = db.ready()
	try:
		sql = "select * from usuario where id = %s"
		cursor.execute(sql, [usuario_id])
		usuario = _load_usuario(cursor.fetchone(), cursor)

		return make_response({"msg":None, "success":True, "data":usuario})
	except KeyError:
		return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_USUARIOS)
def deletar(usuario_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from usuario where id = %s", [usuario_id])
		usuario = cursor.fetchone()
		if usuario is None:
			return make_response({"msg":"Usuário inexistente", "success":True, "data":None})

		cursor.execute("delete from perfis_usuario where usuario_id = %s", [usuario['id']])
		cursor.execute("delete from usuario where id = %s", [usuario_id])
		conn.commit()
		return make_response({"msg":"Usuário deletado com sucesso", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

def atualizar(): # usuario alterando seus proprios dados
	token = None
	conn, cursor = db.ready()
	try:
		token = request.headers['Authorization']
		decoded = decode_token(token)
		usuario_id = decoded['payload']['user_id']
		_atualizar_usuario(usuario_id, request.form, request.files, cursor)

		conn.commit()
		return make_response({"msg":'Dados atualizados com sucesso', "success":True, "data":None})
	except KeyError:
		return make_response({"msg":'Envie a chave no header "Authorization"', "success":False, "data":None})
	except AttributeError as e:
		return make_response({"msg":str(e), "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()
