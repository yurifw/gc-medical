from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token
from .auth import SOLICITAR_MATERIAL, LISTAR_CIRURGIAS_PROPRIAS, ATUALIZAR_CIRURGIA, LISTAR_CIRURGIAS
from .setor import CHM, HOSPITAL
from .pacientes import _load_paciente_id
import endpoints.encaminhamento

def _load_cirurgia_id(cirurgia_id, cursor):
	cursor.execute("select * from cirurgia where id = %s", [cirurgia_id])
	cirurgia = cursor.fetchone()
	return _load_cirurgia(cirurgia, cursor)

def _load_cirurgia(cirurgia, cursor):

	cirurgia['paciente'] = _load_paciente_id(cirurgia['paciente_id'], cursor)
	del cirurgia['paciente_id']

	sql = """select
		m.nome,
		m.id as material_id,
		m.valor,
		m.codigo,
		s.solicitado,
		s.consumido,
		c.id as caixa_id,
		c.proprietario_id,
		s.id as solicitacao_id,
		cc.id as conteudo_caixa_id
	from
		material m,
		conteudo_caixa cc,
		solicitacao s,
		caixa c
	where
		m.id = cc.material_id and
		cc.id = s.conteudo_caixa_id and
		cc.caixa_id = c.id and
		s.cirurgia_id = %s"""
	cursor.execute(sql, [cirurgia['id']])
	solicitacoes = cursor.fetchall()

	cirurgia['solicitacoes'] = solicitacoes

	sql = "select m.crm, h.nome as hospital, m.id, m.usuario_id as usuario_id from medico m, hospital h where m.usuario_id = %s and h.id = m.hospital_id"
	cursor.execute(sql, [cirurgia['medico']])
	cirurgia['medico'] = cursor.fetchone()

	cirurgia['data'] = cirurgia['data'].strftime("%d/%m/%Y %H:%M")

	cursor.execute("select * from tipo_cirurgia where id = %s", [cirurgia['tipo_cirurgia_id']])
	cirurgia['tipo'] = cursor.fetchone()
	del cirurgia['tipo_cirurgia_id']
	return cirurgia

@requires(SOLICITAR_MATERIAL)
def insert():
	"""
	cirurgia = {
		"tipo": {"id":1, "nome":"urgência"},
		"data": "20/08/2020 19:55",
		"procedimento": "Transfusão de sangue",
		"cid": "CID Teste",
		"paciente": {
			"nome":"Teste",
			"nascimento": "21/05/1996",
			"numero":123,
			"sexo": "F",
			"cidade_residencia": "cidade"
		},
		"materiais":[
			{"conteudo_caixa_id": 1, "solicitado": 2, "nome": "Sangue A+ 500ml"},
			{"conteudo_caixa_id": 2, "solicitado": 1, "nome": "Sangue A- 500ml"},
			{"conteudo_caixa_id": 4, "solicitado": 1, "nome": "Sangue O+ 500ml"}
		],
		"medico":1
	}
	"""

	conn, cursor = db.ready()
	try:
		cirurgia = request.json
		paciente = cirurgia['paciente']
		print(cirurgia)
		if 'observacao' not in cirurgia:
			cirurgia['observacao'] = ''
			
		medico = request.json['medico']
		if medico is None:
			return make_response({"msg":"Escolha o médico responsável","data":None, "success":False})

		cursor.execute("select * from paciente where numero = %s", [paciente['numero']])
		paciente = cursor.fetchone()
		if paciente is None:
			paciente = cirurgia['paciente']
			sql = "insert into paciente (nome, nascimento, numero, sexo, cidade_residencia) values (%s, STR_TO_DATE(%s, '%%d/%%m/%%Y'), %s, %s, %s);"
			cursor.execute(sql, [paciente['nome'], paciente['nascimento'], paciente['numero'], paciente['sexo'], paciente['cidade_residencia']])
			paciente_id = cursor.lastrowid
			paciente['id'] = paciente_id
			cirurgia['paciente'] = paciente

		sql = "insert into cirurgia(paciente_id, data, tipo_cirurgia_id, procedimento, cid, medico) values (%s, STR_TO_DATE(%s, '%%d/%%m/%%Y %%H:%%i'), %s, %s, %s, %s)"
		cursor.execute(sql, [paciente['id'], cirurgia['data'], cirurgia['tipo']['id'], cirurgia['procedimento'], cirurgia['cid'], medico])
		cirurgia['id'] = cursor.lastrowid

		for material in cirurgia['materiais']:
			cursor.execute("select * from conteudo_caixa where id = %s", [material['conteudo_caixa_id']])
			result = cursor.fetchone()
			if material['solicitado'] > result['quantidade']:
				return make_response({"msg":"Máximo de %s excedido para %s" % (result['quantidade'],material['nome']),"data":None, "success":False})

			sql = "insert into solicitacao (cirurgia_id, conteudo_caixa_id, solicitado, consumido, usuario_solicitante_id) values (%s, %s, %s, %s, %s)"
			cursor.execute(sql, [cirurgia['id'], material['conteudo_caixa_id'], material['solicitado'], material['consumido'], medico])

		endpoints.encaminhamento._encaminhar_cirurgia(cirurgia['id'], endpoints.encaminhamento.STATUS_ENCAMINHADO_PARA_CHM, cirurgia['observacao'], medico, cursor)

		conn.commit()
		return make_response({"msg":"Cirurgia cadastrada com sucesso!","data":cirurgia['id'], "success":True})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_CIRURGIAS_PROPRIAS)
def list_by_user():
	usuario_id = decode_token(request.headers['Authorization'])['payload']['user_id']
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from cirurgia where medico = %s", [usuario_id])
		cirurgias = cursor.fetchall()
		for cirurgia in cirurgias:
			cirurgia = _load_cirurgia(cirurgia, cursor)


		return make_response({"msg":None,"data":cirurgias, "success":True})
	finally:
		conn.close()
		cursor.close()

@requires(LISTAR_CIRURGIAS)
def list_by_id(cirurgia_id):
	conn, cursor = db.ready()
	try:
		return make_response({"msg":None,"data":_load_cirurgia_id(cirurgia_id, cursor), "success":True})
	except:
		return make_response({"msg":"Cirurgia não encontrada","data":None, "success":False})
	finally:
		conn.close()
		cursor.close()

@requires(ATUALIZAR_CIRURGIA)
def update(cirurgia_id):
	conn, cursor = db.ready()
	try:
		d = decode_token(request.headers['Authorization'])['payload']
		setor_id = d['setor_id']
		usuario_id = d['user_id']
		cirurgia = request.json
		if 'observacao' not in cirurgia:
			cirurgia['observacao'] = ''

		sql = "select * from encaminhamento where cirurgia_id = %s order by data_encaminhamento desc limit 1"
		cursor.execute(sql, [cirurgia_id])
		c = cursor.fetchone()
		if c is None:
			return make_response({"msg":"Cirurgia não encontrada","data":None, "success":False})
		status = c['status_encaminhamento_id']
		if status != endpoints.encaminhamento.STATUS_REPROVADO_PELA_CHM and setor_id == HOSPITAL:
			return make_response({"msg":"Você não pode editar esta cirurgia, espere o CHM reprová-la!","data":None, "success":False})

		cursor.execute("select * from cirurgia where id = %s", [cirurgia_id])
		cirurgia_cadastrada = cursor.fetchone()

		cursor.execute("select * from paciente where id = %s", [cirurgia_cadastrada['paciente_id']])
		paciente_cadastrado = cursor.fetchone()
		paciente = cirurgia['paciente']
		if paciente_cadastrado['numero'] != paciente['numero']:
			cursor.execute("select * from paciente where numero = %s", [paciente['numero']])
			paciente = cursor.fetchone()
			if paciente is None:
				paciente = cirurgia['paciente']
				sql = "insert into paciente (nome, nascimento, numero) values (%s, STR_TO_DATE(%s, '%%d/%%m/%%Y'), %s);"
				cursor.execute(sql, [paciente['nome'], paciente['nascimento'], paciente['numero']])
				paciente_id = cursor.lastrowid
				paciente['id'] = paciente_id
				cursor.execute("update cirurgia set paciente_id = %s where id =%s", [paciente_id, cirurgia_id])
			else:
				sql = "update paciente set nome=%s, nascimento=STR_TO_DATE(%s, '%%d/%%m/%%Y'), sexo=%s, cidade_residencia=%s where numero=%s"
				cursor.execute(sql, [paciente['nome'], paciente['nascimento'], paciente['sexo'], paciente['cidade_residencia'], paciente['numero']])
		else:
			sql = "update paciente set nome=%s, nascimento=STR_TO_DATE(%s, '%%d/%%m/%%Y'), sexo=%s, cidade_residencia=%s where numero=%s"
			cursor.execute(sql, [paciente['nome'], paciente['nascimento'], paciente['sexo'], paciente['cidade_residencia'], paciente['numero']])

		sql = "update cirurgia set data = STR_TO_DATE(%s, '%%d/%%m/%%Y %%H:%%i'), tipo_cirurgia_id = %s, procedimento =%s, cid =%s, medico=%s, pagamento_realizado=%s where id = %s"
		cursor.execute(sql, [cirurgia['data'], cirurgia['tipo']['id'], cirurgia['procedimento'], cirurgia['cid'], cirurgia['medico']['usuario_id'], cirurgia['pagamento_realizado'], cirurgia_id])

		cursor.execute("delete from solicitacao where cirurgia_id = %s", [cirurgia_id])
		for solicitacao in cirurgia['solicitacoes']:
			cursor.execute("select * from conteudo_caixa where id = %s", [solicitacao['conteudo_caixa_id']])
			c = cursor.fetchone()
			if (c['quantidade'] < solicitacao['solicitado']):
				return make_response({"msg":"Quantidade excedida para o item %s" % solicitacao['nome'],"data":None, "success":False})
			sql = "insert into solicitacao (cirurgia_id, conteudo_caixa_id, solicitado, consumido, usuario_solicitante_id) values (%s, %s, %s, %s, %s)"
			cursor.execute(sql, [cirurgia_id, solicitacao['conteudo_caixa_id'], solicitacao['solicitado'],solicitacao['consumido'], usuario_id])

		if status == endpoints.encaminhamento.STATUS_REPROVADO_PELA_CHM and setor_id == HOSPITAL:
			endpoints.encaminhamento._encaminhar_cirurgia(cirurgia_id, endpoints.encaminhamento.STATUS_ENCAMINHADO_PARA_CHM, cirurgia['observacao'], usuario_id, cursor)

		data_antiga = cirurgia_cadastrada['data'].strftime("%d/%m/%Y %H:%M")
		data_nova = cirurgia['data']
		if data_nova != data_antiga:
			pass
		conn.commit()

		return make_response({"msg":"Cirurgia atualizada com sucesso!","data":cirurgia_id, "success":True})
	finally:
		cursor.close()
		conn.close()
