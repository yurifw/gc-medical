from flask import Flask, make_response
from flask import request
from . import database as db
from . import database as db
from .auth import requires, LISTAR_HOSPITAIS, ATUALIZAR_HOSPITAL, DELETAR_HOSPITAL, CADASTRAR_HOSPITAL
from pymysql.err import IntegrityError

@requires(LISTAR_HOSPITAIS)
def select_all():
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from hospital order by nome")
		hospitais = cursor.fetchall()
		return make_response({"msg":None, "success":True, "data":hospitais})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_HOSPITAIS)
def select_by_id(hospital_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from hospital where id = %s", [hospital_id])
		hospital = cursor.fetchone()
		return make_response({"msg":None, "success":True, "data":hospital})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_HOSPITAL)
def delete(hospital_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("delete from hospital where id = %s", [hospital_id])
		conn.commit()
		return make_response({"msg":"Hospital deletado", "success":True, "data":None})
	except IntegrityError:
		return make_response({"msg":"Erro de integridade: Hospital vinculado a outros registros no banco de dados", "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(CADASTRAR_HOSPITAL)
def insert():
	conn, cursor = db.ready()
	try:
		hospital = request.json
		cursor.execute("insert into hospital (nome, id_auvo) values (%s, %s)", [hospital['nome'], hospital['id_auvo']])
		conn.commit()
		return make_response({"msg":"Hospital cadastrado", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_HOSPITAL)
def update(hospital_id):
	conn, cursor = db.ready()
	try:
		hospital = request.json
		cursor.execute("update hospital set nome = %s, id_auvo = %s where id =%s", [hospital['nome'], hospital['id_auvo'], hospital_id])
		conn.commit()
		return make_response({"msg":"Hospital atualizado", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()
