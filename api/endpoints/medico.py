from flask import Flask, make_response
from flask import current_app
from flask import request
from . import database as db
from .auth import requires, decode_token, LISTAR_MEDICOS, ATUALIZAR_MEDICO, DELETAR_MEDICO, CADASTRAR_MEDICO
from .usuario import _cadastrar_usuario
from base64 import b64encode

@requires(CADASTRAR_MEDICO)
def insert():
	conn, cursor = db.ready()
	try:
		form = {}
		for key in request.form:
			form[key] = request.form[key]

		form['perfis'] = '[{"id":2,"nome":"Médico"}]'
		usuario_id = _cadastrar_usuario(form, request.files, cursor)
		sql = "insert into medico (crm, hospital_id, usuario_id) values (%s, %s, %s)"
		data = [request.form['crm'], request.form['hospital_id'], usuario_id]
		cursor.execute(sql, data)
		conn.commit()
		return make_response({"msg":"Médico cadastrado com sucesso!", "success":True, "data":None})
	except Exception as e:
		msg = str(e)
		if "Duplicate" in msg and "email" in msg:
			msg = "Email já cadastrado"
		return make_response({"msg":msg, "success":False, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_MEDICOS)
def select_all():
	conn, cursor = db.ready()
	try:
		sql = """
			select
				u.id as user_id,
				u.nome as nome,
				u.email as email,
				s.nome as setor,
				s.id as setor_id,
				h.nome as hospital,
				h.id as hospital_id,
				m.id as medico_id,
				m.crm as crm
			from
				usuario u,
				setor s,
				hospital h,
				medico m
			where
				u.id = m.usuario_id and
				s.id = u.setor_id and
				h.id = m.hospital_id
			order by u.nome
			""".replace("\n"," ").replace("\t"," ")
		cursor.execute(sql)
		medicos = cursor.fetchall()
		for medico in medicos:

			medico['hospital'] = {'id':medico['hospital_id'], 'nome':medico['hospital']}
			del medico['hospital_id']
			medico['setor'] = {'id':medico['setor_id'], 'nome':medico['setor']}
			del medico['setor_id']


		return make_response({"msg":None, "success":True, "data":medicos})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_MEDICOS)
def list_usuarios_by_hospital():
	conn, cursor = db.ready()
	try:
		usuario_id = decode_token(request.headers['Authorization'])['payload']['user_id']
		cursor.execute("select hospital_id from medico where usuario_id = %s", [usuario_id])
		hospital_id = cursor.fetchone()['hospital_id']
		sql = """
			select
				u.id as user_id,
				u.nome as nome,
				u.email as email,
				s.nome as setor,
				s.id as setor_id,
				h.nome as hospital,
				h.id as hospital_id,
				m.id as medico_id,
				m.crm as crm
			from
				usuario u,
				setor s,
				hospital h,
				medico m
			where
				u.id = m.usuario_id and
				s.id = u.setor_id and
				h.id = %s and
				m.hospital_id = %s
			order by u.nome
			""".replace("\n"," ").replace("\t"," ")
		cursor.execute(sql, [hospital_id, hospital_id])
		medicos = cursor.fetchall()
		for medico in medicos:

			medico['hospital'] = {'id':medico['hospital_id'], 'nome':medico['hospital']}
			del medico['hospital_id']
			medico['setor'] = {'id':medico['setor_id'], 'nome':medico['setor']}
			del medico['setor_id']


		return make_response({"msg":None, "success":True, "data":medicos})
	finally:
		cursor.close()
		conn.close()

@requires(ATUALIZAR_MEDICO)
def atualizar(medico_id):
	conn, cursor = db.ready()
	try:
		medico = request.json
		cursor.execute("update medico set crm = %s, hospital_id = %s where id = %s", [medico['crm'], medico['hospital_id'], medico_id])
		cursor.execute("select usuario_id from medico where id = %s",[medico_id])
		user_id = cursor.fetchone()['usuario_id']
		cursor.execute("update usuario set nome =%s, email=%s, setor_id=%s where id = %s", [medico['nome'], medico['email'], medico['setor_id'], user_id])
		conn.commit()
		return make_response({"msg":"Médico atualizado com sucesso!", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()

@requires(LISTAR_MEDICOS)
def info(medico_id):
	conn, cursor = db.ready()
	try:
		sql = """
			select
				h.nome as hospital,
				h.id as hospital_id,
				m.id as medico_id,
				m.crm as crm,
				m.usuario_id as user_id,
				u.nome as nome,
				u.email as email,
				u.id as user_id,
				u.setor_id as setor_id
			from
				hospital h,
				medico m,
				usuario u
			where
				h.id = m.hospital_id and
				u.id = m.usuario_id and
				m.id = %s
			""".replace("\n"," ").replace("\t"," ")
		cursor.execute(sql, [medico_id])
		medico = cursor.fetchone()
		medico['hospital'] = {'id':medico['hospital_id'], 'nome':medico['hospital']}
		del medico['hospital_id']
		return make_response({"msg":None, "success":True, "data":medico})
	finally:
		cursor.close()
		conn.close()

@requires(DELETAR_MEDICO)
def delete(medico_id):
	conn, cursor = db.ready()
	try:
		cursor.execute("select * from medico where id = %s", [medico_id])
		medico = cursor.fetchone()
		if medico is None:
			return make_response({"msg":"Médico inexistente", "success":True, "data":None})

		cursor.execute("delete from perfis_usuario where id = %s", [medico['usuario_id']])
		cursor.execute("delete from medico where id = %s", [medico_id])
		cursor.execute("delete from usuario where id = %s", [medico['usuario_id']])
		conn.commit()
		return make_response({"msg":"Médico deletado com sucesso", "success":True, "data":None})
	finally:
		cursor.close()
		conn.close()
