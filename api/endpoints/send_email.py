import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


TEMPLATES= {
	"RESET_SENHA": """
		<html>
		<body style="">

			<table style="width:550px;background-image: linear-gradient(to top right, #76c5bf, #b2dac2);background-repeat: no-repeat;background-tachment:fixed;" align="center">
				<tr>
					<td>
						<div style="margin:auto;width:200px;margin-top:30px;margin-bottom:30px;">
							<img src="http://yuriwaki.com:4200/assets/Logo-Branco.png" style="width:100%;">
						</div>
					</td>
				</tr>
				<tr>
					<td style="width:80%; margin:auto;background-color:#ffffff; padding:15px;line-height: 2em;">
						Olá {nome},<br>
						Você está recebendo este email pois solicitou um reset de sua senha. Para prosseguir, acesse o link
						<a href = "http://107.152.38.248/resetar-senha/{codigo}">http://107.152.38.248/resetar-senha/{codigo}</a>
					</td>
				</tr>
			</table>
		</body>
		</html>
		""",
	"NOVO_STATUS":"""
		<html>
		<body style="">

			<table style="width:550px;background-image: linear-gradient(to top right, #76c5bf, #b2dac2);background-repeat: no-repeat;background-tachment:fixed;" align="center">
				<tr>
					<td>
						<div style="margin:auto;width:200px;margin-top:30px;margin-bottom:30px;">
							<img src="http://yuriwaki.com:4200/assets/Logo-Branco.png" style="width:100%;">
						</div>
					</td>
				</tr>
				<tr>
					<td style="width:80%; margin:auto;background-color:#ffffff; padding:15px;line-height: 2em;">
						Olá,<br>
						Você está recebendo este email pois o procedimento cirúrgico {procedimento} marcado para {data}
						foi alterado por {usuario} no dia {data_alteracao} e agora possui um novo status ({status}) que requer uma ação sua.
					</td>
				</tr>
			</table>
		</body>
		</html>
	"""
}


def send_mail(para, assunto, corpo, anexos=None):
	"""
		para: list - list com enderecos de email que serao os destinatarios
		assunto: str - aparecera como assunto
		corpo: str - texto que sera o corpo do email
		anexos: list - lista de caminhos de arquivos para serem anexados no email
	"""
	# CONFIG #
	email_server = "gcmedical.com.br"
	email_port = "465"
	email_user = "naoresponda@gcmedical.com.br"
	email_pwd = "cW0.rC4!w"
	####################################

	msg = MIMEMultipart()
	msg['From'] = email_user
	msg['To'] = ','.join(para)
	msg['Subject'] = assunto

	msg.attach(MIMEText(corpo, 'html'))
	for f in anexos or []:
		with open(f, "rb") as fil:
			part = MIMEApplication(
				fil.read(),
				Name=basename(f)
			)
		# After the file is closed
		part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
		msg.attach(part)
	smtp = smtplib.SMTP_SSL(email_server, email_port)
	# smtp.set_debuglevel(1)
	smtp.login(email_user, email_pwd)
	try:
		smtp.sendmail(email_user, para, msg.as_string())
		for address in para:
			smtp.sendmail(email_user, address, msg.as_string())
	except smtplib.SMTPRecipientsRefused:
		print("SMTPRecipientsRefused")
		pass
	finally:
		smtp.quit()


# send_mail(["yuridefw2@gmail.com"], "teste", "teste")
