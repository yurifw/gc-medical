import unittest
import requests
import json
from random import randrange
from requests.packages.urllib3.exceptions import InsecureRequestWarning


SERVER = "https://yuriwaki.com/%s"
USUARIO_TESTE = "teste@teste.com"
SENHA_TESTE = "123456"

STATUS_FINALIZADO_ID = 1
STATUS_ENCAMINHADO_PARA_CHM = 2
STATUS_APROVADO_PELA_CHM = 3
STATUS_REPROVADO_PELA_CHM = 4
STATUS_COMERCIAL_PARA_LOGISTICA = 5
STATUS_COMERCIAL_PARA_INSTRUMENTACAO = 6
STATUS_REPROVADO_PELO_COMERCIAL = 7
STATUS_FATURAMENTO_SOLICITADO = 8
STATUS_FATURAMENTO_APROVADO = 9

class TestStatusEncaminhamentos(unittest.TestCase):

	@classmethod
	def setUpClass(self):
		requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

		response = requests.post(SERVER % "api/login", json={"email":USUARIO_TESTE, "senha":SENHA_TESTE}, verify=False)
		response = json.loads(response.text)
		if response['success']:
			self.JWT = response['data']
			self.HEADERS = {"Authorization": self.JWT}
		else:
			print("Falha ao autenticar, teste abortado")
			exit()

	def setUp(self):
		cirurgia = {
			"tipo": {"id":1, "nome":"urgência"},
			"data": "20/08/2020 19:55",
			"procedimento": "Procedimento %s" % randrange(1000,9999),
			"cid": "CID %s" % randrange(1000,9999),
			"paciente": {
				"nome":"Teste",
				"data_nascimento": "21/05/1996",
				"numero":123
			},
			"materiais":[
				{"conteudo_caixa_id": 1, "solicitado": 2, "nome": "Sangue A+ 500ml"},
				{"conteudo_caixa_id": 2, "solicitado": 1, "nome": "Sangue A- 500ml"},
				{"conteudo_caixa_id": 4, "solicitado": 1, "nome": "Sangue O+ 500ml"}
			],
			"observacao":"Cirurgia criada pelo script de testes automaticos"
		}
		response = requests.post(SERVER % "api/cirurgia", json=cirurgia, headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		if response['success']:
			self.CIRURGIA_ID = response['data']
		else:
			print("erro ao criar cirurgia")
			exit()

	def id_das_cirurgias_nos_encaminhamentos(self, encaminhamentos):
		return list(map(lambda e: e['cirurgia']['id'], encaminhamentos))

	def encaminhar_cirurgia(self, cirurgia_id, status, observacao):
		a = {"cirurgia_id":self.CIRURGIA_ID, "status_id":status, "observacao":observacao }
		response = requests.post(SERVER % "api/encaminhamentos/mover-para", json=a, headers = self.HEADERS, verify=False)

	def test_cirurgia_recem_criada_aguarda_chm(self):
		response = requests.get(SERVER % "api/encaminhamentos/criados", headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		self.assertTrue(self.CIRURGIA_ID in self.id_das_cirurgias_nos_encaminhamentos(response['data']))

	def test_cirurgia_aprovada_nao_aguarda_chm(self):
		self.encaminhar_cirurgia(self.CIRURGIA_ID, STATUS_APROVADO_PELA_CHM, "Aprovado pelo script de testes")
		response = requests.get(SERVER % "api/encaminhamentos/criados", headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		self.assertFalse(self.CIRURGIA_ID in self.id_das_cirurgias_nos_encaminhamentos(response['data']))

	def test_cirurgia_reprovada_nao_aguarda_chm(self):
		self.encaminhar_cirurgia(self.CIRURGIA_ID, STATUS_REPROVADO_PELA_CHM, "Reprovado pelo script de testes")
		response = requests.get(SERVER % "api/encaminhamentos/criados", headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		self.assertFalse(self.CIRURGIA_ID in self.id_das_cirurgias_nos_encaminhamentos(response['data']))

	def test_cirurgia_reprovada_aparece_em_reprovados_por_chm(self):
		self.encaminhar_cirurgia(self.CIRURGIA_ID, STATUS_APROVADO_PELA_CHM, "Aprovado pelo script de testes")
		response = requests.get(SERVER % "api/encaminhamentos/reprovados-por-chm", headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		self.assertFalse(self.CIRURGIA_ID in self.id_das_cirurgias_nos_encaminhamentos(response['data']))

	def test_cirurgia_aprovada_nao_aparece_em_reprovados_por_chm(self):
		self.encaminhar_cirurgia(self.CIRURGIA_ID, STATUS_REPROVADO_PELA_CHM, "Reprovado pelo script de testes")
		response = requests.get(SERVER % "api/encaminhamentos/reprovados-por-chm", headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		self.assertTrue(self.CIRURGIA_ID in self.id_das_cirurgias_nos_encaminhamentos(response['data']))

	def test_cirurgia_aprovada_aparece_em_solicitacoes_aprovadas(self):
		self.encaminhar_cirurgia(self.CIRURGIA_ID, STATUS_APROVADO_PELA_CHM, "Aprovado pelo script de testes")
		response = requests.get(SERVER % "api/encaminhamentos/aprovados-por-chm", headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		self.assertTrue(self.CIRURGIA_ID in self.id_das_cirurgias_nos_encaminhamentos(response['data']))

	def test_cirurgia_recusada_nao_aparece_em_solicitacoes_aprovadas(self):
		self.encaminhar_cirurgia(self.CIRURGIA_ID, STATUS_REPROVADO_PELA_CHM, "Reprovado pelo script de testes")
		response = requests.get(SERVER % "api/encaminhamentos/criados", headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		self.assertFalse(self.CIRURGIA_ID in self.id_das_cirurgias_nos_encaminhamentos(response['data']))

	def test_cirurgias_aprovadas_nao_contem_repetidos(self):
		self.encaminhar_cirurgia(self.CIRURGIA_ID, STATUS_APROVADO_PELA_CHM, "Aprovado pelo script de testes")
		response = requests.get(SERVER % "api/encaminhamentos/criados", headers = self.HEADERS, verify=False)
		response = json.loads(response.text)
		ids = self.id_das_cirurgias_nos_encaminhamentos(response['data'])
		no_dupes = list(set(ids))
		self.assertEqual(len(ids), len(no_dupes))

if __name__ == '__main__':
	unittest.main()
