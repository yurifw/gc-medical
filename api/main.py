import os

from flask import Flask
from flask import render_template
from flask_cors import CORS
from flask import request

import endpoints.pacientes as paciente
import endpoints.cirurgia as cirurgia
import endpoints.caixa as caixa
import endpoints.auth as auth
import endpoints.usuario as usuario
import endpoints.encaminhamento as encaminhamento
import endpoints.setor as setor
import endpoints.perfil as perfil
import endpoints.medico as medico
import endpoints.hospital as hospital
import endpoints.gestor_hospitalar as gestor
import endpoints.documento as documento
import endpoints.permissoes as permissoes
import endpoints.historico as historico
import endpoints.auvo as auvo

app = Flask(__name__, static_folder='public/')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.debug = True
CORS(app)

app.add_url_rule('/api/pacientes', 'pac_c', paciente.insert, methods=['POST'])
app.add_url_rule('/api/pacientes', 'pac', paciente.select_all, methods=['GET'])
app.add_url_rule('/api/pacientes/<int:paciente_id>', 'pac_id', paciente.select_id, methods=['GET'])
app.add_url_rule('/api/pacientes/<int:paciente_id>', 'pac_a', paciente.atualizar, methods=['PATCH'])
app.add_url_rule('/api/pacientes/<int:paciente_id>', 'pac_d', paciente.deletar, methods=['DELETE'])
app.add_url_rule('/api/cirurgia', 'cir_p', cirurgia.insert, methods=['POST'])
app.add_url_rule('/api/cirurgia', 'cir_l', cirurgia.list_by_user, methods=['GET'])
app.add_url_rule('/api/cirurgia/<int:cirurgia_id>', 'cir_u', cirurgia.update, methods=['PATCH'])
app.add_url_rule('/api/cirurgia/<int:cirurgia_id>', 'cir_id', cirurgia.list_by_id, methods=['GET'])
app.add_url_rule('/api/caixas/<string:pesquisa>', 'ca', caixa.search_caixa_item, methods=['GET'])
app.add_url_rule('/api/caixa/<int:caixa_id>', 'ca_id', caixa.search_caixa_id, methods=['GET'])
app.add_url_rule('/api/caixas/proprietarios', 'ca_own', caixa.list_proprietarios, methods=['GET'])
app.add_url_rule('/api/usuario', 'us_new', usuario.insert, methods=['POST'])
app.add_url_rule('/api/usuario', 'us_info', usuario.info, methods=['GET'])
app.add_url_rule('/api/usuarios', 'us_ls', usuario.select_all, methods=['GET'])
app.add_url_rule('/api/usuario', 'us_upd', usuario.atualizar, methods=['PATCH'])
app.add_url_rule('/api/usuario/<int:usuario_id>', 'us_edit', usuario.update, methods=['PATCH'])
app.add_url_rule('/api/usuario/<int:usuario_id>', 'us_del', usuario.deletar, methods=['DELETE'])
app.add_url_rule('/api/usuario/<int:usuario_id>', 'us_lsid', usuario.select_id, methods=['GET'])
app.add_url_rule('/api/login', 'jwt_get', auth.get_token, methods=['POST'])
app.add_url_rule('/api/login/verify', 'jwt_check', auth.verify_token, methods=['GET'])
app.add_url_rule('/api/esqueci-senha', 's_e', auth.esqueci_senha, methods=['POST'])
app.add_url_rule('/api/resetar-senha', 's_r', auth.resetar_senha, methods=['PATCH'])
app.add_url_rule('/api/setores', 'sall', setor.select_all, methods=['GET'])
app.add_url_rule('/api/setores/<int:setor_id>', 'sid', setor.select_by_id, methods=['GET'])
app.add_url_rule('/api/perfis', 'pall', perfil.select_all, methods=['GET'])
app.add_url_rule('/api/medico', 'med_new', medico.insert, methods=['POST'])
app.add_url_rule('/api/medico', 'med_all', medico.select_all, methods=['GET'])
app.add_url_rule('/api/medico/<int:medico_id>', 'med_del', medico.delete, methods=['DELETE'])
app.add_url_rule('/api/medico/<int:medico_id>', 'med_upd', medico.atualizar, methods=['PATCH'])
app.add_url_rule('/api/medico/<int:medico_id>', 'med_inf', medico.info, methods=['GET'])
app.add_url_rule('/api/hospital/medicos', 'med_hosp', medico.list_usuarios_by_hospital, methods=['GET'])
app.add_url_rule('/api/hospital', 'h_all', hospital.select_all, methods=['GET'])
app.add_url_rule('/api/hospital/<int:hospital_id>', 'h_del', hospital.delete, methods=['DELETE'])
app.add_url_rule('/api/hospital', 'h_new', hospital.insert, methods=['POST'])
app.add_url_rule('/api/hospital/<int:hospital_id>', 'h_atu', hospital.update, methods=['PATCH'])
app.add_url_rule('/api/hospital/<int:hospital_id>', 'h_sid', hospital.select_by_id, methods=['GET'])
app.add_url_rule('/api/gestor-hospitalar', 'gh_new', gestor.insert, methods=['POST'])
app.add_url_rule('/api/gestor-hospitalar', 'gh_all', gestor.select_all, methods=['GET'])
app.add_url_rule('/api/gestor-hospitalar/<int:gestor_id>', 'gh_del', gestor.delete, methods=['DELETE'])
app.add_url_rule('/api/gestor-hospitalar/<int:gestor_id>', 'gh_upd', gestor.atualizar, methods=['PATCH'])
app.add_url_rule('/api/gestor-hospitalar/<int:gestor_id>', 'gh_inf', gestor.info, methods=['GET'])
app.add_url_rule('/api/documento', 'doc_new', documento.insert, methods=['POST'])
app.add_url_rule('/api/cirurgia/<int:cirurgia_id>/documentos', 'doc_get', documento.select_cirurgia, methods=['GET'])
app.add_url_rule('/api/documento/<int:documento_id>', 'doc_del', documento.delete, methods=['DELETE'])
app.add_url_rule('/api/documento/<int:documento_id>', 'doc_down', documento.download, methods=['GET'])
app.add_url_rule('/api/permissoes', 'per_all', permissoes.select_all, methods=['GET'])
app.add_url_rule('/api/permissoes/acoes', 'per_ac', permissoes.acoes_disponiveis, methods=['GET'])
app.add_url_rule('/api/permissoes', 'per_new', permissoes.insert, methods=['POST'])
app.add_url_rule('/api/permissoes/<int:perfil_id>', 'per_del', permissoes.delete, methods=['DELETE'])
app.add_url_rule('/api/permissoes/<int:perfil_id>', 'per_up', permissoes.update, methods=['PATCH'])
app.add_url_rule('/api/permissoes/<int:perfil_id>', 'per_id', permissoes.select_id, methods=['GET'])
app.add_url_rule('/api/historico/<int:cirurgia_id>', 'his_id', historico.select_id, methods=['GET'])
app.add_url_rule('/api/encaminhamentos/<int:encaminhamento_id>', 'encs_id', encaminhamento.list_id, methods=['GET'])
app.add_url_rule('/api/encaminhamentos/mover-para', 'encs_move', encaminhamento.mover_encaminhamento, methods=['POST'])
app.add_url_rule('/api/encaminhamentos/chm', 'enc_chm', encaminhamento.list_chm, methods=['GET'])
app.add_url_rule('/api/encaminhamentos/comercial', 'enc_com', encaminhamento.list_comercial, methods=['GET'])
app.add_url_rule('/api/encaminhamentos/medicos', 'enc_med', encaminhamento.list_medicos, methods=['GET'])
app.add_url_rule('/api/encaminhamentos/status-possiveis', 'enc_s', encaminhamento.list_status, methods=['GET'])
app.add_url_rule('/api/auvo/usuarios', 'auvo_u', auvo.list_users, methods=['GET'])



# sends 404 to index so angular can handle them
@app.errorhandler(404)
def not_found(e):
	requested_file = request.path[1:] if len(request.path[1:]) > 0 else "index.html"
	if "." not in requested_file:
		requested_file = "index.html"
	return app.send_static_file(requested_file)
